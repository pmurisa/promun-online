<?php
header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');


require "DBAPI.php";
require "ODBCAPI.php";
$total = 1; //for counting number of requistions


$promunRequisitions = getRequisitions();
//print_r($promunRequisitions);
if(empty($promunRequisitions)){
    print_r("No Requisitions");
    }else{
foreach($promunRequisitions as $pr){
    $reqNo = @$pr["req-no"];
    $reqDate = @$pr["req-date"];
    $reqDesc = @$pr["req-descr"];
    $reqCode = @$pr["pass-code"];
    $authCode = @$pr["auth-code"];
    $orderType = @$pr["ord-type"];
    $reqType = @$pr["req-type"];
    $reqStatus = @$pr["req-status"];
    $authNumber =  @$pr["auth-number"];
    $captCode =  @$pr["capt-code"];
    $all_auths =  @$pr["all-auths"]; 
    
   //check if duplicate exist skip
   $status = checkRequisitions($reqNo);
   if (@$status['status'] == 'False') {
   $createReq =  createRequisitions($reqNo, $reqDate, $reqDesc, $reqCode, $authCode, $orderType, $reqType, $reqStatus, $authNumber, $captCode,$all_auths,$total );
   }
}
}
if(@$status['status'] == 'False'){
   
    $rslt["msg"] = "Promun Requisitions  Succesfully synced!";
   
    $rslt["status"] = "ok";
}
else{
     $rslt["msg"] = "Requisitions already exist! Error: ".@$status["status"];
    $rslt["status"] = "failed";
}
echo json_encode($rslt);