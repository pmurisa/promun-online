<?php 

require "../Approvals/DBAPI.php";
$coDetails = getCompanyDetails();
if(!empty($coDetails)){
    $host = @$coDetails[0]['host'];
    $port = @$coDetails[0]['port'];
    $username = @$coDetails[0]['username'];
    $password = @$coDetails[0]['emailPassword'];
    $la = @$coDetails[0]['description'];
}
$updateTo;
require '..\phpMailer\PHPMailerAutoload.php';
$mail = new phpMailer;
$mail->Host = $host;
$mail->Port = $port;
$mail->SMTPAuth = true;
$mail->SMTPSecure = 'tls';
$mail->isSMTP();
$mail->Username =$username;
$mail->Password = $password;



?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<style>
/*Sidemenu*/
.menu 
{
	list-style:none;
}
.menu li ul 
{
	list-style:none;
}
.menu li ul li 
{
	display:block; 
	height:40px; 
	border-bottom:solid 1px #E4D9D9;
}
.menu li ul a, .menu li ul a:visited, .menu li ul a:active {
	display:block; 
	height:40px; 
	padding:5px 5px 0px 5px; 
	text-decoration:none; 
	color: blue;
	font-size: 12px;
	}
.menu li ul  a:hover {
	background: white;
	color: brown;
	font-weight: bold;
	border-bottom:solid 1px #E4D9D9;
}
.menu h2 {
	display:block;
	border-bottom:solid 2px brown;
	padding:5px;
	margin:0px 0px 0px 0px;
	font-family: Arial, Helvetica, sans-serif;
	font-size:18px;
	color:white;
	font-weight: bolder;
}

</style>
<title>E-Promun Approval Platform</title>

<!------------------------------------------------------------------------- Bootstrap --------------------------------------------------------------->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
<link rel="icon" href="../Approvals/img/axislogo.jpg">
</head>
<body><br>
<form method="post" style="  box-shadow :1px 1px 20px gray; width: 95%; border-radius: 10px 10px 10px; margin:auto">
<table border="0" width="90%" align="center" class="table-responsive" style="border-color:gray">
<tbody>
          <tr>
        	<td colspan="2"  height="50" align="center">
            	<table border="0" width="90%" align="center" class="table-responsive" style="border-color:gray">
                	<tr>
                    	<td align="center">
                        	<img src="img/axislogo.jpg" width="100" height="100" class="img-circle">
                        </td>
                        <td width="80%" align="center">
                        	<h3 align="center" style="color: blue"><strong>Promun Online</strong></h3>
							<p style="line-height: 20px">Zimbabwe <br> <label style="color:#FF0000">"Your Order, Our Commitment"</label></p>
                        </td>
                        <td align="center">
                        	<img src="img/download.jpg" width="100" height="100" class="img-responsive">
                        </td>
                    </tr>
                </table>
            </td>
          </tr>
		  <tr>
        	<td colspan="2" height="5" style="background-color: #E4D9D9">
            </td>
          </tr>
          <tr>
          	<td colspan="2">
          		<table width="100%" border="0">
                	<tr>
                        <td width="35%">
                        <h5 style=" color:brown;" class="badge">
                            <strong> <?php echo $la; ?>
							
							</strong></h5>
                        </td>
                        <td align="right" style="color: #0000FF" width="35%">
                            <?php
								
  echo "<h5 style='color: blue'><a href='../Approvals/adminlogout.php'><strong>click here to logout</strong></a></h5>";
							?>
                         </td>
                     </tr>
                </table>
             </td>
          </tr>
  
 
<tr>
    <td colspan="2">
       <table border="1" class="table-bordered" width="100%" style="background-color: #002F74; border-radius:20px; border-color:white;">
          <tr>
             <td width="20%" style="background-color: white" valign="top">
				 <?php
				 
                    include('admindashboard.php');
                 ?>
             </td>
             <td width="80%" valign="top"><br>
            <table class="table-stripped" border="0" width="98%" align="center"  >
            <td>
			<div id="circle">
	
<?php
$checkNet =  checkInternet();
	if($checkNet['status']=="offline"){
		echo "<script>alert('Error: No network connection emails can not be submitted!!');location='../Approvals/noNetPdf.php '</script>";
	}else{
$email = EmailRatePayer();

if(empty($email)){
    echo "No Emails to deliver";
}
else{
    foreach($email as $emailData){
$to = @$emailData["toEmail"];
$from = @$emailData["fromEmail"];
$subject = @$emailData["subject"];
$body = @$emailData["body"];
$status = @$emailData["status"];
$updateTo = @$emailData["to"];
$email_code = @$emailData["seq"];


/*Update the email sent */
$code = $email_code;




$mail->setFrom($from);
$mail->addAddress($to);
$mail->addReplyTo($from);
$mail->Subject = $subject;
$mail->Body=$body;
if(!$mail->send()){

//echo 'Mailer error: ' . $mail->ErrorInfo;
//echo "host is $host Port is $port Username is $username Password is$password";
echo "<script>alert('View Exception Report Port is $port'); location='../Approvals/unsentEmailsPdf.php'</script>";
//updateEmails();

}else {echo "Message succesfully sent($to)";
    $Create = updateEmails($code);
    echo "<script>alert('View Exception Report'); location='../Approvals/sentEmailsPdf.php'</script>";
}
}
}
}



?>
</div>

</td>
<td>


</td>
</tr>

<tr>
<td>


</td>
</tr>
</table>



</td>
</tr>
<?php
include('../Approvals/footer1.php');
?>
