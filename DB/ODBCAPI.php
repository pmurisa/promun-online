<?php
try{
$dbodbc = new PDO('odbc:promun', 'Sysprogress', 'Sysprogress');
$dbodbc->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

} catch (Exception $ex){
    echo "<script>alert('Notice: Promun (Promun) Database is not Running ');</script>".$ex->getMessage();
    die();
}
try{

    
    $dbexd = new PDO('odbc:exd', 'Sysprogress', 'Sysprogress');
    $dbexd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    } catch (Exception $ex){
        echo "<script>alert('Notice: Promun EXD Database is not Running ');</script>".$ex->getMessage();
        
        die();
    }
    try{
   
        
        $dbinc = new PDO('odbc:inc', 'Sysprogress', 'Sysprogress');
        $dbinc->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
   
        } catch (Exception $ex){
            echo "<script>alert('Notice: Promun INC Database is not Running ');</script>".$ex->getMessage();
            die();
        }
        /*try{
            
            
            $dbrec = new PDO('odbc:rec', 'Sysprogress', 'Sysprogress');
            $dbrec->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch (Exception $ex){
                echo "<script>alert('Notice: Promun REC Database is not Running ');</script>".$ex->getMessage();
                die();
            }*/
Function GetDbStatus(){
    
    try{
   
        
        $dbexd = new PDO('odbc:exd', 'Sysprogress', 'Sysprogress');
        $dbexd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $result = "Yes";
        } catch (Exception $ex) {

       $result = "No";
   }
 
   return $result;
 }


Function GetEmails(){
    global $dbodbc;
   try {
       $sql = $dbodbc->prepare('select "seq-no", "msg-from", "msg-to", "msg-subject","msg-date","msg-body",sent from PUB."email-log" where sent = 0');
       $sql->execute();
       $result = $sql->fetchALL(PDO::FETCH_ASSOC);
   } catch (Exception $ex) {
       $result = $ex->getMessage();
   }
 
   return $result;
 }
function updatePromunEmails($emailCode) {
    global $dbodbc;
    try {
        
        $sql = $dbodbc->prepare('update PUB."email-log" set sent = 1 where "seq-no" = ?');
        $sql->execute(array($emailCode));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result["status"] = "ok";
        } else {
            $result["status"] = "fail";
        }
    } catch (Exception $ex) {
        $result["status"] = $ex->getMessage();
    }
    return $result;
}

Function getUsers(){
    global $dbodbc;
   try {
       $sql = $dbodbc->prepare('select "pass-code", "pass-word", "igl-from","igl-to", "lock-user", "max-auth", "loan-level","w-phone","power" from PUB."pass-file"');
       $sql->execute();
       $result = $sql->fetchALL(PDO::FETCH_ASSOC);
   } catch (Exception $ex) {
       $result = $ex->getMessage();
   }
 
   return $result;
 }
 Function getVoucherSetUp(){
    global $dbexd;
   try {
       $sql = $dbexd->prepare('select "requestedby", "requestedby-2", "requestedby-3","requestedby-4", "approvedby-hod", "approvedby-fd", "authorisedby-tc" from PUB.paymentvoucher');
       $sql->execute();
       $result = $sql->fetchALL(PDO::FETCH_ASSOC);
   } catch (Exception $ex) {
       $result = $ex->getMessage();
   }
 
   return $result;
 }
 Function getCompanyDetails(){
    global $dbodbc;
   try {
       $sql = $dbodbc->prepare('select "vat-reg-no", "cof-desc", "co-code" from PUB.procof');
       $sql->execute();
       $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    
   } catch (Exception $ex) {
       $result = $ex->getMessage();
   }
 
   return $result;
 }
 Function getRequisitions(){
    global $dbexd;
    
   try {
       $sql = $dbexd->prepare('select "req-no", "req-date", "req-descr","pass-code", "auth-code", "ord-type", "req-type", "req-status","auth-number","capt-code","all-auths" from PUB.cmmreq ');
       $sql->execute();
       $result = $sql->fetchALL(PDO::FETCH_ASSOC);
   } catch (Exception $ex) {
       $result = $ex->getMessage();
   }
 
   return $result;
 }
 Function  getOrders() {
    global $dbexd;
    
   try {
       $sql = $dbexd->prepare('select "order-no", "ord-type","ord-date", "req-no","ord-desc", "br-code", "inv-status", "pass-code", "supplier","capt-code","auth-code" from PUB.cmmamf ');
       $sql->execute();
       $result = $sql->fetchALL(PDO::FETCH_ASSOC);
   } catch (Exception $ex) {
       $result = $ex->getMessage();
   }
 
   return $result;
 }
 Function getSuppliers(){
    global $dbexd;
    
   try {
       $sql = $dbexd->prepare('select "account", "br-code","name", "vat-reg-no","contact","phys-addr","phone","category","tax-clearance-exp-date","tax-clearance-no" from PUB.crdamf ');
       $sql->execute();
       $result = $sql->fetchALL(PDO::FETCH_ASSOC);
   } catch (Exception $ex) {
       $result = $ex->getMessage();
   }
 
   return $result;
 }
 Function getOrdersQuantity(){
    global $dbexd;
    
   try {
       $sql = $dbexd->prepare('select "order-no", "ord-type","seq-no", "qty","alloc", "whse", "stk-code", "amt", "descrip","invoiced","inv-amt","inv-qty","vat-amt","quote-number" from PUB.cmmdcf ');
       $sql->execute();
       $result = $sql->fetchALL(PDO::FETCH_ASSOC);
   } catch (Exception $ex) {
       $result = $ex->getMessage();
   }
 
   return $result;
 }

 Function getOrderTypes(){
    global $dbexd;
    
   try {
       $sql = $dbexd->prepare('select "ord-type", "descr" from PUB.cmmord ');
       $sql->execute();
       $result = $sql->fetchALL(PDO::FETCH_ASSOC);
   } catch (Exception $ex) {
       $result = $ex->getMessage();
   }
 
   return $result;
 }
 Function getRequisitionLine(){
    global $dbexd;
    
   try {
       $sql = $dbexd->prepare('select "req-no", "seq-no","whse","stk-code","qty","amt","description","alloc","gl-acct","unit-cost","req-type" from PUB.cmmrcf ');
       $sql->execute();
       $result = $sql->fetchALL(PDO::FETCH_ASSOC);
   } catch (Exception $ex) {
       $result = $ex->getMessage();
   }
 
   return $result;
 }
 Function getWareHouses(){
    global $dbexd;
    
   try {
       $sql = $dbexd->prepare('select "code", "descrip","gl-acc-no", "alloc" from PUB.iscwrhse ');
       $sql->execute();
       $result = $sql->fetchALL(PDO::FETCH_ASSOC);
   } catch (Exception $ex) {
       $result = $ex->getMessage();
   }
 
   return $result;
 }
 Function getWareHousesDetails(){
    global $dbexd;
    
   try {
       $sql = $dbexd->prepare('select "stk-code", "descrip","reorder", "max-level","bf-qty","ins","outs","whse","cmm-qty","cost","qty","cost" from PUB.iscsmf ');
       $sql->execute();
       $result = $sql->fetchALL(PDO::FETCH_ASSOC);
       
   } catch (Exception $ex) {
       $result = $ex->getMessage();
   }
 
   return $result;
 }
 function get_all_debtors() 
 {
     global $dbinc;
     try {
      //  $sql = $dbinc->prepare('SELECT PUB.muncmf.acc,PUB.muncmf.name,PUB.muncmf.addr1,PUB.muncmf.addr2,PUB.muncmf.addr3,PUB.muncmf.cell,PUB.muncmf.phone,PUB.muncmf.email,PUB.muncmf.idno,PUB.muncmf.balance FROM PUB.muncmf where company=0 and active != 99');
         $sql = $dbinc->prepare('SELECT PUB.muncmf.acc,PUB.muncmf.name,PUB.muncmf.addr1,PUB.muncmf.addr2,PUB.muncmf.addr3,PUB.muncmf.cell,PUB.muncmf.phone,PUB.muncmf.email,PUB.muncmf.idno,PUB.muncmf.balance FROM PUB.muncmf where company=0');
         $sql->execute();
         $result = $sql->fetchALL(PDO::FETCH_ASSOC);
     } catch (Exception $ex) {
         $result = $ex->getMessage();
     }
     return $result; 
 }
/* function GetBalTypes() 
{
    global $dbrec;
    try {
        $sql = $dbrec->prepare("select * from PUB.municf");
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result; 
}*/
function GetCustomerBalances($Acc)
{
     global $dbinc;
    try {
        $sql = $dbinc->prepare(' select * from PUB.munbmf where acc = ?');
        $sql->execute(array($Acc));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) 
    {
        $result = $ex->getMessage();
    }
    return $result; 
}
function getActualBalanceTypes(){
    global $dbinc;
     try {
         $sql = $dbinc->prepare('SELECT "bmf-type","des-eng","rcpt-code" from  PUB.muntyp');
         $sql->execute();
         $result = $sql->fetchALL(PDO::FETCH_ASSOC);
     } catch (Exception $ex) {
         $result = $ex->getMessage();
     }
     return $result; 
 }
/*/ Function getAccTransHis($acc,$startDate, $endDate){
    global $dbinc;
       try {
           $sql = $dbinc->prepare('SELECT acc,amt,period,prog,ref , "bmf-type", "vat-amt" , "tr-date" from  PUB.munthf 
                   where  acc = ? and "tr-date" >= ? and "tr-date" <= ? ');
           $sql->execute(array($acc,$startDate, $endDate)); 
           $result = $sql->fetchALL(PDO::FETCH_ASSOC);
       } catch (Exception $ex) {
           $result = $ex->getMessage();
       }
       return $result;   
   }*/
 Function getAccTransHis(){
    global $dbinc;
       try {
           $sql = $dbinc->prepare('SELECT acc,amt,period,prog,ref , "bmf-type", "vat-amt" , "tr-date" from  PUB.munthf ');
           $sql->execute(); 
           $result = $sql->fetchALL(PDO::FETCH_ASSOC);
       } catch (Exception $ex) {
           $result = $ex->getMessage();
       }
       return $result;   
   }
   Function getMunbmf(){
    global $dbinc;
       try {
           $sql = $dbinc->prepare('SELECT acc,bal,type,deposit,"bf-bal" from  PUB.munbmf ');
           $sql->execute(); 
           $result = $sql->fetchALL(PDO::FETCH_ASSOC);
       } catch (Exception $ex) {
           $result = $ex->getMessage();
       }
       return $result;   
   }
   // to take meter details
   Function getMunrmf(){
    global $dbinc;
       try {
           $sql = $dbinc->prepare('SELECT acc,"meter-no",read,tarif from  PUB.munrmf ');
           $sql->execute(); 
           $result = $sql->fetchALL(PDO::FETCH_ASSOC);
       } catch (Exception $ex) {
           $result = $ex->getMessage();
       }
       return $result;   
   }
   Function getIgl(){
    global $dbexd;
       try {
           $sql = $dbexd->prepare('SELECT "bal-year","amount","acct-no","budget" from  PUB.iglabf ');
           $sql->execute(); 
           $result = $sql->fetchALL(PDO::FETCH_ASSOC);
       } catch (Exception $ex) {
           $result = $ex->getMessage();
       }
       return $result;   
   }
   function  updatePromunOrders($passCode,$reqNo){
    global $dbexd;
    try {
        
        $sql = $dbexd->prepare('update PUB."cmmamf" set "pr-type" = ?,printed = 1  where "order-no" = ?');
        $sql->execute(array($passCode,$reqNo));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result["status"] = "ok";
        } else {
            $result["status"] = "fail";
        }
    } catch (Exception $ex) {
        $result["status"] = $ex->getMessage();
    }
    return $result;
}
function updatePromunRequisitions($reqStatus,$actual_date,$authNumber,$reqNo){
    global $dbexd;
    try {
        
        $sql = $dbexd->prepare('update PUB."cmmreq" set "req-status" = ?, "req-auth-no"= ? , "auth-number"=? where "req-no" = ?');
        $sql->execute(array($reqStatus,$actual_date,$authNumber,$reqNo));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result["status"] = "ok";
        } else {
            $result["status"] = "fail";
        }
    } catch (Exception $ex) {
        $result["status"] = $ex->getMessage();
    }
    return $result;
}





