<?php
//$db = new PDO("sqlsrv:Server=sql5031.site4now.net;Database=DB_A33C8A_epromun", "DB_A33C8A_vicfalls_admin", "12BElvedere");

//$db = new PDO("sqlsrv:Server=PROMUNSERVER;Database=vicfalls", "sa", "12BElvedere");
$db = new PDO("sqlsrv:Server=DESKTOP-C84RPM3\SQLEXPRESS;Database=axisEmails", "sa", "12BElvedere");
//$db = new PDO("sqlsrv:Server=sql5031.site4now.net;Database=DB_A33C8A_vicfalls", "DB_A33C8A_vicfalls_admin", "12BElvedere");
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

function CreateEmails($emailCode, $emailTo, $emailFrom, $emailSubject, $emailBody,$emailStatus, $emailDate) {
    global $db;
    try {

        $sql = $db->prepare('insert into emails("seq","toEmail","from",subject,body,status,date) values (?,?,?,?,?,?,?)');
        $sql->execute(array($emailCode, $emailTo, $emailFrom, $emailSubject, $emailBody,$emailStatus, $emailDate));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result['status'] = 'ok';
        } else {
            $result['status'] = 'fail';
        }
    } catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }
    return $result;
}
function createCompanyDetails($vatReg , $description, $code){
    global $db;
    try{
        $sql = $db->prepare('insert into company("vat", "description", "code") values(?,?,?) ');
        $sql->execute(array($vatReg, $description, $code));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result['status'] = 'ok';
        } else {
            $result['status'] = 'No user with those details';
        }

    }catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }
    return $result;

}
function createUsers($passCode, $passWord, $iglFrom, $iglTo, $lockUser, $maxAuth, $loanLevel,$email,$isAdmin ){
    global $db;
    try{
        $sql = $db->prepare('insert into users("passCode", "passWord", "iglFrom", "iglTo", "lockUser", "maxAuth", "loanLevel","email","isAdmin") values(?,?,?,?,?,?,?,?,?) ');
        $sql->execute(array($passCode, $passWord, $iglFrom, $iglTo, $lockUser, $maxAuth, $loanLevel,$email,$isAdmin));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result['status'] = 'ok';
        } else {
            $result['status'] = 'fail';
        }

    }catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }
    return $result;

}
function createAxisUser($code,$pass,$admin){
global $db;
try{
    $sql = $db->prepare('insert into users("passCode", "passWord","isAdmin") values(?,?,?) ');
    $sql->execute(array($code,$pass,$admin));
    $count = $sql->rowCount();
    if ($count > 0) {
        $result['status'] = 'ok';
    } else {
        $result['status'] = 'fail';
    }

}catch (Exception $ex) {
    $result['status'] = $ex->getMessage();
}
return $result;

}
function createRequisitions($reqNo, $reqDate, $reqDesc, $reqCode, $authCode, $orderType, $reqType, $reqStatus, $authNumber,$captCode,$all_auths,$total){
    global $db;
    try{
        $sql = $db->prepare('insert into requisitions("reqNo","reqDate", "reqDesc", "passCode", "authCode", "ordType","reqType", "reqStatus","authNumber","captCode","allAuths","total") values(?,?,?,?,?,?,?,?,?,?,?,?) ');
        $sql->execute(array($reqNo, $reqDate, $reqDesc, $reqCode, $authCode, $orderType, $reqType, $reqStatus, $authNumber, $captCode,$all_auths,$total));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result['status'] = 'ok';
        } else {
            $result['status'] = 'fail';
        }

    }catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }
    return $result;

}
function  createMunbmf($acc, $balance, $type, $deposit, $bfBal){
    global $db;
    try{
        $sql = $db->prepare('insert into munbmf("acc","balance", "type", "deposit", "bfBal") values(?,?,?,?,?) ');
        $sql->execute(array($acc, $balance, $type, $deposit, $bfBal));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result['status'] = 'ok';
        } else {
            $result['status'] = 'fail';
        }

    }catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }
    return $result;

}
function  createBudget($acc, $amt, $balYear,$budget){
    global $db;
    try{
        $sql = $db->prepare('insert into budgets("acc","amt", "balYear","budget") values(?,?,?,?) ');
        $sql->execute(array($acc, $amt, $balYear,$budget));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result['status'] = 'ok';
        } else {
            $result['status'] = 'fail';
        }

    }catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }
    return $result;

}
function createMunthf($acc, $amt, $period, $ref, $bmfType,$vatAmt,$trDate){
    global $db;
    try{
        $sql = $db->prepare('insert into munthf("acc","amt", "period", "ref", "bmfType",vat,trDate) values(?,?,?,?,?,?,?) ');
        $sql->execute(array($acc, $amt, $period, $ref, $bmfType,$vatAmt,$trDate));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result['status'] = 'ok';
        } else {
            $result['status'] = 'fail';
        }

    }catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }
    return $result;

}
function createOrders($ordNo, $ordType,$ordDate, $ordDesc,$reqNo,$brCode,$invStatus,$supplier, $passCode, $authCode, $captCode, $ordStatus,$total){
    global $db;
    try{
        $sql = $db->prepare('insert into orders("ordNo","ordType","ordDate", "ordDesc","reqNo", "brCode", "invStatus", "supplier","passCode", "authCode","captCode","ordStatus","total") values(?,?,?,?,?,?,?,?,?,?,?,?,?) ');
        $sql->execute(array($ordNo, $ordType,$ordDate, $ordDesc,$reqNo,$brCode,$invStatus,$supplier, $passCode, $authCode, $captCode, $ordStatus,$total));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result['status'] = 'ok';
        } else {
            $result['status'] = 'fail';
        }

    }catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }
    return $result;

}

function createSuppliers($acc, $brCode,$name, $vatReg,$contact,$address,$phone,$category,$taxClearanceDate,$taxClearanceNo ){
    global $db;
    try{
        $sql = $db->prepare('insert into suppliers("account","brCode","name", "vat","contact", "address", "phone", "category","taxDate","taxNo") values(?,?,?,?,?,?,?,?,?,?) ');
        $sql->execute(array($acc, $brCode,$name, $vatReg,$contact,$address,$phone,$category,$taxClearanceDate,$taxClearanceNo ));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result['status'] = 'ok';
        } else {
            $result['status'] = 'fail';
        }

    }catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }
    return $result;

}
function createOrdersQuantity($ordNo, $ordType,$seqNo, $qty,$allocation,$whse,$stkCode,$amt, $descrip, $inv, $invAmt, $invQty,$vatAmt,$qNo ){
    global $db;
    try{
        $sql = $db->prepare('insert into OrderDescription("ordNo","ordType","seqNo","qty", "allocation","Whse", "stkCode", "amt", "descrip","inv","invAmt","invQty","vatAmt","qNo") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?) ');
        $sql->execute(array($ordNo, $ordType,$seqNo, $qty,$allocation,$whse,$stkCode,$amt, $descrip, $inv, $invAmt, $invQty,$vatAmt,$qNo  ));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result['status'] = 'ok';
        } else {
            $result['status'] = 'fail';
        }

    }catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }
    return $result;

}
function createOrderTypes($ordType,$ordDescrip ){
    global $db;
    try{
        $sql = $db->prepare('insert into OrderTypes("ordType","ordDescrip") values(?,?) ');
        $sql->execute(array($ordType,$ordDescrip  ));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result['status'] = 'ok';
        } else {
            $result['status'] = 'fail';
        }

    }catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }
    return $result;

}
function createReqLine($reqNo, $seqNo,$whse, $stkCode,$qty,$amt,$description,$alloc, $glAcc, $unitCost, $reqType ){
    global $db;
    try{
        $sql = $db->prepare('insert into requisitionLines("reqNo","seqNo","whse","stkCode","qty","amt","descrip","alloc","glAcc","cost","reqType") values(?,?,?,?,?,?,?,?,?,?,?) ');
        $sql->execute(array($reqNo, $seqNo,$whse, $stkCode,$qty,$amt,$description,$alloc, $glAcc, $unitCost, $reqType));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result['status'] = 'ok';
        } else {
            $result['status'] = 'fail';
        }

    }catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }
    return $result;

}
function createWarehouseMaster($code, $descrip,$gl, $alloc ){
    global $db;
    try{
        $sql = $db->prepare('insert into wareHouseMaster("code","descrip","gl","alloc") values(?,?,?,?) ');
        $sql->execute(array($code, $descrip,$gl, $alloc ));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result['status'] = 'ok';
        } else {
            $result['status'] = 'fail';
        }

    }catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }
    return $result;

}
function createWarehouseDetails($stkCode, $descrip,$whse, $reorder,$maxLevel,$bfQty,$stkReceived,$onOrderQty,$qty,$cost ){
    global $db;
    try{
        $sql = $db->prepare('insert into wareHouseDetails("stkCode","descrip","whse","reorder","maxLevel","bfQty","stkReceived","onOrder","qty","cost") values(?,?,?,?,?,?,?,?,?,?) ');
        $sql->execute(array($stkCode, $descrip,$whse, $reorder,$maxLevel,$bfQty,$stkReceived,$onOrderQty,$qty,$cost ));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result['status'] = 'ok';
        } else {
            $result['status'] = 'fail';
        }

    }catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }
    return $result;

}
function checkRequisitions($reqNo) {
    global $db;
    try {
        $sql = $db->prepare("select * from requisitions where reqNo=?");
        $sql->execute(array($reqNo));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        if ($sql->rowCount() > 0) {
            $status['status'] = 'true';
           
        } else {
            $status['status'] = 'False';

        }
    } catch (Exception $ex) {
        $status = $ex->getMessage();
    }

    return $status;
}
function checkMunbmf($balance,$acc){
    global $db;
    try {
        $sql = $db->prepare("select * from munbmf where balance=? and acc=?");
        $sql->execute(array($balance,$acc));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        if ($sql->rowCount() > 0) {
            $status['status'] = 'true';
           
        } else {
            $status['status'] = 'False';

        }
    } catch (Exception $ex) {
        $status = $ex->getMessage();
    }

    return $status;
}
function checkWareHouse($code) {
    global $db;
    try {
        $sql = $db->prepare("select * from wareHouseMaster where code=?");
        $sql->execute(array($code));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        if ($sql->rowCount() > 0) {
            $status['status'] = 'true';
           
        } else {
            $status['status'] = 'False';

        }
    } catch (Exception $ex) {
        $status = $ex->getMessage();
    }

    return $status;
}
function checkWareHouseDetails($whse,$stkCode) {
    global $db;
    try {
        $sql = $db->prepare("select * from wareHouseDetails where whse=? and stkCode =?");
        $sql->execute(array($whse,$stkCode));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        if ($sql->rowCount() > 0) {
            $status['status'] = 'true';
           
        } else {
            $status['status'] = 'False';

        }
    } catch (Exception $ex) {
        $status = $ex->getMessage();
    }

    return $status;
}
function  checkUser($passCode) {
    global $db;
    try {
        $sql = $db->prepare("select * from users where passCode=?");
        $sql->execute(array($passCode));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        if ($sql->rowCount() > 0) {
            $status['status'] = 'true';
           
        } else {
            $status['status'] = 'False';

        }
    } catch (Exception $ex) {
        $status = $ex->getMessage();
    }

    return $status;
}
function checkAxis($code){
    global $db;
    try {
        $sql = $db->prepare("select * from users where passCode=?");
        $sql->execute(array($code));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        if ($sql->rowCount() > 0) {
            $status['status'] = 'true';
           
        } else {
            $status['status'] = 'False';

        }
    } catch (Exception $ex) {
        $status = $ex->getMessage();
    }

    return $status;
}


function checkRequisitionLine($reqNo,$seqNo)   {
    global $db;
    try {
        $sql = $db->prepare("select * from requisitionLines where reqNo=? and seqNo = ?");
        $sql->execute(array($reqNo,$seqNo));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        if ($sql->rowCount() > 0) {
            $status['status'] = 'true';
           
        } else {
            $status['status'] = 'False';

        }
    } catch (Exception $ex) {
        $status = $ex->getMessage();
    }

    return $status;
}
function checkOrders($ordNo,$ordType)   {
    global $db;
    try {
        $sql = $db->prepare("select * from orders where ordNo=? and ordType = ?");
        $sql->execute(array($ordNo,$ordType));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        if ($sql->rowCount() > 0) {
            $status['status'] = 'true';
           
        } else {
            $status['status'] = 'False';

        }
    } catch (Exception $ex) {
        $status = $ex->getMessage();
    }

    return $status;
}
function checkDebtorBal($acc,$bal)   {
    global $db;
    try {
        $sql = $db->prepare("select * from DebtorsBalances where acc=? and balance = ?");
        $sql->execute(array($acc,$bal));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        if ($sql->rowCount() > 0) {
            $status['status'] = 'true';
           
        } else {
            $status['status'] = 'False';

        }
    } catch (Exception $ex) {
        $status = $ex->getMessage();
    }

    return $status;
}
function updateDetails($bal,$acc){
    global $db;
    try {
        $sql = $db->prepare("update DebtorsBalances balance = ? where acc=?");
        $sql->execute(array($acc,$bal));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        if ($sql->rowCount() > 0) {
            $status['status'] = 'true';
           
        } else {
            $status['status'] = 'False';

        }
    } catch (Exception $ex) {
        $status = $ex->getMessage();
    }

    return $status;
}
function checkBalType($IncCode,$IncomeCodeDesc)  {
    global $db;
    try {
        $sql = $db->prepare("select * from BalanceTypes where incCode=? and incDesc = ?");
        $sql->execute(array($IncCode,$IncomeCodeDesc));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        
        if ($sql->rowCount() > 0) {
            $status['status'] = 'true';
           
        } else {
            $status['status'] = 'False';

        }
    } catch (Exception $ex) {
        $status = $ex->getMessage();
    }
    return $status;
    
}

function UpdateBal($bal,$acc){
    global $db;
    try {
        $sql = $db->prepare("update DebtorsBalances set balance=? where acc=?");
        $sql->execute(array($bal,$acc));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        
        if ($sql->rowCount() > 0) {
            $status['status'] = 'true';
           
        } else {
            $status['status'] = 'False';

        }
    } catch (Exception $ex) {
        $status = $ex->getMessage();
    }
    return $status;
    print_r($result);
    
}
function checkBalTypeDescrip($bmfType,$bmfDesc){
    global $db;
    try {
        $sql = $db->prepare("select * from BalanceTypeDesrip where bmfType=? and bmfDesc = ?");
        $sql->execute(array($bmfType,$bmfDesc));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        
        if ($sql->rowCount() > 0) {
            $status['status'] = 'true';
           
        } else {
            $status['status'] = 'False';

        }
    } catch (Exception $ex) {
        $status = $ex->getMessage();
    }

    return $status;
    
}
function takeCompany(){
    global $db;
      try {
  
          $sql = $db->prepare("select * from company ");
          $sql->execute();
          $result = $sql->fetchAll(PDO::FETCH_ASSOC);
      } catch (Exception $ex) {
          $result = $ex->getMessage();
      }
      return $result;   
  }
function checkCompany($vatReg){
    global $db;
    try {
        $sql = $db->prepare("select * from company where vat=? ");
        $sql->execute(array($vatReg));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        
        if ($sql->rowCount() > 0) {
            $status['status'] = 'true';
           
        } else {
            $status['status'] = 'False';

        }
    } catch (Exception $ex) {
        $status = $ex->getMessage();
    }

    return $status;
    
}
function checkBal($acc,$bal){
    global $db;
    try {
        $sql = $db->prepare("select * from DebtorsBalances where acc=? and balance = ?");
        $sql->execute(array($acc,$bal));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        
        if ($sql->rowCount() > 0) {
            $status['status'] = 'true';
           
        } else {
            $status['status'] = 'False';

        }
    } catch (Exception $ex) {
        $status = $ex->getMessage();
    }

    return $status;
    
}
function checkSupplier($acc){
    global $db;
    try {
        $sql = $db->prepare("select * from suppliers where account=? ");
        $sql->execute(array($acc));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        
        if ($sql->rowCount() > 0) {
            $status['status'] = 'true';
           
        } else {
            $status['status'] = 'False';

        }
    } catch (Exception $ex) {
        $status = $ex->getMessage();
    }

    return $status;
    
}
function checkOrderTypes($ordType,$ordDescrip)  {
    global $db;
    try {
        $sql = $db->prepare("select * from OrderTypes where ordType=? and ordDescrip = ?");
        $sql->execute(array($ordType,$ordDescrip));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        if ($sql->rowCount() > 0) {
            $status['status'] = 'true';
           
        } else {
            $status['status'] = 'False';

        }
    } catch (Exception $ex) {
        $status = $ex->getMessage();
    }

    return $status;
}

function checkOrderLines($ordNo,$seqNo)  {
    global $db;
    try {
        $sql = $db->prepare("select * from OrderDescription where ordNo=?  and SeqNo = ?");
        $sql->execute(array($ordNo,$seqNo));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        if ($sql->rowCount() > 0) {
            $status['status'] = 'true';
           
        } else {
            $status['status'] = 'False';

        }
    } catch (Exception $ex) {
        $status = $ex->getMessage();
    }

    return $status;
}
//function to send emails
function LaunchEmails($urlEmails)
{
    $options = array(
       CURLOPT_TIMEOUT   => 3,
       CURLOPT_NOSIGNAL  => true,
       CURLOPT_USERAGENT => "Launcher"
    );
    $ch = curl_init($urlOrdTypes);
    curl_setopt_array($ch,$options);
    curl_exec($ch);
 }
 
function  UploadBalTypes($IncCode,$IncomeCodeDesc,$IncType,$LedgerAcc,$BalanceTypeCode,$linkToAcc){
    global $db;
   
      try{
          $stm =  $db->prepare("insert into  BalanceTypes(incCode,incDesc,incType,lgAcc,blCode,lkAcc) values(?,?,?,?,?,?)");
          $stm->execute(array($IncCode,$IncomeCodeDesc,$IncType,$LedgerAcc,$BalanceTypeCode,$linkToAcc));
         $count = $stm->rowCount();
          if ($count > 0) {
              $result["status"] = "ok";
          } else {
              $result["status"] = "fail";
          }
          
      } catch (Exception $ex) {
                 $result["status"]=$ex->getMessage();
      }
      
      return $result;   
  }
  function UploadDetails($acc,$name,$add1,$add2,$add3,$cell,$phone,$email,$idno,$bal){
    global $db;
   
      try{
          $stm =  $db->prepare("insert into  DebtorsBalances(acc,name,addr1,addr2,addr3,cell,phone,email,idno,balance) values(?,?,?,?,?,?,?,?,?,?)");
          $stm->execute(array($acc,$name,$add1,$add2,$add3,$cell,$phone,$email,$idno,$bal));
         $count = $stm->rowCount();
          if ($count > 0) {
              $result["status"] = "ok";
          } else {
              $result["status"] = "Fail";
          }
          
      } catch (Exception $ex) {
                 $result["status"]=$ex->getMessage();
      }
      
      return $result;   
  }
  function InsertActBalanceTypes($bmfType,$bmfDesc,$RecCode){
    global $db;
   
      try{
          $stm =  $db->prepare("insert into  BalanceTypeDesrip(bmfType,bmfDesc,recCode) values(?,?,?)");
          $stm->execute(array($bmfType,$bmfDesc,$RecCode));
         $count = $stm->rowCount();
          if ($count > 0) {
              $result["status"] = "true";
          } else {
              $result["status"] = "False";
          }
          
      } catch (Exception $ex) {
                 $result["status"]=$ex->getMessage();
      }
      
      return $result;   
  }

  function checkInternet() 
  {
  $host_name = 'www.google.com';
  $port_no = '80';
  
  $st = (bool)@fsockopen($host_name, $port_no, $err_no, $err_str, 10);
  if ($st) {
      $nt['status']='connected';
  } else {
      $nt['status']='offline';
  }
      
  return $nt;
  }
  function LaunchPdf($url)
{
   $options = array(
      CURLOPT_TIMEOUT   => 3,
      CURLOPT_NOSIGNAL  => true,
      CURLOPT_USERAGENT => "Launcher"
   );
   $ch = curl_init($url);
   curl_setopt_array($ch,$options);
   curl_exec($ch);
}
function Deleteigl() {
    global $db;
    try {

        $stmt = $db->prepare("delete from budgets");
        $stmt->execute();
        $result = "done";
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}
function syncOrders(){
    global $db;
      try {
  
          $sql = $db->prepare("select * from orders where ordStatus != '' or invStatus = 'x'");
          $sql->execute();
          $result = $sql->fetchAll(PDO::FETCH_ASSOC);
      } catch (Exception $ex) {
          $result = $ex->getMessage();
      }
      return $result;   
  }
  function syncApprovedRequisitionsToPromun(){
    global $db;
      try {
  
          $sql = $db->prepare("select * from requisitions where reqStatus = 'R' or reqStatus = 'X' ");
          $sql->execute();
          $result = $sql->fetchAll(PDO::FETCH_ASSOC);
      } catch (Exception $ex) {
          $result = $ex->getMessage();
      }
      return $result;   
  }
 