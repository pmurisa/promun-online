<?Php
require('../Approvals/pdf/fpdf.php');
require "DBAPI.php";



class myPDF extends FPDF{
    function  header(){
     $this->image('img/axislogo.jpg',10,10,-200);   
     $this->SetFont('Arial','B',14);
     $this->Cell(276,10,'SMS  messages sent Report',0,0,'C');
     $this->Ln();
     $this->SetFont('Times','',12);
     $this->Cell(276,10,'Rate Payer Details',0,0,'C');
     $this->Ln(20);
    }
    function footer(){
        $this->SetY(-15);
        $this->SetFont('Arial','',8);
        $this->Cell(0,10,'Page'.$this->PageNo().'/{nb}',0,0,'C');


    }
    function headerTable(){
        $this->SetFont('Times','B',12);
        $this->Cell(60,10,'Account',1,0,'C');
        $this->Cell(60,10,'Delivered To',1,0,'C');
        $this->Cell(100,10,'Name',1,0,'L');
        $this->Cell(20,10,'Status',1,0,'C');
        $this->Ln();


    }
    function viewTable(){
        $this->SetFont('Times','',10);
        $status = "Submited";
        
       $data =  sentSms();
       foreach($data as $da){
           $acc= @$da['acc'];
           $phone= @$da['phone'];
           $name= @$da['name'];
    $this->Cell(60,10,$acc,1,0,'C');
    $this->Cell(60,10,$phone,1,0,'C');
    $this->Cell(100,10,$name,1,0,'L');
    $this->Cell(20,10,$status,1,0,'C');
    $this->Ln();
       }







    }




}

$pdf = new myPDF(); 
$pdf->AliasNbPages();
$pdf->AddPage('L','A4',0);
$pdf->headerTable();
$pdf->viewTable();
$pdf->SetFont('Arial','B',12);
$pdf->Output('sentMessages.pdf','I');
?>