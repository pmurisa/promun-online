<?php

include('header.php');

require "DBAPI.php";
//require "AutoSendEmails.php";
$passCode = $_SESSION['pCode'];
if(empty($passCode)){
    header ("location:login.php");
}else{
 
    // take comapny details
    $codata = getCompanyDetails();
   
    if(empty($codata)){

      echo "<script>alert('Please setup Your company details first'); location='adminLogin.php'</script>";  
    }
    //sync all emails to gmail
    else{
     
        $UserData = getUserDetails($passCode);
        $lockUser = @$UserData[0]['lockUser'];
        $maxAuth = @$UserData[0]['maxAuth'];
        $user = @$UserData[0]['passCode'];
        $name =@$codata[0]["description"];
        $vat =@$codata[0]["vat"];
        $ipAddress= @$codata[0]["ip"];
        $email =@$codata[0]["email"];
        $status =@$codata[0]["status"];
        $reason =@$codata[0]["reason"];
        $info = countOrders($passCode);
        $num_orders = @$info['result'];
        $json = $ipAddress;
        $json = json_encode($json);
       
        
        
       //check LA status
       if($status != "active"){

        echo "<script>alert('Notice Your Municipality was blocked reason $reason '); location='login.php'</script>";
       }
     
        //Count the number of new requisitions per user
        $count = countNewRequisitions($passCode);
        $new_Requisitions = @$count[0]['todayRequisitions']; 
       
        if(empty($new_Requisitions)){
          $new_Requisitions = 0;
        } 
         //Count the number of approved requisitions per user
        $countApproved = countApprovedRequisitions($passCode);
        $approved_Requisitions = @$countApproved[0]['approvedRequisitions']; 
        if(empty($approved_Requisitions)){
          $approved_Requisitions = 0;
        }
         //Count the number of new orders per user
        $count_orders = countNewOrders($passCode);
        $new_orders = @$count_orders[0]['newOrders'];
        if(empty($new_orders)){
          $new_orders = 0;
        }
         //Count the number of approved orders per user
        $count_approved = countApprovedOrders($passCode);
        $approved_orders = @$count_approved[0]['approvedOrders'];
        if(empty($approved_orders)){
          $approved_orders = 0;
        }
         //Count the number of rejected requisitions per user
        $count_rejected = countRejectedRequisitions($passCode);
        $rejected_Requisitions = @$count_rejected[0]['todayRequisitions']; 
        if(empty($rejected_Requisitions)){
          $rejected_Requisitions = 0;
        } 
         //Count the number of complete orders per user
        $count_complete_orders = countCompleteOrders($passCode);
        $complete_orders = @$count_complete_orders[0]['completeOrders']; 
        if(empty($complete_orders)){
          $complete_orders = 0;
        } 
        
        
        /*set_time_limit(100); // make it run forever
        while(!sleep(20)) {
            autoSend();
            sleep(10);
        }*/
        
    }
    
     
     

    

}






?>
<html>
  <head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style>

  /* Button used to open the contact form - fixed at the bottom of the page */
.open-button {
  background-color: #555;
  color: white;
  padding: 16px 20px;
  border: none;
  cursor: pointer;
  opacity: 0.8;
  position: fixed;
  bottom: 23px;
  right: 28px;
  width: 280px;
}

/* The popup form - hidden by default */
.form-popup {
  
  position:fixed;
  top: 400px;
  right: 150px;
  border: 3px solid #f1f1f1;
  border-radius:15px;
  z-index: 9;
}

/* Add styles to the form container */
.form-container {
  max-width: 200px;
  padding: 10px;
  background-color:white;
  
}

/* Full-width input fields */
.form-container input[type=number], .form-container input[type=text] {
  width: 60px;
  padding: 15px;
  margin: 5px 0 22px 0;
  border: none;
  background: #f1f1f1;
}

/* When the inputs get focus, do something */
.form-container input[type=number]:focus, .form-container input[type=text]:focus {
  background-color: #ddd;
  outline: none;
}

/* Set a style for the submit/login button */
.form-container .btn {
  background-color: #4CAF50;
  color: white;
  padding: 16px 20px;
  border: none;
  cursor: pointer;
  width: 100%;
  margin-bottom:10px;
  opacity: 0.8;
}

/* Add a red background color to the cancel button */
.form-container .cancel {
  background-color: red;
}

/* Add some hover effects to buttons */
.form-container .btn:hover, .open-button:hover {
  opacity: 1;
}
.input{
  font-size:10px;
  border-radius:4px;
  
}
  </style>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
    
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['New Requsitions',     <?php echo $new_Requisitions;?>],
          ['New Orders',   <?php echo $new_orders;?>   ],
          ['Approved Orders',   <?php echo $approved_orders;?>   ],
          ['Authorised Requisitions',   <?php echo $approved_Requisitions;?>],
          ['Rejected Requisitions', <?php echo   $rejected_Requisitions;?>],
          ['Orders Complete',    7]
        ]);

        var options = {
          title: 'Your DashBoard Statics',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
        chart.draw(data, options);
      }
    </script>
       <script>
    function openForm() {
  document.getElementById("myForm").style.display = "block";
}

    function closeForm() {
  document.getElementById("myForm").style.display = "none";
}
    </script>
 
  </head>
  
 


 
<tr>
    <td colspan="2">
       <table border="1" class="table-bordered" width="100%" style="background:#002F74;margin: 0;background-repeat: no-repeat;background-attachment: fixed; border-radius:20px; border-color:white;">
          <tr>
             <td width="20%" style="background-color: white" valign="top">
				 <?php
                    include('approvaldashboard.php');
                 ?>
             </td>
             <td width="80%" valign="top"><br>
            <table class="table-stripped" border="0" width="98%" align="center"  >
            <td>
            <a href="#"><img src="img/company.jpg" style="margin-left:10%;margin-top:10px; " class="img-circle" width="60" height="60"></a>
            <p class="well" style="width:fit-content; height:fit-content; font-size:10px; margin-left:10%; align-text:centre;"><?php echo $name; ?></p>
            <a href="#"><img src="img/vat.jpg" style="float-left; margin-left:60%;margin-top:10px;  " class="img-circle" width="50" height="50"></a>
            <p class="well" style="width:fit-content; height:fit-content; font-size:10px; margin-left:60%;"><?php echo $vat; ?></p>
            </td>
            <td>
            <p style="float-left; margin-left:30%;" id="piechart_3d" style="width:fit-content; height: fit-content;"></p>
            
            </td>
            </tr>
      
            <tr>
            <td>
            
            
            </td>
          </tr>
      </table>
     
      
    </td>
</tr>
<?php
include('footer1.php');
?>
<script>
             setInterval(function ()
                {
                                       $.getJSON("../DB/index.php",
                            function (resp) {
                     
                                if (resp.status === 'ok' || resp.resp === 'ok')
                                {
                                 
                                    //alert("Emails up to date");
                                 
                                }
                            });
                }, 1000);//time in milliseconds
              
               
                
                function reloadPage(){
                  location.reload(true);
                                      }
  </script>

