<?php
require "DBAPI.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
/*Sidemenu*/
.menu 
{
	list-style:none;
}
.menu li ul 
{
	list-style:none;
}
.menu li ul li 
{
	display:block; 
	height:40px; 
	border-bottom:solid 1px #E4D9D9;
}
.menu li ul a, .menu li ul a:visited, .menu li ul a:active {
	display:block; 
	height:40px; 
	padding:5px 5px 0px 5px; 
	text-decoration:none; 
	color: blue;
	font-size: 13px;
	}
.menu li ul  a:hover {
	background: white;
	color: brown;
	font-weight: bold;
	border-bottom:solid 1px #E4D9D9;
}
.menu h2 {
	display:block;
	border-bottom:solid 2px brown;
	padding:5px;
	margin:0px 0px 0px 0px;
	font-family: Arial, Helvetica, sans-serif;
	font-size:18px;
	color:white;
	font-weight: bolder;
}
</style>
<title>Approve Requisitions</title>

<!------------------------------------------------------------------------- Bootstrap --------------------------------------------------------------->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
<link rel="icon" href="img/axislogo.jpg">
</head>
<body><br>
<form method="post" style="  box-shadow :1px 1px 20px gray; width: 95%; border-radius: 10px 10px 10px; margin:auto">
<table border="0" width="90%" align="center" class="table-responsive" style="border-color:gray">
<tbody>
          <tr>
        	<td colspan="2"  height="50" align="center">
            	<table border="0" width="90%" align="center" class="table-responsive" style="border-color:gray">
                	<tr>
                    	<td align="center">
                        	<img src="img/gweru.jpg" width="100" height="100" class="img-responsive">
                        </td>
                        <td width="80%" align="center">
                        	<h3 align="center" style="color: blue"><strong>E-Promun System</strong></h3>
							<p style="line-height: 20px">Zimbabwe <br> <label style="color:#FF0000">"Your Order, Our Commitment"</label></p>
                        </td>
                        <td align="center">
                        	<img src="img/download.jpg" width="90" height="90" class="img-responsive">
                        </td>
                    </tr>
                </table>
            </td>
          </tr>
		  <tr>
        	<td colspan="2" height="5" style="background-color: #E4D9D9">
            </td>
          </tr>
          <tr>
          	<td colspan="2">
          		<table width="100%" border="0">
                	<tr>
                        <td width="35%">
                        <h5 style=" color:#B32C0E; font-weight: bolder">
                            <strong>Welcome to Approvals Platform</strong></h5>
                        </td>
                        <td align="right" style="color: #0000FF" width="35%">
                            <h5 style=" color: blue"><a href="profile.php"><strong>Click here to go Home</strong></a></h5>
                         </td>
                     </tr>
                </table>
             </td>
          </tr>
         <tr>
        	<td colspan="2">
            	<table border="1" class="table-bordered" width="100%">
                	<tbody>
                    	<tr>
                        	<td width="25%" style="background-color: white" valign="top">
                            	<?php
									include('approvaldashboard.php');
								?>
                            </td>
                            <td width="75%" valign="top">
                            	 <table width="100%" border="0" class="table-striped" align="center">
                                	 <tr><td height="10" align="center" colspan="5"></td></tr>
                                     <tr>
                                     	<td colspan="5" align="center">
        <table class="table-stripped" border="0" width="95%" align="center">
            <tr>
     			<td width="95%">
                <input type="password" class="form-control input-sm col-sm-10" placeholder="Enter Your Vendor Number" name="txtdetails">
                </td>
                <td width="5%"><button type="submit" name="btnsearch" class="btn btn-default btn-sm glyphicon glyphicon-search"></button></td>
            </tr>
         </table>
                                     	</td>
                                     </tr>
                                      <tr>
                                     	<td colspan="5" align="center" height="34"></td>
                                      </tr>
                                      <tr>
                                     	<td colspan="5" align="center">
                                        <?php
                                       
                                        $requisitions = getRequisitions();
                                        //print_r($requisitions);
										foreach($requisitions as $pr){
                                            $reqNo = @$pr["req-no"];
                                            $reqDate = @$pr["req-date"];
                                            $reqDesc = @$pr["req-desc"];
                                            $reqCode = @$pr["pass-code"];
                                            $authCode = @$pr["auth-code"];
                                            $orderType = @$pr["ord-type"];
                                            $reqType = @$pr["req-type"];
                                            $reqStatus = @$pr["req-status"];
                                            $authNumber =  @$pr["auth-number"];
                                        
						
                                echo "<tr>";
                                 echo "<td width='10%' valign='middle' align='center'><img src='img/axislogo.jpg' width = '50' height = '50'></td>";
                                    echo "<td width='90%'>";
                                        echo "<table width = '100%' class='table-responsive' border='0'>";
                                            echo "<tr>";
                                                echo "<td width='12%' style='font-size: 12px'><strong>"."Requisition Number:"."</strong></td>";
                                                echo "<td style='font-size: 12px' style='color:blue'>".$reqNo."</td>";
                                            echo "</tr>";
											
                                            echo "<tr>";
                                                echo "<td width='40%' style='font-size: 12px'><strong>"."Description:"."</strong></td>";
                                  				echo "<td style='font-size: 12px'>".$reqDesc."</td>";
                                            echo "</tr>";
                                            echo "<tr>";
                                                echo "<td width='40%' style='font-size: 12px'><strong>"."Order Type:"."</strong></td>";
                                       			echo "<td style='font-size: 12px'>".$orderType."</td>";
                                            echo "</tr>";
                                            echo "<tr>";
                              			 echo "<td width='40%' style='font-size: 12px'><strong>"."Requisition Type:"."</strong></td>";
                                       	echo "<td style='font-size: 12px'>".$reqType."</td>";
                                            echo "</tr>";
											 echo "<tr>";
                              			 echo "<td width='40%' style='font-size: 12px'><strong>"."Date Requested"."</strong></td>";
                                       	echo "<td style='font-size: 12px'>".$reqDate."</td>";
                                            echo "</tr>";
											 echo "<tr>";
                              			 echo "<td width='40%' style='font-size: 12px'><strong>"."Status:"."</strong></td>";
                                       	echo "<td style='font-size: 12px'>".$reqStatus."</td>";
                                            echo "</tr>";
                                            echo "<tr>";
			                                 
                                            echo "<td>";
                                            echo "<td><a href='notices.php?req-no=".@$pr['req-no']."' class='btn btn-default btn-xs'>"."APPROVE"."</a></td>";
                                            echo "<td><a href='notices.php?req-no=".@$pr['req-no']."' class='btn btn-default btn-xs'>"."DECLINE"."</a></td>";
                                            echo "</td>";
                                            "</tr>";
											
                                        echo "</table>";
                                        echo "</td>";
                                            echo "</tr>";
                                            echo "<tr>";
                                                echo "<td width='10%' valign='middle' align='center' colspan='2'><hr></td>";;
                                            echo "</tr>";
                                            echo "<tr>";
                                            echo "<td width='10%' valign='middle' align='center' colspan='2'><hr></td>";;
                                    echo "</tr>";
                                    echo "<tr>";
			
			
                                    echo "</td>";
                                    echo "<td>";
                                    /*echo "<td><a href='notices.php?req-no=".$pr['req-no']."' class='btn btn-default btn-xs'>"."DEACTIVATE"."</a></td>";*/
                                    
                                    echo "</td>";
                                    "</tr>";
                                    echo"<tr>";
                                    echo"<td>";
                
                                    if(isset($_GET['req-no']))
                                    { $req = $_GET['req-no'];
                        
                                     //mysqli_query($con,"DELETE FROM complains  where phone = '$phone'");
                                    //mysql_query("UPDATE documentation set status = 'deactivated' where upn = '$upn'");
                                     echo "<script>alert('APPROVED SUCCESSFULLY'); location='notices.php'</script>";
                                     }
                                    echo"</td>";
                                    echo"</tr>";

                    
                                        }
										?>
                                     	</td>
                                     </tr>
                                  </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
		</table>
        <table border="0" class="table-responsive" width="90%" align="center">
        <tbody>
        <?php
			include('footer.php'); 
		?>
