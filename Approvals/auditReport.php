<?Php
require('/pdf/fpdf.php');
require "DBAPI.php";



class myPDF extends FPDF{
    function  header(){
     $this->image('img/axislogo.jpg',10,10,-200);   
     $this->SetFont('Arial','B',14);
     $this->Cell(276,10,'Municipality Audit Report',0,0,'C');
     $this->Ln();
     $this->SetFont('Times','',12);
     $this->Cell(276,10,'Actions Details',0,0,'C');
     $this->Ln(20);
    }
    function footer(){
        $this->SetY(-15);
        $this->SetFont('Arial','',8);
        $this->Cell(0,10,'Page'.$this->PageNo().'/{nb}',0,0,'C');


    }
    function headerTable(){
        $this->SetFont('Times','B',12);
        $this->Cell(20,10,'User',1,0,'C');
        $this->Cell(60,10,'Action Perfomed',1,0,'L');
        $this->Cell(60,10,'Account Linked',1,0,'C');
        $this->Cell(60,10,'Date',1,0,'C');
        $this->Cell(60,10,'Time',1,0,'C');
        
        $this->Ln();


    }
    function viewTable(){
        $this->SetFont('Times','B',8);
      
        
       $data =  audit();
       foreach($data as $da){
           $user= @$da['id'];
           $action= @$da['action'];
           $account= @$da['account'];
           $date= @$da['date'];
           $time= @$da['time'];
    $this->Cell(20,10,$user,1,0,'C');
    $this->Cell(60,10,$action,1,0,'C');
    $this->Cell(60,10,$account,1,0,'L');
    $this->Cell(60,10,$date,1,0,'C');
    $this->Cell(60,10,$time,1,0,'C');
    $this->Ln();
       }







    }




}

$pdf = new myPDF(); 
$pdf->AliasNbPages();
$pdf->AddPage('L','A4',0);
$pdf->headerTable();
$pdf->viewTable();
$pdf->SetFont('Arial','B',12);
$pdf->Output('sentMessages.pdf','I');
?>