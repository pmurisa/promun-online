<?php

require "DBAPI.php";
$tot = 0;
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Individual Requisition Report</title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
</head>
<body><br>
<form method="post" style="  box-shadow :1px 1px 20px gray; width: 60%; border-radius: 10px 10px 10px; margin:auto">
<table width="100%" align="center" border="0">
	<tr>
    
    	<td align="center" height="25"></td>
      
    </tr>
 
    
    <tr>
    	<td align="center">
        	<?php
                $details =  ReportReqView();
                $reqNo = @$details[0]["reqNo"];
                
				
                $take = ReportRequisitionView($reqNo);
                

                
                if(!empty($take)){
                    foreach($take as $pr){
                        $reqNo = @$pr["reqNo"];
                        $reqDate = @$pr["reqDate"];
                        $reqDesc = @$pr["reqDesc"];
                        $reqCode = @$pr["passCode"];
                        $captCode = @$pr["captCode"];
                        $authCode = @$pr["authCode"];
                        $ordType = @$pr["ordType"];
                        $reqType = @$pr["reqType"];
                        $reqStatus = @$pr["reqStatus"];
						$authNumber =  @$pr["authNumber"];
						$reqAuthNumber =  @$pr["reqAuthNumber"];
                        //get requisition lines
						$lines =  getRequisitionLines($reqNo);
						
                        //get Order types
                        $description = getOrderTypes($ordType);
                        $req_name = @$description[0]["ordDescrip"];
                        //get order details
						$reqDetail =  getReqDetails($reqNo);
                        foreach($reqDetail as $reqDetails){
                            $seq = @$reqDetails["SeqNo"];
                            $dec = @$reqDetails["descrip"];
                            $alloc = @$reqDetails["alloc"];
                            $qty = @$reqDetails["qty"];
                            $total = @$reqDetails["amt"];
                           //calculate order totals
                            $tot = $tot + $total;
                        }
                        if($reqStatus == "*"){
                            $reqStatus = "UnAuthorised";
                        }
                        if($reqStatus == "R"){
                            $reqStatus = "Approved";
                        }
                        if($reqStatus == "C"){
                            $reqStatus = "Cancelled";
                        }
                        if($reqStatus == "x"){
                            $reqStatus = "Rejected";
        
                        }
                        if($reqType == "N"){
                            $reqType = "Non Stock";
        
                        }
                        if($reqType == "O"){
                            $reqType = "Out Of stock";
        
                        }
                        if($reqType == "I"){
                            $reqType = "Into Stock";
        
                        }
                      
			
						echo "<table width='90%'>";
							echo "<tr>";
								echo "<td>";
									echo "<div class='panel panel-primary'>";
										echo "<table width='95%' align='center'>";
											echo "<tr>";
												echo "<td valign='middle'><img src='img/National court of arms.jpg' width='60' height='60'></td>";
												echo "<td align='center'>";
													echo "<table width='100%' align='center'>";
					echo "<tr><td align='center'><label style='color: blue; font-size: 14px'>E-PROMUN</label></td></tr>";
														echo "<tr><td align='center'><label style='color: brown'>REQUISITION REPORT</label></td></tr>";
													echo "</table>";
												echo "</td>";
												echo "<td valign='middle'><img src='img/National court of arms.jpg' width='60' height='60'></td>";
											echo "</tr>";
											echo "<tr>";
												echo "<td height='3' colspan='3' bgcolor='white'></td>";
											echo "</tr>";
											echo "<tr>";
												echo "<td height='3' colspan='3' bgcolor='brown'></td>";
											echo "</tr>";
											echo "<tr>";
												echo "<td height='3' colspan='3' bgcolor='white'></td>";
											echo "</tr>";
											echo "<tr>";
												echo "<td height='3' colspan='3' bgcolor='white'>";
													echo "<table width='100%' class='table-responsive'>";
															echo "<tr>";
																echo "<td width='15%'><strong>Req No</strong></td>";
																echo "<td width='35%'>".": ".$reqNo."</td>";
																echo "<td width='15%'><strong>Description</strong></td>";
																echo "<td width='35%'>".": ".$reqDesc."</td>";
															echo "</tr>";
															echo "<tr>";
																echo "<td width='15%'><strong>Date</strong></td>";
																echo "<td width='35%'>".": ".$reqDate."</td>";
																echo "<td width='15%'><strong>Order Type</strong></td>";
																echo "<td width='35%'>".": ".$req_name."</td>";
															echo "</tr>";;
															
													echo "</table>";
												echo "</td>";
											echo "</tr>";
											echo "<tr>";
												echo "<td colspan='3'>";
													echo "<table width='100%' class='table-bordered'>";
														echo "<tr>";
															echo "<td width='50%'><strong>ITEM</strong></td>";
															echo "<td width='50%'><strong>DESCRIPTION</strong></td>";
															echo "<td width='715%'></td>";
														echo "</tr>";
														echo "<tr>";
															echo "<td width='50%'>"."Description"."</td>";
                                                             
                                                            echo "<td width='50%'>".$dec."</td>";     
															echo "<td width='715%'></td>";
														echo "</tr>";
														echo "<tr>";
															echo "<td width='50%'>"."Allocation"."</td>";
															echo "<td width='50%'>".$alloc."</td>";
															echo "<td width='715%'></td>";
														echo "</tr>";
														
															echo "<tr>";
															echo "<td width='50%'>"."Quantity"."</td>";
                                                            echo "<td width='50%'>".$qty."</td>";
                                                            
															echo "<td width='715%'></td>";
														echo "</tr>";
														echo "<tr>";
														echo "<td width='50%' >"."Req Status"."</td>";
		
														echo "<td width='50%' >".$reqStatus."</td>";
														echo "<td width='715%'></td>";
													echo "</tr>";
													
												echo "<tr>";
													echo "<td width='50%' >"."User To Approve"."</td>";
	
													echo "<td width='50%'>".$authCode."</td>";
													echo "<td width='715%'></td>";
												echo "</tr>";
												echo "<tr>";
													echo "<td width='50%' >"."Requested By"."</td>";
	
													echo "<td width='50%'>".$reqCode."</td>";
													echo "<td width='715%'></td>";
                                                echo "</tr>";
                                                if(!empty($reqAuthNumber)){
                                                echo "<tr>";
                                                echo "<td width='50%' >"."Authorisation Number"."</td>";

                                                echo "<td width='50%'>".$reqAuthNumber."</td>";
                                                echo "<td width='715%'></td>";
                                            echo "</tr>";
                                                }
														echo "<tr>";
															echo "<td width='50%' style='color: red'>"."Total Cost"."</td>";
			
															echo "<td width='50%' style='color: red'>"."$".$tot."</td>";
															echo "<td width='715%'></td>";
														echo "</tr>";
													
														
														
													echo "</table>";
												echo "</td>";
											echo "</tr>";
										echo "</table>";
										echo "</br>";
									echo "</div";
								echo "</td>";
							echo "</tr>";
						echo "</table>";
					}
				}
			?>
        </td>
    </tr>
    <tr>
    	<td align="center" height="5">
       
            <button type="submit" class="btn btn-default btn-xs" name="btnconfirm" style="color: #008000; border-color:#008000; text-decoration: none;"><a href="ReportsHome.php">Back</a></button>
             <button type="submit" class="btn btn-alert btn-xs" name="btnprint" style="color: #008000; border-color:#008000; text-decoration: none;" onClick="window.print()">Print</button>
            <br><br>
           
        </td>
    </tr>
    <tr>
</table>
</form>
</body>
</html>
<?php
include('footer1.php');
?>
