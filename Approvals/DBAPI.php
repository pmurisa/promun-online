<?php
require "STARTDBAPI.php";
$server = $_SESSION['server'];
$db = $_SESSION['dbname'];
$dbuser = $_SESSION['username'];
$dbpass = $_SESSION['dbpassword'];

//$dbn = new PDO("sqlsrv:Server=sql5031.site4now.net;Database=DB_A33C8A_epromun", "DB_A33C8A_epromun_admin", "12BElvedere");
try{
$dbn = new PDO("sqlsrv:Server=$server;Database=$db", "$dbuser", "$dbpass");
$dbn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch (Exception $ex){
    echo "<script>alert('Could not database ($db)user($dbuser)password($dbpass) for Local Authority');</script>";
    die();
}

function Is_Logged_In() {
    if (isset($_SESSION['pCode'])) {
        return true;
    }
}

function logoutadmin() {
    session_destroy();
    unset($_SESSION['pCode']);
    unset($_SESSION['server']);
    unset($_SESSION['dbname']);
    unset($_SESSION['username']);
    unset($_SESSION['dbpassword']);
    return true;
}

function logout() {
    session_destroy();
    unset($_SESSION['pCode']);
    unset($_SESSION['server']);
    unset($_SESSION['dbname']);
    unset($_SESSION['username']);
    unset($_SESSION['dbpassword']);
    return true;
}
function allocation() {
    session_destroy();
    unset($_SESSION['Alloc']);
    return true;
}


function UserLogin($passCode,$passWord) {
    //load users before trying to login
    //$urlUsers = "http://localhost/ePromun/DB/SyncUsers.php";
    global $dbn;
    //LaunchUsers($urlUsers);
    try {
        $sql = $dbn->prepare("select * from users where passCode = ? and passWord = ?");
        $sql->execute(array($passCode,$passWord));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        if ($sql->rowCount() > 0) {
            $_SESSION['pCode'] = @$result[0]['passCode'];
            $status['status'] = 'passed';
            header ("location:LoadContent.php");
           
        } else {
            $status['status'] = 'Failed';
            echo "<script>alert('Error: Invalid Login Credentials '); location='login.php'</script>";

           
        }
    } catch (Exception $ex) {
        $status['status'] = $ex->getMessage();
    }

    return $status;
}
function adminLogin($passCode,$passWord) {
    
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from users where passCode = ? and passWord = ?");
        $sql->execute(array($passCode,$passWord));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        print_r($result);
        if ($sql->rowCount() > 0) {
            $_SESSION['passCode'] = @$result[0]['passCode'];
            $_SESSION['isAdmin'] = @$result[0]['isAdmin'];
            $pass = @$result[0]['passWord'];
            $user = @$result[0]['passCode'];
            $status['status'] = 'passed';
            if($user == "axis" && $pass == "12AXissolutions"){
                
            header ("location:axisHome.php");
            }
           else{
            header ("location:adminHome.php");
           }
            
            
        } else {
            $status['status'] = 'Failed';
            echo "<script>alert('Error: Invalid Login Credentials'); location='adminLogin.php'</script>";
           
        }
    } catch (Exception $ex) {
        $status['status'] = $ex->getMessage();
      
    }

    return $status;
}

function getUserDetails($passCode) {
    global $dbn;

    try {

        $sql = $dbn->prepare('select * from users where passCode=?');
        $sql->execute(array( $passCode));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }

    return $result;
}
function getStkCodeInfo($stk,$whse) {
    global $dbn;

    try {

        $sql = $dbn->prepare('select * from wareHouseDetails where stkCode=? and whse = ?');
        $sql->execute(array( $stk,$whse));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }

    return $result;
}
function getStock() {
    global $dbn;

    try {

        $sql = $dbn->prepare('select * from wareHouseDetails');
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }

    return $result;
}
function getStoresMen() {
    global $dbn;

    try {

        $sql = $dbn->prepare("select * from users where isAdmin = 'stores' ");
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }

    return $result;
}
function getTotalStkCode() {
    global $dbn;

    try {

        $sql = $dbn->prepare('select * from wareHouseDetails');
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }

    return $result;
}
function noEmails() {
    global $dbn;

    try {

        $sql = $dbn->prepare("select * from DebtorsBalances where convert(varchar, email) = '' ");
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }

    return $result;
}
function getstk() {
    global $dbn;

    try {

        $sql = $dbn->prepare('select * from wareHouseDetails ORDER BY stkCode ASC');
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }

    return $result;
}
function getStkView() {
    global $dbn;

    try {

        $sql = $dbn->prepare('select * from whseView');
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }

    return $result;
}
 

function getCompanyDetails(){
    global $dbn;
      try {
  
          $sql = $dbn->prepare('select * from company');
          $sql->execute();
          $result = $sql->fetchAll(PDO::FETCH_ASSOC);
      } catch (Exception $ex) {
          $result = $ex->getMessage();
      }
      return $result;   
  }
  
function CompanyDetails(){
    global $dbn;
      try {
  
          $sql = $dbn->prepare('select * from company');
          $sql->execute();
          $result = $sql->fetchAll(PDO::FETCH_ASSOC);
      } catch (Exception $ex) {
          $result = $ex->getMessage();
      }
      return $result;   
  }
  function getRequisitions($passCode){
    global $dbn;
      try {
  
          $sql = $dbn->prepare("select * from requisitions where reqStatus = '*' and authCode = ? ");
          $sql->execute(array($passCode));
          $result = $sql->fetchAll(PDO::FETCH_ASSOC);
      } catch (Exception $ex) {
          $result = $ex->getMessage();
      }
      return $result;   
  }
  function getRequisitionsApproved($passCode){
    global $dbn;
      try {
  
          $sql = $dbn->prepare("select * from requisitions where reqStatus = 'R' and authCode = ? ORDER BY reqNo ASC ");
          $sql->execute(array($passCode));
          $result = $sql->fetchAll(PDO::FETCH_ASSOC);
      } catch (Exception $ex) {
          $result = $ex->getMessage();
      }
      return $result;   
  }
  function getRequisitionsComplete($passCode){
    global $dbn;
      try {
  
          $sql = $dbn->prepare("select * from requisitions where reqStatus = 'C' and authCode = ? ORDER BY reqNo ASC ");
          $sql->execute(array($passCode));
          $result = $sql->fetchAll(PDO::FETCH_ASSOC);
      } catch (Exception $ex) {
          $result = $ex->getMessage();
      }
      return $result;   
  }
  function getRequisitionsOnHold($passCode){
    global $dbn;
      try {
  
          $sql = $dbn->prepare("select * from requisitions where reqStatus = 'H'and authCode = ? ORDER BY reqNo ASC ");
          $sql->execute(array($passCode));
          $result = $sql->fetchAll(PDO::FETCH_ASSOC);
      } catch (Exception $ex) {
          $result = $ex->getMessage();
      }
      return $result;   
  }
  function getRequisitionsRejected($passCode){
    global $dbn;
      try {
  
          $sql = $dbn->prepare("select * from requisitions where reqStatus = 'x' and authCode = ? ");
          $sql->execute(array($passCode));
          $result = $sql->fetchAll(PDO::FETCH_ASSOC);
      } catch (Exception $ex) {
          $result = $ex->getMessage();
      }
      return $result;   
  }
  
  function getNewRequisition($reqNo) {
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from requisitions where reqNo =? ");
        $sql->execute(array($reqNo));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        //print_r($result);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function countNewRequisitions($passCode){
    global $dbn;
    //$result=array();
   try {
       $sql = $dbn->prepare("select sum(total) as todayRequisitions from requisitions where authCode=? and reqStatus = '*'");
       $sql->execute(array($passCode));
       $result = $sql->fetchALL(PDO::FETCH_ASSOC);
      
  } catch (Exception $ex) {
      $result =  $ex->getMessage();
   }

   return $result;
}
//calculate total amount tied up in debtors
function getTotalDebt() {
    global $dbn;
    try {
        $sql = $dbn->prepare("select sum(balance) as totalDebt from  DebtorsBalances");
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        //print_r($result);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function countNewOrders($passCode){
    global $dbn;
    //$result=array();
   try {
       $sql = $dbn->prepare("select sum(total) as newOrders from orders where authCode=? and ordStatus = ''");
       $sql->execute(array($passCode));
       $result = $sql->fetchALL(PDO::FETCH_ASSOC);
      
  } catch (Exception $ex) {
      $result =  $ex->getMessage();
   }

   return $result;
}
function countApprovedOrders($passCode){
    global $dbn;
    //$result=array();
   try {
       $sql = $dbn->prepare("select sum(total) as approvedOrders from orders where authCode=? and ordStatus != ''");
       $sql->execute(array($passCode));
       $result = $sql->fetchALL(PDO::FETCH_ASSOC);
      
  } catch (Exception $ex) {
      $result =  $ex->getMessage();
   }

   return $result;
}
function countCompleteOrders($passCode){
    global $dbn;
    //$result=array();
   try {
       $sql = $dbn->prepare("select sum(total) as completeOrders from orders where authCode=? and invStatus = 'i'");
       $sql->execute(array($passCode));
       $result = $sql->fetchALL(PDO::FETCH_ASSOC);
      
  } catch (Exception $ex) {
      $result =  $ex->getMessage();
   }

   return $result;
}
function countApprovedRequisitions($passCode){
    global $dbn;
    //$result=array();
   try {
       $sql = $dbn->prepare("select sum(total) as approvedRequisitions from requisitions where authCode=? and reqStatus = 'R'");
       $sql->execute(array($passCode));
       $result = $sql->fetchALL(PDO::FETCH_ASSOC);
      
  } catch (Exception $ex) {
      $result =  $ex->getMessage();
   }

   return $result;
}
function countRejectedRequisitions($passCode){
    global $dbn;
    //$result=array();
   try {
       $sql = $dbn->prepare("select sum(total) as rejectedRequisitions from requisitions where authCode=? and reqStatus = 'x'");
       $sql->execute(array($passCode));
       $result = $sql->fetchALL(PDO::FETCH_ASSOC);
      
  } catch (Exception $ex) {
      $result =  $ex->getMessage();
   }

   return $result;
}
function checkRequisitions($reqNo) {
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from requisitionLines where reqNo =? ");
        $sql->execute(array($reqNo));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;  //here
}
function getRequisitionLines($reqNo) {
    global $dbn;
    try {
        $sql = $dbn->prepare("select sum(amt) as totalAmt from requisitionLines where reqNo=? ");
        $sql->execute(array($reqNo));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        //print_r($result);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function  getRequisitionL($reqNo){
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from requisitionLines where reqNo=? ");
        $sql->execute(array($reqNo));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        //print_r($result);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function  getAllRatePayer(){
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from DebtorsBalances ");
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        
        
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function  getPayer($acc){
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from DebtorsBalances where acc=? ");
        $sql->execute(array($acc));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        if ($sql->rowCount() > 0) {
            $status['status'] = 'passed';
        } else {
            $status['status'] = 'Failed';
        }
        
        
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $status;
}
function  getAllRatePayerArrears(){
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from DebtorsBalances ");
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        
        
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function getSingleRatePayerArrears($acc){
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from DebtorsBalances where acc = ? ORDER BY acc ASC");
        $sql->execute(array($acc));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        
        
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function approveRequisition($reason,$actual_date,$authNumber,$date,$reqNo) {
    global $dbn;
    try {
        $sql = $dbn->prepare("update requisitions set reqStatus='R',authReason=?,reqAuthNumber=?,authNumber=?,authDate=? where reqNo=?");
        $sql->execute(array($reason,$actual_date,$authNumber,$date,$reqNo));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        //print_r($result);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function rejectOrders($passCode,$reqNo) {
    global $dbn;
    try {
        $sql = $dbn->prepare("update orders set invStatus='x',ordStatus=? where ordNo=?");
        $sql->execute(array($passCode,$reqNo));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        //print_r($result);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function updatePartiallySql($reason,$auth2,$reqNo){
    global $dbn;
    try {
        $sql = $dbn->prepare("update requisitions set reqStatus='*',authReason=?,authCode=? where reqNo=?");
        $sql->execute(array($reason,$auth2,$reqNo));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        //print_r($result);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function saveSMTP($host,$port,$username,$password,$codeLA){
    global $dbn;
    try {
        $sql = $dbn->prepare("update company set host=?,port=?,username=?,emailPassword=? where code=?");
        $sql->execute(array($host,$port,$username,$password,$codeLA));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        //print_r($result);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function approveOrder($passCode,$reqNo) {
    global $dbn;
    try {
        $sql = $dbn->prepare("update orders set ordStatus=? where ordNo=?");
        $sql->execute(array($passCode,$reqNo));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function updateCo($email,$bCode,$bank,$sms,$smspwd,$smsCredit,$api,$code ){
    global $dbn;
    try {
        $sql = $dbn->prepare('update company set email=?,bCode=?, bank=?,smsAccount=?,password=?, smsCredit=? ,api=? where code = ? ');
        $sql->execute(array($email,$bCode,$bank,$sms,$smspwd,$smsCredit,$api,$code));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
      
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function updateServer($new,$old ){
    global $dbn;
    try {
        $sql = $dbn->prepare('update company set ip=? where ip=?');
        $sql->execute(array($new,$old));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        if ($sql->rowCount() > 0) {
            $_SESSION['serverIP'] = @$result[0]['ip'];
        } else {
           
           
        }
       
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function ipExist($old){
    global $dbn;
    try {
        $sql = $dbn->prepare('select * from company where ip=?');
        $sql->execute(array($old));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        if ($sql->rowCount() > 0) {
            $status['status'] = 'passed';
        } else {
            $status['status'] = 'Failed';
        }
        
    } catch (Exception $ex) {
        $status['status'] = $ex->getMessage();
    }

    return $status;
}
function checkUser($passCode){
    global $dbn;
    try {
        $sql = $dbn->prepare('select * from users where passCode=? ');
        $sql->execute(array($passCode));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        if ($sql->rowCount() > 0) {
            $status['status'] = 'passed';
        } else {
            $status['status'] = 'Failed';
        }
        
    } catch (Exception $ex) {
        $status['status'] = $ex->getMessage();
    }

    return $status;
}
function saveEmail($email,$acc){
    global $dbn;
    try {
        $sql = $dbn->prepare("update DebtorsBalances set email=? where acc=?");
        $sql->execute(array($email,$acc));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        //print_r($result);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function savePhone($phone,$acc){
    global $dbn;
    try {
        $sql = $dbn->prepare("update DebtorsBalances set cell=? where acc=?");
        $sql->execute(array($phone,$acc));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        //print_r($result);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function updateUser($passCode ){
    global $dbn;
    try {
        $sql = $dbn->prepare("update users set isAdmin='1' where passCode=?");
        $sql->execute(array($passCode));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        //print_r($result);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function updateStores($passCode ){
    global $dbn;
    try {
        $sql = $dbn->prepare("update users set isAdmin='stores' where passCode=?");
        $sql->execute(array($passCode));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        //print_r($result);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function  updatePartiallyApproved($reason,$auth2,$reqNo){
    global $dbn;
    try {
        $sql = $dbn->prepare("update requisitions set authReason=?,authNumber=? where reqNo=?");
        $sql->execute(array($reason,$authNumber,$reqNo));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        //print_r($result);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function   rejectRequisition($reason, $reqNo) {
    global $dbn;
    try {
        $sql = $dbn->prepare("update requisitions set reqStatus='x',authReason=? where reqNo=?");
        $sql->execute(array($reason, $reqNo));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        //print_r($result);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function holdRequisition($reason, $reqNo) {
    global $dbn;
    try {
        $sql = $dbn->prepare("update requisitions set reqStatus='H',authReason=? where reqNo=?");
        $sql->execute(array($reason, $reqNo));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        //print_r($result);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function getOrders(){
    global $dbn;
      try {
  
          $sql = $dbn->prepare("select * from orders where ordStatus = '' and invStatus = 'O' ORDER BY ordNo ");
          $sql->execute();
          $result = $sql->fetchAll(PDO::FETCH_ASSOC);
      } catch (Exception $ex) {
          $result = $ex->getMessage();
      }
      return $result;   
  }

  function getOrdersAll(){
    global $dbn;
      try {
  
          $sql = $dbn->prepare("select * from orders ORDER BY ordNo");
          $sql->execute();
          $result = $sql->fetchAll(PDO::FETCH_ASSOC);
      } catch (Exception $ex) {
          $result = $ex->getMessage();
      }
      return $result;   
  }
  function syncOrders(){
    global $dbn;
      try {
  
          $sql = $dbn->prepare("select * from orders where ordStatus != '' or invStatus = 'x'");
          $sql->execute();
          $result = $sql->fetchAll(PDO::FETCH_ASSOC);
      } catch (Exception $ex) {
          $result = $ex->getMessage();
      }
      return $result;   
  }
  function Company(){
    global $db;
      try {
  
          $sql = $db->prepare("select * from company ");
          $sql->execute();
          $result = $sql->fetchAll(PDO::FETCH_ASSOC);
      } catch (Exception $ex) {
          $result = $ex->getMessage();
      }
      return $result;   
  }
  
  function countOrders($passCode){
    global $dbn;
      try {
  
          $sql = $dbn->prepare("select COUNT(*) from orders where authCode = ? ORDER BY ordNo ");
          $sql->execute(array($passCode));
          $result['result'] = $sql->fetchAll(PDO::FETCH_ASSOC);
      } catch (Exception $ex) {
          $result = $ex->getMessage();
      }
      return $result;   
  }
  function getReq(){
    global $dbn;
      try {
  
          $sql = $dbn->prepare("select * from requisitions ORDER BY reqNo ASC ");
          $sql->execute();
          $result = $sql->fetchAll(PDO::FETCH_ASSOC);
      } catch (Exception $ex) {
          $result = $ex->getMessage();
      }
      return $result;   
  }
  function warehouseStatus1(){
    global $dbn;
      try {
  
          $sql = $dbn->prepare("select * from wareHouseDetails where stkCode <=200000 and stkCode >=100000 ");
          $sql->execute();
          $result = $sql->fetchAll(PDO::FETCH_ASSOC);
      } catch (Exception $ex) {
          $result = $ex->getMessage();
      }
      return $result;   
  }
  function warehouseStatus2(){
    global $dbn;
      try {
  
          $sql = $dbn->prepare("select * from wareHouseDetails where stkCode <=300000 and stkCode >=200000 ");
          $sql->execute();
          $result = $sql->fetchAll(PDO::FETCH_ASSOC);
      } catch (Exception $ex) {
          $result = $ex->getMessage();
      }
      return $result;   
  }
  function warehouseStatus3(){
    global $dbn;
      try {
  
          $sql = $dbn->prepare("select * from wareHouseDetails where stkCode <=400000 and stkCode >=300000 ");
          $sql->execute();
          $result = $sql->fetchAll(PDO::FETCH_ASSOC);
      } catch (Exception $ex) {
          $result = $ex->getMessage();
      }
      return $result;   
  }
  function warehouseStatus4(){
    global $dbn;
      try {
  
          $sql = $dbn->prepare("select * from wareHouseDetails where stkCode <=500000 and stkCode >=400000 ");
          $sql->execute();
          $result = $sql->fetchAll(PDO::FETCH_ASSOC);
      } catch (Exception $ex) {
          $result = $ex->getMessage();
      }
      return $result;   
  }
  function warehouseStatus5(){
    global $dbn;
      try {
  
          $sql = $dbn->prepare("select * from wareHouseDetails where stkCode <=600000 and stkCode >=500000 ");
          $sql->execute();
          $result = $sql->fetchAll(PDO::FETCH_ASSOC);
      } catch (Exception $ex) {
          $result = $ex->getMessage();
      }
      return $result;   
  }
  function warehouseStatus6(){
    global $dbn;
      try {
  
          $sql = $dbn->prepare("select * from wareHouseDetails where stkCode <=700000 and stkCode >=600000 ");
          $sql->execute();
          $result = $sql->fetchAll(PDO::FETCH_ASSOC);
      } catch (Exception $ex) {
          $result = $ex->getMessage();
      }
      return $result;   
  }
  function takeDescription($stkCode){
    global $dbn;
      try {
  
          $sql = $dbn->prepare("select * from wareHouseMaster ");
          $sql->execute(array($stkCode));
          $result = $sql->fetchAll(PDO::FETCH_ASSOC);
      } catch (Exception $ex) {
          $result = $ex->getMessage();
      }
      return $result;   
  }
  function syncApprovedRequisitionsToPromun(){
    global $dbn;
      try {
  
          $sql = $dbn->prepare("select * from requisitions where reqStatus = 'R' or reqStatus = 'X' ");
          $sql->execute();
          $result = $sql->fetchAll(PDO::FETCH_ASSOC);
      } catch (Exception $ex) {
          $result = $ex->getMessage();
      }
      return $result;   
  }
  function getStockCodeDetails(){
    global $dbn;
      try {
  
          $sql = $dbn->prepare("select * from wareHouseMaster ORDER BY code ASC ");
          $sql->execute();
          $result = $sql->fetchAll(PDO::FETCH_ASSOC);
      } catch (Exception $ex) {
          $result = $ex->getMessage();
      }
      return $result;   
  }
  function getOrdersApproved($passCode){
    global $dbn;
      try {
  
          $sql = $dbn->prepare("select * from orders where invStatus = 'O' and authCode =? ORDER BY ordNo ASC");
          $sql->execute(array($passCode));
          $result = $sql->fetchAll(PDO::FETCH_ASSOC);
      } catch (Exception $ex) {
          $result = $ex->getMessage();
      }
      return $result;   
  }
  function getCancelledOrders($passCode){
    global $dbn;
      try {
  
          $sql = $dbn->prepare("select * from orders where invStatus = 'X' and ordStatus= ? ORDER BY ordNo ASC");
          $sql->execute(array($passCode));
          $result = $sql->fetchAll(PDO::FETCH_ASSOC);
      } catch (Exception $ex) {
          $result = $ex->getMessage();
      }
      return $result;   
  }
  function getInvoicedOrders($passCode){
    global $dbn;
      try {
  
          $sql = $dbn->prepare("select * from orders where invStatus = 'i' and ordStatus = ? ");
          $sql->execute(array($passCode));
          $result = $sql->fetchAll(PDO::FETCH_ASSOC);
      } catch (Exception $ex) {
          $result = $ex->getMessage();
      }
      return $result;   
  }
  function getCompletedOrders($passCode){
    global $dbn;
      try {
  
          $sql = $dbn->prepare("select * from orders where invStatus = 'C' and ordStatus = ? ");
          $sql->execute(array($passCode));
          $result = $sql->fetchAll(PDO::FETCH_ASSOC);
      } catch (Exception $ex) {
          $result = $ex->getMessage();
      }
      return $result;   
  }
  //special query to split values in one column
  function getStoresQuantity(){
    global $dbn;
      try {
  
          $sql = $dbn->prepare("select * from wareHouseDetails where invStatus = 'i' and authCode = ? ");
          $sql->execute();
          $result = $sql->fetchAll(PDO::FETCH_ASSOC);
      } catch (Exception $ex) {
          $result = $ex->getMessage();
      }
      return $result;   
  }
  function getNewOrders($reqNo) {
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from orders where ordNo=? ORDER BY ordNo ASC");
        $sql->execute(array($reqNo));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        //print_r($result);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function getApprovedOrders($reqNo) {
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from orders where ordNo=?");
        $sql->execute(array($reqNo));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        //print_r($result);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function getTotalRatePayers() {
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from DebtorsBalances");
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        //print_r($result);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function getTotalNoEmail() {
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from DebtorsBalances where email = 'null'");
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        print_r($result);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function getOrdersReport($date,$toDate) {
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from orders where ordDate >=? and ordDate <=?");
        $sql->execute(array($date,$toDate));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        if ($sql->rowCount() > 0) {
            $status['status'] = 'passed';
        } else {
            $status['status'] = 'Failed';
        }
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function takeAccDetails($fromAcc,$endAcc){
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from DebtorsBalances where cell !='' and acc >=? and acc <=?");
        $sql->execute(array($fromAcc,$endAcc));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        if ($sql->rowCount() > 0) {
            $status['status'] = 'passed';
        } else {
            $status['status'] = 'Failed';
        }
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function takeAccDetailsBal($start,$end,$minBal, $maxBal){
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from DebtorsBalances where phone!='' and acc >=? and acc <=? and balance >=? and balance <=?");
        $sql->execute(array($start,$end,$minBal, $maxBal));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        if ($sql->rowCount() > 0) {
            $status['status'] = 'passed';
        } else {
            $status['status'] = 'Failed';
        }
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function  vat($vat){
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from company where vat=?");
        $sql->execute(array($vat));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        if ($sql->rowCount() > 0) {
            $_SESSION['vat']= @$result[0]['vat'];
            $status['status'] = 'passed';
            
        } else {
            $status['status'] = 'Failed';
        }
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $status;
}
function takeAccDetailsEmail($start,$end){
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from DebtorsBalances where convert(varchar, email) != '' and acc >=? and acc <=?");
        $sql->execute(array($start,$end));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        if ($sql->rowCount() > 0) {
            $status['status'] = 'passed';
        } else {
            $status['status'] = 'Failed';
        }
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function rejEmail($start,$end){
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from DebtorsBalances where convert(varchar, email) = '' and acc >=? and acc <=?");
        $sql->execute(array($start,$end));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        if ($sql->rowCount() > 0) {
            $status['status'] = 'passed';
        } else {
            $status['status'] = 'Failed';
        }
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function rejEmail2(){
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from emails where convert(varchar, toEmail) = '' or convert(varchar, toEmail) = ';' ");
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        if ($sql->rowCount() > 0) {
            $status['status'] = 'passed';
        } else {
            $status['status'] = 'Failed';
        }
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function takeAccMobile($fromAcc,$endAcc){
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from DebtorsBalances where cell='' and acc >=? and acc <=?");
        $sql->execute(array($fromAcc,$endAcc));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        if ($sql->rowCount() > 0) {
            $status['status'] = 'passed';
        } else {
            $status['status'] = 'Failed';
        }
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function takeAccMobileBal($fromAcc,$endAcc,$minBal,$maxBal){
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from DebtorsBalances where phone='' and acc >=? and acc <=? and balance >=? and balance <=?");
        $sql->execute(array($fromAcc,$endAcc,$minBal,$maxBal));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        if ($sql->rowCount() > 0) {
            $status['status'] = 'passed';
        } else {
            $status['status'] = 'Failed';
        }
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function takeAccEmail($start,$end){
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from DebtorsBalances where convert(varchar, email) = '' and acc >=? and acc <=?");
        $sql->execute(array($start,$end));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        if ($sql->rowCount() > 0) {
            $status['status'] = 'passed';
        } else {
            $status['status'] = 'Failed';
        }
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function getRequisitionsReport($date,$toDate) {
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from requisitions where reqDate >=? and reqDate <=? ORDER BY reqNo ASC");
        $sql->execute(array($date,$toDate));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        if ($sql->rowCount() > 0) {
            $status['status'] = 'passed';
        } else {
            $status['status'] = 'Failed';
        }
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function getOrdersReportOrd($ordNo) {
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from orders where ordNo =? ");
        $sql->execute(array($ordNo));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        if ($sql->rowCount() > 0) {
            $status['status'] = 'passed';
        } else {
            $status['status'] = 'Failed';
        }
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function  getVote($year,$alloc) {
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from budgets where balYear = ? and acc=?");
        $sql->execute(array($year,$alloc));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        if ($sql->rowCount() > 0) {
            $status['status'] = 'passed';
        } else {
            $status['status'] = 'Failed';
        }
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function getReqReport($reqNo) {
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from requisitions where reqNo =? ");
        $sql->execute(array($reqNo));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        if ($sql->rowCount() > 0) {
            $status['status'] = 'passed';
        } else {
            $status['status'] = 'Failed';
        }
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function validateApprover2($reqNo) {
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from requisitions where reqNo =? ");
        $sql->execute(array($reqNo));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        if ($sql->rowCount() > 0) {
            $status['status'] = 'passed';
        } else {
            $status['status'] = 'Failed';
        }
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function checkReq($reqNo){
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from requisitions where reqNo =? ");
        $sql->execute(array($reqNo));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function checkReqLines($reqNo){
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from requisitionLines where reqNo =? ");
        $sql->execute(array($reqNo));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function  checkRow($emailCode,$emailDate) {
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from emails where seq =? and date = ? ");
        $sql->execute(array($emailCode,$emailDate));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        if ($sql->rowCount() > 0) {
            $status['status'] = 'passed';
        } else {
            $status['status'] = 'Failed';
        }
    } catch (Exception $ex) {
        $status = $ex->getMessage();
    }

    return $status;
}
function CreateEmails($emailCode, $emailTo, $emailFrom, $emailSubject, $emailBody,$emailStatus, $emailDate) {
    global $dbn;
    try {

        $sql = $dbn->prepare('insert into emails("seq","toEmail","from",subject,body,status,date) values (?,?,?,?,?,?,?)');
        $sql->execute(array($emailCode, $emailTo, $emailFrom, $emailSubject, $emailBody,$emailStatus, $emailDate));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result['status'] = 'ok';
        } else {
            $result['status'] = 'fail';
        }
    } catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }
    return $result;
}
function getEmailDetails($w_date){
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from emails where date =? ");
        $sql->execute(array($w_date));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function validateApprover($ordNo) {
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from orders where ordNo =? ");
        $sql->execute(array($ordNo));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        if ($sql->rowCount() > 0) {
            $status['status'] = 'passed';
        } else {
            $status['status'] = 'Failed';
        }
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function  validateOrderApprover($reqNo){
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from orders where ordNo =? ");
        $sql->execute(array($reqNo));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        if ($sql->rowCount() > 0) {
            $status['status'] = 'passed';
        } else {
            $status['status'] = 'Failed';
        }
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function getApprovedReport($type) {
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from orders where ordStatus=?");
        $sql->execute(array($type));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        if ($sql->rowCount() > 0) {
            $status['status'] = 'passed';
        } else {
            $status['status'] = 'Failed';
        }
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function insertInOrderView($date,$toDate,$type) {
    global $dbn;
    try {
        $sql = $dbn->prepare('insert into orderView(date,toDate,type) values (?,?,?)');
        $sql->execute(array($date,$toDate,$type));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
  
function saveAudit($passCode,$action,$date,$time,$name){
    global $dbn;
    try {
        $sql = $dbn->prepare('insert into audit(id,action,date,time,account) values (?,?,?,?,?)');
        $sql->execute(array($passCode,$action,$date,$time,$name));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function logDetails($passCode,$action,$date){
    global $dbn;
    try {
        $sql = $dbn->prepare('insert into audit(id,action,date) values (?,?,?)');
        $sql->execute(array($passCode,$action,$date));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function saveAccSms($acc,$phone,$bal,$statsms,$msg,$txtid,$name,$passCode){
    global $dbn;
    try {
        $sql = $dbn->prepare('insert into sms(acc,phone,balance,status,message,txtid,name,senderId) values (?,?,?,?,?,?,?,?)');
        $sql->execute(array($acc,$phone,$bal,$statsms,$msg,$txtid,$name,$passCode));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        print_r($result);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
    
}
function saveRejects($acc,$name,$bal){
    global $dbn;
    try {
        $sql = $dbn->prepare('insert into rejectedsms(acc,name,balance) values (?,?,?)');
        $sql->execute(array($acc,$name,$bal));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function  saveInvalid($acc,$name,$bal,$invalid){
    global $dbn;
    try {
        $sql = $dbn->prepare('insert into rejectedsms(acc,name,balance,reason) values (?,?,?,?)');
        $sql->execute(array($acc,$name,$bal,$invalid));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function saveRejectEmail($acc,$name,$bal){
    global $dbn;
    try {
        $sql = $dbn->prepare('insert into rejectedemail(acc,name,balance) values (?,?,?)');
        $sql->execute(array($acc,$name,$bal));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function saveEmails($seq, $to, $from, $subject, $body,$status, $date) {
    global $dbn;
    try {

        $sql = $dbn->prepare('insert into emails(seq,toEmail,from,subject,body,status,date) values (?,?,?,?,?,?,?)');
        $sql->execute(array($seq, $to, $from, $subject, $body,$status, $date));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result['status'] = 'ok';
        } else {
            $result['status'] = 'fail';
        }
    } catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }
    return $result;
}
function saveQueEmails($seq, $body,$status, $date, $subject, $to, $fromEmail ) {
    global $dbn;
    try {

        $sql = $dbn->prepare('insert into queEmails(seq,body,status,date,subject,toEmail,fromEmail) values (?,?,?,?,?,?,?)');
        $sql->execute(array($seq, $body,$status, $date, $subject, $to, $fromEmail ));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
   
}
function saveDetails($whse,$stkCode,$qty,$received,$desc) {
    global $dbn;
    try {
        $sql = $dbn->prepare('insert into stats(whse,stkCode,qty,received,descrip) values (?,?,?,?,?)');
        $sql->execute(array($whse,$stkCode,$qty,$received,$desc));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function DeleteOrderView() {
    global $dbn;
    try {

        $stmt = $dbn->prepare("delete from orderView");
        $stmt->execute();
        $result = "done";
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}
function DeleteSMS() {
    global $dbn;
    try {

        $stmt = $dbn->prepare("delete from sms");
        $stmt->execute();
        $result = "done";
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}
function queEmail(){
    global $dbn;
    try {

        $stmt = $dbn->prepare("delete from queEmail");
        $stmt->execute();
        $result = "done";
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}
function DeleteRejectedEmail() {
    global $dbn;
    try {

        $stmt = $dbn->prepare("delete from rejectedemail");
        $stmt->execute();
        $result = "done";
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}
function DeleteRejectedSMS() {
    global $dbn;
    try {

        $stmt = $dbn->prepare("delete from rejectedsms");
        $stmt->execute();
        $result = "done";
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}
function deleteSaved() {
    global $dbn;
    try {

        $stmt = $dbn->prepare("delete from stats");
        $stmt->execute();
        $result = "done";
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}
function deleteStkView() {
    global $dbn;
    try {

        $stmt = $dbn->prepare("delete from WhseView");
        $stmt->execute();
        $result = "done";
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}
function DeleteView() {
    global $dbn;
    try {

        $stmt = $dbn->prepare("delete from ordViewByOrdNum");
        $stmt->execute();
        $result = "done";
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}
function DeleteReqView() {
    global $dbn;
    try {

        $stmt = $dbn->prepare("delete from ReqView");
        $stmt->execute();
        $result = "done";
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}
function ReportOrderView() {
    global $dbn;
    try {

        $sql = $dbn->prepare("select * from orderView");
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}
function takeSavedDetails() {
    global $dbn;
    try {

        $sql = $dbn->prepare("select * from stats");
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}

function takeRejected(){
    global $dbn;
    try {

        $sql = $dbn->prepare("select * from rejectedsms");
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}
function rejectedEm(){
    global $dbn;
    try {

        $sql = $dbn->prepare("select * from rejectedemail");
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}
function sentSms(){
    global $dbn;
    try {

        $sql = $dbn->prepare("select * from sms");
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}
function  audit(){
    global $dbn;
    try {

        $sql = $dbn->prepare("select * from audit");
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}
function sentEmail(){
    global $dbn;
    try {

        $sql = $dbn->prepare("select * from queEmails");
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}
function ReportReqView() {
    global $dbn;
    try {

        $sql = $dbn->prepare("select * from ReqView");
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}
function ReportOrderViewOrd() {
    global $dbn;
    try {

        $sql = $dbn->prepare("select * from ordViewByOrdNum");
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}

function ReportOrderViewDetails($date,$toDate) {    
    global $dbn;
    try {

        $sql = $dbn->prepare("select * from orders where  ordDate >= ? and ordDate <= ?");
        $sql->execute(array($date,$toDate));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}
function ReportRequisitionViewDetails($date,$toDate) {    
    global $dbn;
    try {

        $sql = $dbn->prepare("select * from requisitions where  reqDate >= ? and reqDate <= ?");
        $sql->execute(array($date,$toDate));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}
function ReportRequisitionView($reqNo) {    
    global $dbn;
    try {

        $sql = $dbn->prepare("select * from requisitions where  reqNo = ? ");
        $sql->execute(array($reqNo));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}
function ReportOrderViewDetailsOrd($ordNo) {    
    global $dbn;
    try {

        $sql = $dbn->prepare("select * from orders where  ordNo = ? ");
        $sql->execute(array($ordNo));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}
function allOrders() {    
    global $dbn;
    try {

        $sql = $dbn->prepare("select * from orders ");
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}
function allRequisitions(){    
    global $dbn;
    try {

        $sql = $dbn->prepare("select * from requisitions ");
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}
function ordViewByOrdNum($ordNo) {    
    global $dbn;
    try {

        $sql = $dbn->prepare('insert into ordViewByOrdNum(ordNo) values (?)');
        $sql->execute(array($ordNo));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}
function blockLA($reason,$codeLA){    
    global $dbn;
    try {

        $sql = $dbn->prepare("update company set status ='blocked', reason=? where code =? ");
        $sql->execute(array($reason,$codeLA));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}
function unblockLA($reason,$codeLA){    
    global $dbn;
    try {

        $sql = $dbn->prepare("update company set status ='active', reason=? where code =? ");
        $sql->execute(array($reason,$codeLA));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}
function reqView($reqNo) {    
    global $dbn;
    try {

        $sql = $dbn->prepare('insert into ReqView(reqNo) values (?)');
        $sql->execute(array($reqNo));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}
function saveStkDetails($whse,$stk){    
    global $dbn;
    try {

        $sql = $dbn->prepare('insert into whseView(whse,stk) values (?,?)');
        $sql->execute(array($whse,$stk));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}
function getInvoicedOrdersH($reqNo){
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from orders where ordNo=?");
        $sql->execute(array($reqNo));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        //print_r($result);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function checkLAs($code){
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from company where code=?");
        $sql->execute(array($code));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        if ($sql->rowCount() > 0) {
            $status['status'] = 'passed';
            
        } else {
            $status['status'] = 'Failed';
           
        }
       
    } catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }

    return $status;
}

function getCancelledOrdersH($reqNo) {
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from orders where ordNo=?");
        $sql->execute(array($reqNo));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        //print_r($result);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function getOrderDetails($ordNo) {
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from OrderDescription where ordNo=?");
        $sql->execute(array($ordNo));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        //print_r($result);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function getReqDetails($reqNo) {
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from requisitionLines where reqNo=?");
        $sql->execute(array($reqNo));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        //print_r($result);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function getSupplierDetails($brCode) {
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from suppliers where brCode=?");
        $sql->execute(array($brCode));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        //print_r($result);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function getOrderTypes($ordType) {
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from OrderTypes where ordType = ?");
        $sql->execute(array($ordType));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        //print_r($result);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
//function to sync requistions automatically at background when logging in
function LaunchRequisitions($url)
{
   $options = array(
      CURLOPT_TIMEOUT   => 3,
      CURLOPT_NOSIGNAL  => true,
      CURLOPT_USERAGENT => "Launcher"
   );
   $ch = curl_init($url);
   curl_setopt_array($ch,$options);
   curl_exec($ch);
}
function LaunchEmailsPro($urlEmailsPro){
    $options = array(
       CURLOPT_TIMEOUT   => 3,
       CURLOPT_NOSIGNAL  => true,
       CURLOPT_USERAGENT => "Launcher"
    );
    $ch = curl_init($urlEmailsPro);
    curl_setopt_array($ch,$options);
    curl_exec($ch);
 }
//function to sync orders automatically at background when logging in
function LaunchOrders($urlO)
{
   $options = array(
      CURLOPT_TIMEOUT   => 3,
      CURLOPT_NOSIGNAL  => true,
      CURLOPT_USERAGENT => "Launcher"
   );
   $ch = curl_init($urlO);
   curl_setopt_array($ch,$options);
   curl_exec($ch);
}
//function to sync company deatails automatically at background when logging in
function LaunchCo($urlCo)
{
   $options = array(
      CURLOPT_TIMEOUT   => 3,
      CURLOPT_NOSIGNAL  => true,
      CURLOPT_USERAGENT => "Launcher"
   );
   $ch = curl_init($urlCo);
   curl_setopt_array($ch,$options);
   curl_exec($ch);
}
//function to sync users automatically at background when logging in
function LaunchUsers($urlUsers)
{
   $options = array(
      CURLOPT_TIMEOUT   => 3,
      CURLOPT_NOSIGNAL  => true,
      CURLOPT_USERAGENT => "Launcher"
   );
   $ch = curl_init($urlUsers);
   curl_setopt_array($ch,$options);
   curl_exec($ch);
}
//function to sync emails automatically at background when logging in
function LaunchEmails($urlO)
{
   $options = array(
      CURLOPT_TIMEOUT   => 3,
      CURLOPT_NOSIGNAL  => true,
      CURLOPT_USERAGENT => "Launcher"
   );
   $ch = curl_init($urlEmails);
   curl_setopt_array($ch,$options);
   curl_exec($ch);
}
//function to sync suppliers automatically at background when logging in

function LaunchSuppliers($urlSuppliers)
{
   $options = array(
      CURLOPT_TIMEOUT   => 3,
      CURLOPT_NOSIGNAL  => true,
      CURLOPT_USERAGENT => "Launcher"
   );
   $ch = curl_init($urlSuppliers);
   curl_setopt_array($ch,$options);
   curl_exec($ch);
}
function LaunchRequisitionLines($urlReqLines)
{
    $options = array(
       CURLOPT_TIMEOUT   => 3,
       CURLOPT_NOSIGNAL  => true,
       CURLOPT_USERAGENT => "Launcher"
    );
    $ch = curl_init($urlReqLines);
    curl_setopt_array($ch,$options);
    curl_exec($ch);
 }
function LaunchOrdLines($urlOrdLines)
{
    $options = array(
       CURLOPT_TIMEOUT   => 3,
       CURLOPT_NOSIGNAL  => true,
       CURLOPT_USERAGENT => "Launcher"
    );
    $ch = curl_init($urlOrdLines);
    curl_setopt_array($ch,$options);
    curl_exec($ch);
 }
function LaunchOrdTypes($urlOrdTypes)
{
    $options = array(
       CURLOPT_TIMEOUT   => 3,
       CURLOPT_NOSIGNAL  => true,
       CURLOPT_USERAGENT => "Launcher"
    );
    $ch = curl_init($urlOrdTypes);
    curl_setopt_array($ch,$options);
    curl_exec($ch);
 }
 function LaunchGmail($urlGmail)
{
    $options = array(
       CURLOPT_TIMEOUT   => 3,
       CURLOPT_NOSIGNAL  => true,
       CURLOPT_USERAGENT => "Launcher"
    );
    $ch = curl_init($urlGmail);
    curl_setopt_array($ch,$options);
    curl_exec($ch);
 }
 function LaunchWareHouseMaster($urlWareHouse)
 {
    $options = array(
       CURLOPT_TIMEOUT   => 3,
       CURLOPT_NOSIGNAL  => true,
       CURLOPT_USERAGENT => "Launcher"
    );
    $ch = curl_init($urlWareHouse);
    curl_setopt_array($ch,$options);
    curl_exec($ch);
 }
 function LaunchWareHouseDetails($urlWareHouseDetails) {
    $options = array(
       CURLOPT_TIMEOUT   => 3,
       CURLOPT_NOSIGNAL  => true,
       CURLOPT_USERAGENT => "Launcher"
    );
    $ch = curl_init($urlWareHouseDetails);
    curl_setopt_array($ch,$options);
    curl_exec($ch);
 }
 /*function checkInternet() 
 {
 $host_name = 'www.google.com';
 $port_no = '80';
 
 $st = (bool)@fsockopen($host_name, $port_no, $err_no, $err_str, 10);
 if ($st) {
     $nt['status']='connected';
 } else {
     $nt['status']='offline';
 }
     
 return $nt;
 }*/
 //Functions from SMS Platform
 function takeReadySms(){
    global $dbn;
    try {

        $sql = $dbn->prepare("select * from sms where status = 'unsent'");
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}
function companySms(){
    global $dbn;
    try {

        $sql = $dbn->prepare("select * from company");
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}
function updateSms($id) {
    global $dbn;
    try {
        $sql = $dbn->prepare("update sms set status='submited' where txtid=?");
        $sql->execute(array($id));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function checkInternet() 
{
$host_name = 'www.google.com';
$port_no = '80';

$st = (bool)@fsockopen($host_name, $port_no, $err_no, $err_str, 10);
if ($st) {
    $nt['status']='connected';
} else {
    $nt['status']='offline';
}
    
return $nt;
}
function reduceToSendCredits($msgcredits) {

    global $dbn;
    try {
        $sql = "update company set smsCredit=SMSCreds-?";
        $custquery = $dbn->prepare($sql);
        $custquery->execute(array($msgcredits));
        $custcount = $custquery->rowCount();
        if ($custcount > 0) {
            $rslt["status"] = true;
            $rslt["msg"] = "done";
        } else {
            $rslt["status"] = false;
            $rslt["msg"] = "failed";
        }
        return $rslt;
    } catch (Exception $ex) {
       // echo $ex->message();
    }
}

function getBalance() {
    global $dbn;

    $sql2 = "SELECT smsCredit as creds FROM company";
    $credit = $dbn->prepare($sql2);
    $credit->execute();
    $rslts = $credit->fetchAll(PDO::FETCH_ASSOC);
    return $rslts;
}

function getSmsAccount(){
    global $dbn;
    try {

        $sql = $dbn->prepare("select * from company");
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}
function updateToSendMSG($txtid,$messageID) {
    global $dbn;

    try {
        $stm = $dbn->prepare('update sms set status="sent" where id=?');
        $stm->execute(array($txtid,$messageID));
        if ($stm->rowCount() > 0) {
            $response['status'] = 'OK';
        } else {
            $response['status'] = 'failed';
        }
    } catch (PDOExcepion $ex) {
        echo $ex->getMessage();
    }
}


function checkBalance() {
    
    $accountDetails = getSmsAccount();
    $account = $accountDetails[0]['smsAccount'];
    $password = $accountDetails[0]['password'];
    $api = $accountDetails[0]['api'];
    print_r($account);
    $url = "https://rest.bluedotsms.com/api/CheckBalance?api_id=$api&api_password=$password";  
    
    
    $response = file_get_contents($url);

    $json = json_decode($response, TRUE);
    print_r($json);
    
  
    
}
function EmailRatePayer() {
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from queEmails where status = 0");
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
  
}

function updateEmails($code) {
    global $dbn;
    try {

        $sql = $dbn->prepare('update queEmails set status=1 where seq= ?');
        $sql->execute(array($code));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result['status'] = 'ok';
        } else {
            $result['status'] = 'fail';
        }
    } catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }
    return $result;
}