<?php
include('header.php');
require "DBAPI.php";
$passCode = $_SESSION['pCode'];
if(empty($passCode)){
    header ("location:login.php");
}else{
    $UserData = getUserDetails($passCode);
    $lockUser = @$UserData[0]['lockUser'];
    $maxAuth = @$UserData[0]['maxAuth'];
    $codata = getCompanyDetails();
    

}

?>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
<tr>
    <td colspan="2">
       <table border="1" class="table-bordered" width="100%">
          <tr>
             <td width="20%" style="background-color: white" valign="top">
				 <?php
                    include('approvaldashboard.php');
                 ?>
             </td>
             <td width="80%" valign="top"><br>
                <!--------------------------1st table for new requisitions--------------------------------->
                     <table class="table-responsive" width="95%" align="center">
                	<?php
					       $requisitions = getRequisitions($passCode);
                              
                    if(empty($requisitions)){
                        echo "<tr>";
                        echo "<td colspan='7'><label style='color: red'><i>No New Requisitions<i></label><td>";
                        echo "</tr>";

                       }else{
					
							echo "<tr style='color: #5FA5B3'>";
								echo "<td colspan='7' style='color: green'><strong>"."Available New Requisitions"."</strong><td>";
							echo "</tr>";
							echo "<tr style='color: white' bgcolor='#002F74'>";
								echo "<td width='15%'>"."REQUISITION No"."<td>";
								echo "<td width='40%'>"."DESCRIPTION"."<td>";
                                echo "<td width='16%'>"."DATE REQUESTED"."<td>";
                                echo "<td width='16%'>"."REQUISITION TYPE"."<td>";
								echo "<td width='13%' align='left'>"."VIEW"."<td>";
                            echo "</tr>";
                            foreach($requisitions as $pr){
                                $type = @$pr["reqType"];
                                if($type == "O"){
                                    $type = "Out Of Stock";
                                }
                                if($type == "N"){
                                    $type = "Non Stock";
                                }
                                if($type == "l"){
                                    $type = "Into Of Stock";
                                }
                                echo "<tr>";
                                    echo "<td width='15%'>".@$pr["reqNo"]."<td>";
                                    echo "<td width='40%'>".@$pr["reqDesc"]."<td>";
                                    echo "<td width='16%'>".@$pr["reqDate"]."<td>";
                                    echo "<td width='16%'>".$type."<td>";
                                    //echo "<td>".@$pr["reqType"]."<td>";
                                    echo "<td width='13%'><a href='req.php?reqNo=".@$pr["reqNo"]."''>view more details</a><td>";
                                echo "</tr>";
                                echo "<tr>";
                                    echo "<td colspan='7' height='2'>".""."<td>";
                                echo "</tr>";
                            }
							
							
                           }
						
					
				
					?>
                </table><br>
                <!--------------------------2nd table for approved Requisitions--------------------------------->
                    <table class="table-responsive" width="95%" align="center">
                	<?php
                       $requisitions = getRequisitionsApproved($passCode);
                       
                      
                       if(empty($requisitions)){
                        echo "<tr>";
                        echo "<td colspan='7'><label style='color: red'><i>No Approved Requisitions<i></label><td>";
                        echo "</tr>";

                       }
                      else{
                        echo "<tr style='color: #5FA5B3'>";
                            echo "<td colspan='7' style='color: blue'><strong>"."Approved  Requisitions"."</strong><td>";
                        echo "</tr>";
                        echo "<tr style='color: white' bgcolor='#002F74'>";
                            echo "<td width='15%'>"."REQUISITION No"."<td>";
                            echo "<td width='40%'>"."DESCRIPTION"."<td>";
                            echo "<td width='16%'>"."DATE REQUESTED"."<td>";
                            echo "<td width='16%'>"."REQUISITION TYPE"."<td>";
                            echo "<td width='13%' align='left'>"."VIEW"."<td>";
                        echo "</tr>";
                        foreach($requisitions as $pr){
                            $type = @$pr["reqType"];
                            if($type == "O"){
                                $type = "Out Of Stock";
                            }
                            if($type == "N"){
                                $type = "Non Stock";
                            }
                            if($type == "l"){
                                $type = "Into Of Stock";
                            }
                            echo "<tr>";
                                echo "<td width='15%'>".@$pr["reqNo"]."<td>";
                                echo "<td width='40%'>".@$pr["reqDesc"]."<td>";
                                echo "<td width='16%'>".@$pr["reqDate"]."<td>";
                                echo "<td width='16%'>".$type."<td>";
                                echo "<td width='13%'><a href='approvedRequisitions.php?reqNo=".@$pr["reqNo"]."''>view more details</a><td>";
                            echo "</tr>";
                            echo "<tr>";
                                echo "<td colspan='7' height='2'>".""."<td>";
                            echo "</tr>";
                        }
                       }
                    


					?>
                </table><br>
                <table class="table-responsive" width="95%" align="center">
                	<?php
                       $requisitions = getRequisitionsOnHold($passCode);
                       
                      
                       if(empty($requisitions)){
                        echo "<tr>";
                        echo "<td colspan='7'><label style='color: red'><i>No Requisitions on Hold<i></label><td>";
                        echo "</tr>";

                       }
                      else{
                        echo "<tr style='color: #5FA5B3'>";
                            echo "<td colspan='7' style='color: red'><strong>"."Requisitions on Hold"."</strong><td>";
                        echo "</tr>";
                        echo "<tr style='color: white' bgcolor='#002F74'>";
                            echo "<td width='15%'>"."REQUISITION No"."<td>";
                            echo "<td width='40%'>"."DESCRIPTION"."<td>";
                            echo "<td width='16%'>"."DATE REQUESTED"."<td>";
                            echo "<td width='16%'>"."REQUISITION TYPE"."<td>";
                            echo "<td width='13%' align='left'>"."VIEW"."<td>";
                        echo "</tr>";
                        foreach($requisitions as $pr){
                            $type = @$pr["reqType"];
                            if($type == "O"){
                                $type = "Out Of Stock";
                            }
                            if($type == "N"){
                                $type = "Non Stock";
                            }
                            if($type == "l"){
                                $type = "Into Of Stock";
                            }
                            echo "<tr>";
                                echo "<td width='15%'>".@$pr["reqNo"]."<td>";
                                echo "<td width='40%'>".@$pr["reqDesc"]."<td>";
                                echo "<td width='16%'>".@$pr["reqDate"]."<td>";
                                echo "<td width='16%'>".$type."<td>";
                                echo "<td width='13%'><a href='requisitionsOnHold.php?reqNo=".@$pr["reqNo"]."''>view more details</a><td>";
                            echo "</tr>";
                            echo "<tr>";
                                echo "<td colspan='7' height='2'>".""."<td>";
                            echo "</tr>";
                        }
                       }
                    


					?>
                </table><br>
                 <!--------------------------3rd table for rejected requisitions--------------------------------->
                 <table class="table-responsive" width="95%" align="center">
                	<?php
                       $requisitions = getRequisitionsRejected($passCode);
                       
                      
                       if(empty($requisitions)){
                        echo "<tr>";
                        echo "<td colspan='7'><label style='color: red'><i>No Rejected Requisitions<i></label><td>";
                        echo "</tr>";

                       }
                      else{
                        echo "<tr style='color: #5FA5B3'>";
                            echo "<td colspan='7' style='color: red'><strong>"."Rejected  Requisitions"."</strong><td>";
                        echo "</tr>";
                        echo "<tr style='color: white' bgcolor='#002F74'>";
                            echo "<td width='15%'>"."REQUISITION No"."<td>";
                            echo "<td width='40%'>"."DESCRIPTION"."<td>";
                            echo "<td width='16%'>"."DATE REQUESTED"."<td>";
                            echo "<td width='16%'>"."REQUISITION TYPE"."<td>";
                            echo "<td width='13%' align='left'>"."VIEW"."<td>";
                        echo "</tr>";
                        foreach($requisitions as $pr){
                            $type = @$pr["reqType"];
                            if($type == "O"){
                                $type = "Out Of Stock";
                            }
                            if($type == "N"){
                                $type = "Non Stock";
                            }
                            if($type == "l"){
                                $type = "Into Of Stock";
                            }
                            echo "<tr>";
                                echo "<td width='15%'>".@$pr["reqNo"]."<td>";
                                echo "<td width='40%'>".@$pr["reqDesc"]."<td>";
                                echo "<td width='16%'>".@$pr["reqDate"]."<td>";
                                echo "<td width='16%'>".$type."<td>";
                                echo "<td width='13%'><a href='rejectedRequisitions.php?reqNo=".@$pr["reqNo"]."''>view more details</a><td>";
                            echo "</tr>";
                            echo "<tr>";
                                echo "<td colspan='7' height='2'>".""."<td>";
                            echo "</tr>";
                        }
                       }
                    


					?>
                </table><br>
                  <!--------------------------3rd table for rejected requisitions--------------------------------->
                  <table class="table-responsive" width="95%" align="center">
                	<?php
                       $requisitionsc = getRequisitionsComplete($passCode);
                       
                      
                       if(empty($requisitionsc)){
                        echo "<tr>";
                        echo "<td colspan='7'><label style='color: red'><i>No Complete Requisitions<i></label><td>";
                        echo "</tr>";

                       }
                      else{
                        echo "<tr style='color: #5FA5B3'>";
                            echo "<td colspan='7' style='color: red'><strong>"."Complete  Requisitions"."</strong><td>";
                        echo "</tr>";
                        echo "<tr style='color: white' bgcolor='#002F74'>";
                            echo "<td width='15%'>"."REQUISITION No"."<td>";
                            echo "<td width='40%'>"."DESCRIPTION"."<td>";
                            echo "<td width='16%'>"."DATE REQUESTED"."<td>";
                            echo "<td width='16%'>"."REQUISITION TYPE"."<td>";
                            echo "<td width='13%' align='left'>"."VIEW"."<td>";
                        echo "</tr>";
                        foreach($requisitionsc as $pr){
                            $type = @$pr["reqType"];
                            if($type == "O"){
                                $type = "Out Of Stock";
                            }
                            if($type == "N"){
                                $type = "Non Stock";
                            }
                            if($type == "l"){
                                $type = "Into Of Stock";
                            }
                            echo "<tr>";
                                echo "<td width='15%'>".@$pr["reqNo"]."<td>";
                                echo "<td width='40%'>".@$pr["reqDesc"]."<td>";
                                echo "<td width='16%'>".@$pr["reqDate"]."<td>";
                                echo "<td width='16%'>".$type."<td>";
                                echo "<td width='13%'><a href='completeRequisitions.php?reqNo=".@$pr["reqNo"]."''>view more details</a><td>";
                            echo "</tr>";
                            echo "<tr>";
                                echo "<td colspan='7' height='2'>".""."<td>";
                            echo "</tr>";
                        }
                       }
                    


					?>
                </table><br>
                     
               
             </td>
          </tr>
      </table>
    </td>
</tr>
<?php
include('footer.php');
?>
