<?php
include('header.php');
require "DBAPI.php";
?>
<tr>
    <td colspan="2">
       <table border="1" class="table-bordered" width="100%">
          <tr>
             <td width="20%" style="background-color: white" valign="top">
				 <?php
                    include('approvaldashboard.php');
                 ?>
             </td>
             <td width="80%" valign="top"><br>
            <table class="table-stripped" border="0" width="98%" align="center">
            <tr>
            <td width="">
                 <select class="form-control input-sm" name="reqNo">
                 <option>.....Select Requisition No.....</option>
     
                  <?php
                    $req = getReq();
                      foreach($req as $od){
                    $reqNo = @$od["reqNo"];
                     echo "<option>".$reqNo."</option>";
                     }
     
 
     ?>
             </select>
            </td>
           
                <td width="1%"></td>
                <td width="5%"><button type="submit" name="btnsearch" class="btn btn-default btn-sm glyphicon glyphicon-search"></button></td>
            </tr>
             <tr>
             	<td colspan="7" align="center" height="10">
                </td>
             </tr>
             <tr>
        <td colspan="7" align="center"><?php
            if(isset($_POST['btnsearch']))
            {
                $reqNo = $_POST['reqNo'];
            
                

				if(empty($reqNo) or $reqNo ==".....Select Requisition No....."  )
				{
					echo "<p style='color: red'>Select all details</p>";
                }
               
				else
				{
					if($reqNo <> " "  )
					{
                        //$orders = getOrdersReport($ord);
                        
               
							DeleteReqView();
						
                            reqView($reqNo);
						
							
						
					echo "<table class='table-bordered' width='100%' align='center'>";
						   {
							//$type = getApprovedReport($type);
							
						echo "<tr style=' font-weight: bolder; color: white' bgcolor='#002F74'>";
                        echo "<td style='font-size: 12px'>"."REQ No."."</td>";
                        echo "<td style='font-size: 12px'>DESCRIPTION</td>";
                        echo "<td style='font-size: 12px'>REQUISITION TYPE</td>";
                        echo "<td style='font-size: 12px'>REQUISTED BY</td>";
                        echo "<td style='font-size: 12px'>REQUISITION STATUS</td>";
                        echo "<td style='font-size: 12px'>AUTHORISATION NUMBER</td>";
                        echo "<td style='font-size: 12px'>USER TO APPROVE</td>";
							echo "</tr>";
								
                            $ods =  getReqReport($reqNo);
                            foreach($ods as $pr){
                                $reqNo = @$pr["reqNo"];
                                $reqDate = @$pr["reqDate"];
                                $reqDesc = @$pr["reqDesc"];
                                $reqCode = @$pr["passCode"];
                                $captCode = @$pr["captCode"];
                                $authCode = @$pr["authCode"];
                                $ordType = @$pr["ordType"];
                                $reqType = @$pr["reqType"];
                                $reqStatus = @$pr["reqStatus"];
                                $authNumber =  @$pr["authNumber"];
                                $reqAuthNumber = @$pr["reqAuthNumber"];
                                //get requisition lines
                                $lines =  getRequisitionLines($reqNo);
                                //get Order types
                                $description = getOrderTypes($ordType);
                                $order_name = @$description[0]["ordDescrip"];
                                if($reqStatus == "*"){
                                    $reqStatus = "UnAuthorised";
                                }
                                if($reqStatus == "R"){
                                    $reqStatus = "Approved";
                                }
                                if($reqStatus == "C"){
                                    $reqStatus = "Already Ordered";
                                }
                                if($reqStatus == "x"){
                                    $reqStatus = "Rejected";

                                }
                                if($reqType == "N"){
                                    $reqType = "Non Stock";

                                }
                                if($reqType == "O"){
                                    $reqType = "Out Of stock";

                                }
                                if($reqType == "I"){
                                    $reqType = "Into Stock";

                                }
									echo "<tr>";
                                    echo "<td style='font-size: 12px'>".$reqNo."</td>";
                                    echo "<td style='font-size: 12px'>".$reqDesc."</td>";
                                    echo "<td style='font-size: 12px'>".$reqType."</td>";
                                    echo "<td style='font-size: 12px'>".$reqCode."</td>";
                                    echo "<td style='font-size: 12px'>".$reqStatus."</td>";
                                    echo "<td style='font-size: 12px'>".$reqAuthNumber."</td>";
                                    echo "<td style='font-size: 12px'>".$authCode."</td>";
										
									echo "</tr>";
                                }
                             
							
						   }
                     echo "</table>";
                     if(!empty($reqNo)){
                         echo "<table class='table-bordered' width='100%' align='center'>";
                        echo "<tr>";
                        echo "<a href='IndividualRequisitionReport.php' target='_blank' style='color:red; '><strong>View Report</strong></a>";
                        echo "</tr>";
                    }
				
					}
					
				}
                
            }
           
            ?></td>
            </tr>
         </table>
             </td>
          </tr>
      </table>
    </td>
</tr>
<?php
include('footer.php');
?>