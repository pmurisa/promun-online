<?php
include('adminHeader.php');
require "DBAPI.php";
$passCode = $_SESSION['passCode'];
$LaInf = getCompanyDetails();
$fromEmail = @$LaInf[0]['email'];

if(empty($passCode)){
    header ("location:adminLogin.php");
}
?>
<html>
  <head>
  <style>
    .card1{
    float:left;
    
    


  }
  .body{
    box-shadow :1px 1px 20px purple;
  }
  .card1:hover {
  color: orange;
}
  .card2{
    float:left;
    


  }
  .card2:hover {
  color: blue;
  }
  .card3{
    float:left;



  }
  .text{
    width:100%;
    height:15px;
  }
  .card-text-last{
    font-size:10px;
  }
  .card-text{
    font-size:10px;
  }
  .card3:hover {
  color:red;
  }
  {box-sizing: border-box;}

/* Button used to open the contact form - fixed at the bottom of the page */
.open-button {
  background-color: #555;
  color: white;
  padding: 16px 20px;
  border: none;
  cursor: pointer;
  opacity: 0.8;
  position: fixed;
  bottom: 23px;
  right: 28px;
  width: 280px;
}

/* The popup form - hidden by default */
.form-popup {
  display: none;
  position:fixed;
  bottom: 0;
  right: 50px;
  border: 3px solid #f1f1f1;
  border-radius:15px;
  z-index: 9;
}

/* Add styles to the form container */
.form-container {
  max-width: 200px;
  padding: 10px;
  background-color:white;
  
}

/* Full-width input fields */
.form-container input[type=number], .form-container input[type=text] , .form-container input[type=email]{
  width: 100%;
  padding: 15px;
  margin: 5px 0 2px 0;
  border: none;
  background: #f1f1f1;
  font-size:10px;
}

/* When the inputs get focus, do something */
.form-container input[type=number]:focus, .form-container input[type=text]:focus {
  background-color: #ddd;
  outline: none;
}

/* Set a style for the submit/login button */
.form-container .btn {
  background-color: #4CAF50;
  color: white;
  padding: 16px 20px;
  border: none;
  cursor: pointer;
  width: 100%;
  margin-bottom:10px;
  opacity: 0.8;
}

/* Add a red background color to the cancel button */
.form-container .cancel {
  background-color: red;
}

/* Add some hover effects to buttons */
.form-container .btn:hover, .open-button:hover {
  opacity: 1;
}
#text{
width:100%;


}
.text{
width:100%;
height:30px;


}
.msg{
  width:100%;

}

  
 
  
  </style>
<script>
function openForm() {
  document.getElementById("setSmsForm").style.display = "block";
}

function closeForm() {
  document.getElementById("setSmsForm").style.display = "none";
}
function openBalanceRange() {
  document.getElementById("setBalance").style.display = "block";
}

function closeBalanceRange() {
  document.getElementById("setBalance").style.display = "none";
}
function openEmailForm() {
  document.getElementById("emailForm").style.display = "block";
}
function openFormRate() {
  document.getElementById("rateForm").style.display = "block";
}
function closeFormRate() {
  document.getElementById("rateForm").style.display = "none";
}

function closeEmailForm() {
  document.getElementById("emailForm").style.display = "none";
}
function openSetEmail() {
  document.getElementById("setForm").style.display = "block";
}
function openSetPhone() {
  document.getElementById("setPhone").style.display = "block";
}
function closeSetEmail() {
  document.getElementById("setForm").style.display = "none";
}

</script>
  
 
  </head>
<tr>
    <td colspan="2">
       <table border="1" class="table-bordered" width="100%" style="background-image:img/bg.jpg; #002F74; border-radius:20px; border-color:white;">
          <tr>
             <td width="20%" style="background-color: white" valign="top">
				 <?php
                    include('admindashboard.php');
                 ?>
             </td>
             <td width="80%" valign="top"><br>
            <table class="table-stripped" border="0" width="98%" align="center"  >
            <td>
<div class="card1" style="width: 24.5%; height:350px; background-color:white; border-radius:30px; padding:30px; box-shadow :1px 1px 20px red;">
    <div class="card-body">
    <h5 class="card-title" style="color:silver;">Send Emails to Rate Payers</h5>
    <h6 class="card-subtitle mb-2 text-muted"><a href="sendEmails.php"><img src="img/gmail_icon.jpg" style="margin-left:0px;margin-top:10px; " class="img-circle" width="50" height="50"></a><br></h6>
    <p class="card-text">Email Rate Payers with owing balances.</p>
    
    <a href="sendEmails.php" class="card-link">Click Here</a>
    
    <p class="card-text-last" style="color:red;">Total Amount in Debtors.</p>
    <p class="badge" style="color:white; background-color:brown; font-size:10px;">  <?php
             
            $tot = getTotalDebt();
            $totalDebt = @$tot[0]['totalDebt'];
          
          
            
            echo  "$$totalDebt ZW";
            
            ?></p>
      <button style="width:fit-content; font-size:11px;" type="submit"  class="btn btn-primary  btn-ms" onclick="openBalanceRange()">SMS Using Balance & Acc </button>
  </div>
  
</div>
<div class="card2" style="width: 24.5%;height:350px; background-color:white; border-radius:30px; padding:30px; margin-right:0px; box-shadow :1px 1px 20px green;">
    <div class="card-body">
    <h5 class="card-title" style="color:silver; ">Send Emails to Rate Payers By Range</h5>
    <h6 class="card-subtitle mb-2 text-muted"><a href="OrderNoReport.php"><img src="img/law.jpg" style=" margin-left:0px;margin-top:10px;  " class="img-circle" width="50" height="50"></a><br></a><br></h6>
    <p class="card-text">Email Rate Payers by range.</p>
    
    <button style="font-size:11px;"type="submit"  class="btn btn-primary  btn-ms" onclick="openFormRate()">Send Emails</button>
    
    <p class="card-text-last" style="color:red;">Total Handed Over</p>
    <p class="badge" style="color:green; background-color:yellow;">  <?php
             $count =0;
            $tot = getTotalRatePayers();
          
            foreach($tot as $total){
             
              $tot_r = @$total['acc'];
              $count = $count + 1;
            
            }
            
            echo  $count;
            
            ?></p>
    
  </div>
</div>
<div class="card3" style="width: 24.5%; height:350px; background-color:white; border-radius:30px; padding:30px; box-shadow :1px 1px 20px gold;">
    <div class="card-body">
    <h5 class="card-title" style="color:silver; ">Set Emails for Rate Payers</h5>
    <h6 class="card-subtitle mb-2 text-muted"><a href="#"><img src="img/settings.jpg" style=" margin-left:0px;margin-top:10px;  " class="img-circle" width="50" height="50"></a><br></h6>
    <p class="card-text">Set Emails for Rate Payers.</p>
    
    <button style="font-size:11px;" type="submit"  class="btn btn-primary  btn-ms" onclick="openSetEmail()">Set Emails</button>
    <p class="card-text">Set Phone Numbers for Rate Payers.</p>
    <button style="font-size:11px;" type="submit"  class="btn btn-primary  btn-ms" onclick="openSetPhone()">Set Phone</button>
    <p class="card-text-last" style="color:red;">Accounts Without Emails</p>
    <p class="badge" style="color:white; background-color:blue;">  <?php
          
          $counta =0;
          $tot = noEmails();
        
          foreach($tot as $total){
           
            $tot_r = @$total['acc'];
            $counta = $counta + 1;
          
          }
          
          echo  $counta;




            
            ?></p>
  </div>
</div>
<div class="card3" style="width: 24.5%; height:350px; background-color:white; border-radius:30px; padding:30px; box-shadow :1px 1px 20px silver;">
    <div class="card-body">
    <h5 class="card-title" style="color:silver;">Set sms Contacts for Rate Payer</h5>
    <h6 class="card-subtitle mb-2 text-muted"><a href="#"><img src="img/settings.jpg" style=" margin-left:0px;margin-top:10px;  " class="img-circle" width="50" height="50"></a><br></h6>
    <button style="width:fit-content;font-size:11px;" type="submit"  class="btn btn-primary  btn-ms" onclick="openForm()">Send SMS Messages</button>
    
    <p class="card-text-last" style="color:red;font-size:10px;">SMS Credits</p>
    <p class="badge" style="color:white; background-color:blue; font-size:10px;">  <?php
          
          $count =0;
          $tot = getTotalRatePayers();
          $LaInf = getCompanyDetails();
$tot = @$LaInf[0]["smsCredit"];

        
          
          echo  $tot;




            
            ?></p><br>
      <button style="width:fit-content;font-size:11px;" type="submit"  class="btn btn-warning  btn-ms"><a href="UnsentPdf.php"> Debtors SMS Analysis</a></button>
      <button style="width:fit-content;font-size:11px;" type="submit"  class="btn btn-success  btn-ms"><a href="../SMS/smsBalance.php"> SMS Balance Enquiry</a></button>
      
  </div>
</div>


      
            
            
            
      
            </td>
           
            </tr>
            <tr><td>
            <div class="heading" style="width: 98%; height:2%; background-color:white; border-radius:4px; padding:30px;  margin-top:1%">
            <p class="card-text" style="text-shadow :1px 1px 20px brown;">Client Satisfaction</p>
            </div>
            
            </td>

            
            </tr>
            <tr>
            <td>
            <div class="card3" style="width: 98%; height:350px; background-color:white; border-radius:3px; padding:30px; ">
    <div class="card-body">
    <h5 class="card-title" style="color:silver;">New Developments in Promun Notices</h5>
    <h6 class="card-subtitle mb-2 text-muted"><a href="#"><img src="img/settings.jpg" style=" margin-left:0px;margin-top:10px;  " class="img-circle" width="50" height="50"></a><br></h6>
    <p class="card-text">Thank you for taking the initiative to E-Promun Platform</p>
    
   
    <p class="card-text-last" style="color:red;">In this section You can send Your Queries</p>
    <button type="submit"  class="btn btn-success  btn-ms" onclick="openEmailForm()">Send Queries</button>
 
  </div>
</div>


<div class="form-popup" id="emailForm" style="padding:15px;">
  <form form action="debtors.php" method="POST" class="form-container">
    <h5 style="text-shadow :1px 1px 20px gold; color:brown;">Send Email</h5>

    <label for="email" style="text-shadow :1px 1px 20px red; color:red; font-size:9px;">Your Email Address</label><br>
    <input type="email" class="input" name="email" required><br>
    <textarea rows="4" class="input" cols="22" name="msg">

    </textarea><br>
    <button type="submit" class="btn" name="sendEmail">Send</button>
    <button type="submit" class="btn cancel" onclick="closeEmailForm()">Close</button><br>
    

<?php   
  if(isset($_POST['sendEmail']))
  {
  $from = $_POST['email'];
  $body = $_POST['msg'];
 
  if(empty($from) or empty($body) ){
      echo "<script>alert('Warning: Missing data'); location='debtors.php'</script>";
  }else{
    $seq = rand(50000,100000);
    $status = 0;
    $date = date('Y-m-d');
    $subject = "Customer Request From ePromun";
    $to = "promunsupport@axissol.com";
    $save = saveEmails($seq, $to, $from, $subject, $body,$status, $date);
   
   echo "<script>alert('Success: Request succefully submited Via Email'); location='debtors.php'</script>";
   
  
  }
  
}






?>
  </form>
</div>
<div class="form-popup" id="rateForm" style="padding:15px;">
  <form form action="debtors.php" method="POST" class="form-container">
    <h5 style="text-shadow :1px 1px 20px gold; color:brown;">Send Email</h5>
    <input type="text" class="input" placeholder ="Subject"  name="subject" required><br>
    <input type="number" class="input" placeholder ="Start Account"  name="start" required><br>
    <input type="number" class="input" placeholder = "End Account" name="last" required>
    <textarea rows="4"  cols="22" name="msg">

    </textarea>
    <button type="submit" class="btn" name="deliver">Send</button>
    <button type="submit" class="btn cancel" onclick="closeFormRate()">Close</button><br>
    

<?php   
 if(isset($_POST['deliver']))
 {
 $start = $_POST['start'];
 $end = $_POST['last'];
 $msg = $_POST['msg'];
 $subject = $_POST['subject'];

 if(empty($start) or empty($end) or empty($msg) or empty($subject)){
     echo "<script>alert('Warning: Bulk emails cant be sent'); location='debtors.php'</script>";
 }else{
   
   queEmail();
   DeleteRejectedEmail();
  
 
  
  //save accounts without emails separately for the range specified
  $reportEmail  = rejEmail($start,$end);
  foreach($reportEmail as $reject){
   $acc =@$reject['acc'];
   $name =@$reject['name'];
   $bal =@$reject['balance'];
   
   
    $saveRejectE = saveRejectEmail($acc,$name,$bal);
    
   
   
 
  }
  $takeAcc  = takeAccDetailsEmail($start,$end);
  if(empty($takeAcc)){
    echo "<script>alert('Warning: No accounts with emails '); location='unsentEmailsPdf.php'</script>";
  }
 else{
  foreach($takeAcc as $emailAcc){
    $acc =@$emailAcc['acc'];
    $to =@$emailAcc['email'];
    $bal =@$emailAcc['balance'];
    $name =@$emailAcc['name'];
    $status= 0;
    $seq = rand(30000,9000000000000);
    $body = "Greetings $name Holder of Account $acc your balance is $bal $msg";
    $action = "Sent Emails";
    $date = date('Y-m-d');
    
    
    $save_email_que = saveQueEmails($seq, $body,$status, $date, $subject, $to, $fromEmail );
   
  }
  print_r($save_email_que);
  //echo "<script>alert('Success:( $save_email_que) Emails submited for delivery '); location='../DB/sendEmail.php'</script>";
  /*if($save_email_que['status']='ok'){
    echo "<script>alert('Success: Emails submited for delivery to seq $seq to $to from $fromEmail subject $subject body $body status $status date $date '); location='../DB/sendEmail.php'</script>";
    
      

    }*/
}
}
 }
 







?>
  </form>
</div>
<div class="form-popup" id="setForm" style="padding:15px;">
  <form form action="debtors.php" method="POST" class="form-container">
    <h5 style="text-shadow :1px 1px 20px gold; color:red;">Set Rate Payer Email</h5>
    <input type="number"  type="number" class="form-control input-sm" class="input" placeholder="Enter Rate Payer Account" name="acc" required>
    <input class="input" type="email" class="form-control input-sm" placeholder="Email Address" name="email" required><br>
    <button type="submit" class="btn" name="setEmail">Submit</button>
    <button type="submit" class="btn cancel" onclick="closeSetEmail()">Close</button><br>
    

<?php   
  if(isset($_POST['setEmail']))
  {
  
  $acc = $_POST['acc'];
  $email = $_POST['email'];
  if(empty($acc) or empty($email)){
      echo "<script>alert('Warning: No Data Provided'); location='setEmails.php'</script>";
  }else{
     $status = getAllRatePayer($acc);
     if(@$status['status'] == 'False'){

      //$rslt["msg"] = "No such Account!";
      echo "<script>alert('Warning: No Such account($acc)'); location='debtors.php'</script>";
      
      
     
      
  }
  else{
      $save_email = saveEmail($email,$acc);
      // $rslt["msg"] = "Success";
      echo "<script>alert('Successfully set account ($acc) email'); location='debtors.php'</script>";

  }

 
}

}






?>
  </form>
</div>
</div>
<div class="form-popup" id="setPhone" style="padding:15px;">
  <form form action="debtors.php" method="POST" class="form-container">
    <h5 style="text-shadow :1px 1px 20px gold; color:red;">Set Rate Payer Phone</h5>
    <input type="number" class="input" placeholder="Enter Rate Payer Account" name= "acc">
    <input class="input" type="number" class="form-control input-sm" placeholder="Enter Phone" name="phone" ><br>
    <button type="submit" class="btn" name="setPhone">Submit</button>
    <button type="submit" class="btn cancel" onclick="closeSetEmail()">Close</button><br>
    

<?php   
  if(isset($_POST['setPhone']))
  {
  
  $acc = $_POST['acc'];
  $phone = $_POST['phone'];
  if(empty($acc) or empty($phone)){
      echo "<script>alert('Warning: No Data Provided here'); location='debtors.php'</script>";
  }else{
     $status = getPayer($acc);
     if(@$status['status'] == 'False'){

      //$rslt["msg"] = "No such Account!";
      echo "<script>alert('Warning: No Such account($acc)'); location='debtors.php'</script>";
      
      
     
      
  }
  else{
      $save_phone = savePhone($phone,$acc);
     // print_r($status);
      // $rslt["msg"] = "Success";
      echo "<script>alert('Successfully set account ($acc) phone'); location='debtors.php'</script>";

  }

 
}

}






?>
  </form>
</div>
<div class="form-popup" id="setSmsForm" style="padding:15px;">
  <form form action="debtors.php" method="POST" class="form-container">
    <h5 style="text-shadow :1px 1px 20px gold; color:red;">Send Bulk SMS</h5>
    
    <input type="number" class="input" placeholder= "Start Account" name="startAcc" required><br>
    <input type="number" class="input" placeholder= "End Account"  name="lastAcc" required><br>
    <textarea rows="4" class="input" cols="22" name="msg">
    </textarea><br>
    <button type="submit" class="btn" name="send">Send</button>
    <button type="submit" class="btn cancel" onclick="closeForm()">Close</button><br>
    </form>

<?php   
  if(isset($_POST['send']))
  {
    
  $fromAcc = $_POST['startAcc'];
  $endAcc = $_POST['lastAcc'];
  $msgStart = $_POST['msg'];
 
  if(empty($fromAcc) or empty($endAcc) or empty($msgStart)){
      echo "<script>alert('Warning: Bulk messages cant be sent'); location='debtors.php'</script>";
  }else{
    $txtid = 0;
    //Delete existing existing sms in both dbs
    DeleteSMS();
    DeleteRejectedSMS();
    //take details from debtors accounts
   $takeAcc  = takeAccDetails($fromAcc,$endAcc);
   //save accounts with invalid phone numbers separately for the range specified
   $reportNoMobile  = takeAccMobile($fromAcc,$endAcc);
   foreach($reportNoMobile as $reject){
    $acc =@$reject['acc'];
    $name =@$reject['name'];
    $bal =@$reject['balance'];
    $saveReject = saveRejects($acc,$name,$bal);
   }
   
   if(empty($takeAcc)){
    echo "<script>alert('Error: No accounts with phone numbers '); location='UnsentPdf.php'</script>";
   }else{
   foreach($takeAcc as $smsAcc){
    
     $acc = @$smsAcc['acc'];
     
     $phone = @$smsAcc['cell'];
     $bal =@$smsAcc['balance'];
     $name =@$smsAcc['name'];
     $statsms = "unsent";
     $txtid = $txtid + 1;
     $msg = "Greetings $name Holder of Account $acc your balance is $bal,$msgStart";
    // $insert = saveAccSms($acc,$phone,$bal,$statsms,$msg,$txtid,$name,$passCode);
     //$prePhone =@$smsAcc['cell'];
     //$phone = substr($prePhone ,0,10);
    
    
    if(strlen($phone) < 12){
      $invalid = "Invalid phone number $phone";
      $saveInvalidPhone = saveInvalid($acc,$name,$bal,$invalid);
      print_r("Invalid");
      
    }
    if(strlen($phone) >= 12){
    
     
     $insert = saveAccSms($acc,$phone,$bal,$statsms,$msg,$txtid,$name,$passCode);
     
  
     //take audit details
     $action = "Sent Emails";
     $date = date('Y-m-d');
     $time = date('H:i:s');
     $insertUserDetails = saveAudit($passCode,$action,$date,$time,$name);
     if(isset($insert)){
      echo "<script>alert('Success: Messages submited for delivery '); location='../SMS/sms.php'</script>";
       

     }
  
     
    }

   }
  
   
  }
  
  }
  
}






?>
  
</div>
<div class="form-popup" id="setBalance" style="padding:15px;">
  <form form action="debtors.php" method="POST" class="form-container">
    <h5 style="text-shadow :1px 1px 20px gold; color:red;">Send Bulk SMS</h5>
    
    <input type="number" class="input" placeholder= "Start Account" name="startAcc" required><br>
    <input type="number" class="input" placeholder= "End Account"  name="lastAcc" required><br>
    <input type="number" class="input" placeholder= "Minimun Balance" name="minBal" required><br>
    <input type="number" class="input" placeholder= "Maximun Balance"  name="maxBal" required><br>
    <textarea rows="4" class="input" cols="22" name="msg">
    </textarea><br>
    <button type="submit" class="btn" name="sendBal">Send</button>
    <button type="submit" class="btn cancel" onclick="closeBalanceRange()">Close</button><br>
    

<?php   
  if(isset($_POST['sendBal']))
  {
  $fromAcc = $_POST['startAcc'];
  $endAcc = $_POST['lastAcc']; 
  $minBal = $_POST['minBal'];
  $maxBal = $_POST['maxBal'];
  $msgStart = $_POST['msg'];
  
  if(empty($fromAcc) or empty($endAcc) or empty($msgStart) or empty($minBal) or empty($maxBal)){
      echo "<script>alert('Warning: Bulk messages cant be sent'); location='debtors.php'</script>";
  }else{
    $txtid = 0;
    //Delete existing existing sms in both dbs
    DeleteSMS();
    DeleteRejectedSMS();
    //take details from debtors accounts
   $takeAcc  = takeAccDetailsBal($fromAcc,$endAcc,$minBal,$maxBal);
   //save accounts with invalid phone numbers separately for the range specified
   $reportNoMobile  = takeAccMobileBal($fromAcc,$endAcc,$minBal,$maxBal);
   foreach($reportNoMobile as $reject){
    $acc =@$reject['acc'];
    $name =@$reject['name'];
    $bal =@$reject['balance'];
    $saveReject = saveRejects($acc,$name,$bal);
   }

   if(empty($takeAcc)){
    echo "<script>alert('Error: No account fits this category '); location='debtors.php'</script>";
   }else{
   foreach($takeAcc as $smsAcc){
     $acc =@$smsAcc['acc'];
     $prePhone =@$smsAcc['phone'];
     $phone = substr($prePhone ,0,10);
     $bal =@$smsAcc['balance'];
     $name =@$smsAcc['name'];
     $statsms = "unsent";
     $txtid = $txtid + 1;
     $msg = "Greetings $name Holder of Account $acc your balance is $bal,$msgStart";
    // $insert = saveAccSms($acc,$phone,$bal,$statsms,$msg,$txtid,$name,$passCode);
     
    if(strlen($phone) <> 10){
      $invalid = "Invalid phone number $phone";
      $saveInvalidPhone = saveInvalid($acc,$name,$bal,$invalid);
      
    }
    if(strlen($phone)==10){
   
     
     $insert = saveAccSms($acc,$phone,$bal,$statsms,$msg,$txtid,$name,$passCode);
     
  
     //take audit details
     $action = "Sent Emails";
     $date = date('Y-m-d');
     $time = date('H:i:s');
     $insertUserDetails = saveAudit($passCode,$action,$date,$time,$name);
     if(isset($insert)){
      echo "<script>alert('Success: Messages submited for delivery '); location='../SMS/sms.php'</script>";
       

     }
     
    }

   }
  
   
  }
  
  }
  
}






?>
  </form>
</div>
            
            </td>
          </tr>
      </table>
    </td>
</tr>
<?php
include('footer1.php');
?>