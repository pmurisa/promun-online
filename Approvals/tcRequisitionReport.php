<?php
include('header.php');
require "DBAPI.php";
?>
<tr>
    <td colspan="2">
       <table border="1" class="table-bordered" width="100%">
          <tr>
             <td width="20%" style="background-color: white" valign="top">
				 <?php
                    include('approvaldashboard.php');
                 ?>
             </td>
             <td width="80%" valign="top"><br>
            <table class="table-stripped" border="0" width="98%" align="center">
            <tr>
                <td width="">
                	<input type="date" class="form-control input-sm" name="fromDate">
                        
                </td>
                <td width="1%"></td>
                <td width="">
                	<input type="date" class="form-control input-sm" name="toDate">
                        
                </td>
                <td width="1%"></td>
               
                    <td width="">
                	<select class="form-control input-sm" name="type">
                        <option>.....Select Requisition.....</option>
                        <option>All</option>
                       <option>Approved</option>
                       <option>Rejected</option>
                    </select>
                </td>
                <td width="1%"></td>
                <td width="5%"><button type="submit" name="btnsearch" class="btn btn-default btn-sm glyphicon glyphicon-search"></button></td>
            </tr>
             <tr>
             	<td colspan="7" align="center" height="10">
                </td>
             </tr>
             <tr>
        <td colspan="7" align="center"><?php
            if(isset($_POST['btnsearch']))
            {
                $date = $_POST['fromDate'];
                $toDate=  $_POST['toDate'];
                $type = $_POST['type'];

				if(empty($date) or $type == ".....Select Requisition....." or empty($toDate) )
				{
					echo "<p style='color: red'>Select all details</p>";
                }
               
				else
				{
					if($date <> " " or $type <> ".....Select Requisition....." )
					{
                        //$orders = getOrdersReport($ord);
                        
               
							DeleteOrderView();
							if($type == "Approved")
							{
								insertInOrderView($date,$toDate,$type);
							}
							else
							{
								insertInOrderView($date,$toDate,$type);
							}
							
							//echo "<a href='ReportTamplate.php' target='_blank'><strong>View Orders</strong></a>";
						
					echo "<table class='table-bordered' width='100%' align='center'>";
						   {
							$type = getApprovedReport($type);
							
						echo "<tr style=' font-weight: bolder; color: white' bgcolor='#002F74'>";
                        echo "<td style='font-size: 12px'>"."REQ No."."</td>";
                        echo "<td style='font-size: 12px'>DESCRIPTION</td>";
                        echo "<td style='font-size: 12px'>REQUISITION TYPE</td>";
                        echo "<td style='font-size: 12px'>REQUISTED BY</td>";
                        echo "<td style='font-size: 12px'>REQUISITION STATUS</td>";
                        echo "<td style='font-size: 12px'>USER TO APPROVE</td>";
							echo "</tr>";
								
                $ods =  getRequisitionsReport($date,$toDate);
                foreach($ods as $pr){
                $reqNo = @$pr["reqNo"];
				$reqDate = @$pr["reqDate"];
				$reqDesc = @$pr["reqDesc"];
				$reqCode = @$pr["passCode"];
				$captCode = @$pr["captCode"];
				$authCode = @$pr["authCode"];
				$ordType = @$pr["ordType"];
				$reqType = @$pr["reqType"];
				$reqStatus = @$pr["reqStatus"];
				$authNumber =  @$pr["authNumber"];
				//get requisition lines
				$lines =  getRequisitionLines($reqNo);
				//get Order types
				$description = getOrderTypes($ordType);
                $order_name = @$description[0]["ordDescrip"];
				if($reqStatus == "*"){
					$reqStatus = "UnAuthorised";
				}
				if($reqStatus == "R"){
					$reqStatus = "Approved";
				}
				if($reqStatus == "x"){
					$reqStatus = "Rejected";

				}
				if($reqStatus == "C"){
					$reqStatus = "Completed";

				}
				if($reqType == "N"){
					$reqType = "Non Stock";

				}
				if($reqType == "O"){
					$reqType = "Out Of stock";

				}
				if($reqType == "I"){
					$reqType = "Into Stock";

				}
									echo "<tr>";
										echo "<td style='font-size: 12px'>".$reqNo."</td>";
										echo "<td style='font-size: 12px'>".$reqDesc."</td>";
                                        echo "<td style='font-size: 12px'>".$reqType."</td>";
                                        echo "<td style='font-size: 12px'>".$reqCode."</td>";
										echo "<td style='font-size: 12px'>".$reqStatus."</td>";
										echo "<td style='font-size: 12px'>".$authCode."</td>";
										
										
									echo "</tr>";
                                }
                             
							
						   }
                     echo "</table>";
                     if(!empty($reqNo)){
                         echo "<table class='table-bordered' width='100%' align='center'>";
                        echo "<tr>";
                        echo "<a href='allRequisitionsReport.php' target='_blank' style='color:red; '><strong>View Report</strong></a>";
                        echo "</tr>";
                    }
				
					}
					
				}
                
            }
           
            ?></td>
            </tr>
         </table>
             </td>
          </tr>
      </table>
    </td>
</tr>
<?php
include('footer.php');
?>