<?php

include('header.php');
require "DBAPI.php";
require "AutoSendEmails.php";
$passCode = $_SESSION['pCode'];
if(empty($passCode)){
    header ("location:login.php");
}else{
 
    // take comapny details
    $codata = getCompanyDetails();
   
    if(empty($codata)){

        header ("location:LoadContent.php");  
    }
    //sync all emails to gmail
    else{
        $UserData = getUserDetails($passCode);
        $lockUser = @$UserData[0]['lockUser'];
        $maxAuth = @$UserData[0]['maxAuth'];
        $user = @$UserData[0]['passCode'];
        $name =@$codata[0]["description"];
        $vat =@$codata[0]["vat-reg-no"];
        $info = countOrders($passCode);
        $num_orders = @$info['result'];
        //Count the number of new requisitions per user
        $count = countNewRequisitions($passCode);
        $new_Requisitions = @$count[0]['todayRequisitions']; 
        if(empty($new_Requisitions)){
          $new_Requisitions = 0;
        } 
         //Count the number of approved requisitions per user
        $countApproved = countApprovedRequisitions($passCode);
        $approved_Requisitions = @$countApproved[0]['approvedRequisitions']; 
        if(empty($approved_Requisitions)){
          $approved_Requisitions = 0;
        }
         //Count the number of new orders per user
        $count_orders = countNewOrders($passCode);
        $new_orders = @$count_orders[0]['newOrders'];
        if(empty($new_orders)){
          $new_orders = 0;
        }
         //Count the number of approved orders per user
        $count_approved = countApprovedOrders($passCode);
        $approved_orders = @$count_approved[0]['approvedOrders'];
        if(empty($approved_orders)){
          $approved_orders = 0;
        }
         //Count the number of rejected requisitions per user
        $count_rejected = countRejectedRequisitions($passCode);
        $rejected_Requisitions = @$count_rejected[0]['todayRequisitions']; 
        if(empty($rejected_Requisitions)){
          $rejected_Requisitions = 0;
        } 
         //Count the number of complete orders per user
        $count_complete_orders = countCompleteOrders($passCode);
        $complete_orders = @$count_complete_orders[0]['completeOrders']; 
        if(empty($complete_orders)){
          $complete_orders = 0;
        } 
        
        
        /*set_time_limit(100); // make it run forever
        while(!sleep(20)) {
            autoSend();
            sleep(10);
        }*/
        
    }
    
     
     

    

}






?>
<html>
  <head>
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
   <script type="text/javascript">
      google.charts.load('current', {'packages':['gauge']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Label', 'Value'],
          ['Sugar', 80],
          ['Tissues', 55],
          ['Tar', 68],
          ['Tea', 68]
        ]);

        var options = {
          width: 400, height: 120,
          redFrom: 90, redTo: 100,
          yellowFrom:75, yellowTo: 90,
          minorTicks: 5
        };

        var chart = new google.visualization.Gauge(document.getElementById('chart_div'));

        chart.draw(data, options);

        setInterval(function() {
          data.setValue(0, 1, 40 + Math.round(60 * Math.random()));
          chart.draw(data, options);
        }, 13000);
        setInterval(function() {
          data.setValue(1, 1, 40 + Math.round(60 * Math.random()));
          chart.draw(data, options);
        }, 5000);
        setInterval(function() {
          data.setValue(2, 1, 60 + Math.round(20 * Math.random()));
          chart.draw(data, options);
        }, 26000);
      }
    </script>
  </head>

 
<tr>
    <td colspan="2">
       <table border="1" class="table-bordered" width="100%" style="background-color: #002F74; border-radius:20px; border-color:white;">
          <tr>
             <td width="20%" style="background-color: white" valign="top">
				 <?php
                    include('approvaldashboard.php');
                 ?>
             </td>
             <td width="80%" valign="top"><br>
            <table class="table-stripped" border="0" width="98%" align="center"  >
            <td>
            <p id="chart_div" style="width: 400px; height: 120px;"></p>
            
            
            </td>
            </tr>
            <tr>
            <td>
            
            
            </td>
          </tr>
      </table>
    </td>
</tr>
<?php
include('footer1.php');
?>