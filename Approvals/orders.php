<?php
include('header.php');
require "DBAPI.php";
$passCode = $_SESSION['pCode'];
if(empty($passCode)){
    header ("location:login.php");
}else{
    $UserData = getUserDetails($passCode);
    $lockUser = @$UserData[0]['lockUser'];
    $maxAuth = @$UserData[0]['maxAuth'];
   
    

}
?>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
<tr>
    <td colspan="2">
       <table border="1" class="table-bordered" width="100%">
          <tr>
             <td width="20%" style="background-color: white" valign="top">
				 <?php
                    include('approvaldashboard.php');
                 ?>
             </td>
             <td width="80%" valign="top"><br>
                <!--------------------------1st table for new orders--------------------------------->
                     <table class="table-responsive" width="95%" align="center">
                	<?php
                           $orders = getOrders($passCode);
                           //print_r($orders);
                              
                    if(empty($orders)){
                        echo "<tr>";
                        echo "<td colspan='7'><label style='color: red'><i>No New Captured Orders<i></label><td>";
                        echo "</tr>";

                       }else{
					
							echo "<tr style='color: #5FA5B3'>";
								echo "<td colspan='7' style='color: green'><strong>"."Available Captured Orders"."</strong><td>";
							echo "</tr>";
							echo "<tr style='color: white' bgcolor='#002F74'>";
								echo "<td width='10%'>"."ORDER No"."<td>";
								echo "<td width='43%'>"."DESCRIPTION"."<td>";
                                echo "<td width='15%'>"."DATE CAPTURED"."<td>";
                                echo "<td width='15%'>"."ORDER TYPE"."<td>";
								echo "<td width='12%'align='left'>"."VIEW"."<td>";
                            echo "</tr>";
                            foreach($orders as $pr){
                               
                                echo "<tr>";
                                    echo "<td width='10%'>".@$pr["ordNo"]."<td>";
                                    echo "<td width='43%'>".@$pr["ordDesc"]."<td>";
                                    echo "<td width='15%'>".@$pr["ordDate"]."<td>";
                                    echo "<td width='15%'>".@$pr["ordType"]."<td>";
                                    //echo "<td>".@$pr["reqType"]."<td>";
                                    echo "<td width='12%'><a href='NewOrders.php?ordNo=".@$pr["ordNo"]."''>view more details</a><td>";
                                echo "</tr>";
                                echo "<tr>";
                                    echo "<td colspan='7' height='2'>".""."<td>";
                                echo "</tr>";
                            }
							
							
                           }
						
					
				
					?>
                </table><br>
                <!--------------------------2nd table for approved orders--------------------------------->
                    <table class="table-responsive" width="95%" align="center">
                	<?php
                       $approved_orders = getOrdersApproved($passCode);
                       
                      
                       if(empty($approved_orders)){
                        echo "<tr>";
                        echo "<td colspan='7'><label style='color: red'><i>No Approved Orders Yet<i></label><td>";
                        echo "</tr>";

                       }
                      else{
                        echo "<tr style='color: #5FA5B3'>";
                            echo "<td colspan='7' style='color: blue'><strong>"."Approved  Orders"."</strong><td>";
                        echo "</tr>";
                        echo "<tr style='color: white' bgcolor='#002F74'>";
                        echo "<td width='10%'>"."ORDER No"."<td>";
                        echo "<td width='43%'>"."DESCRIPTION"."<td>";
                        echo "<td width='15%'>"."DATE CAPTURED"."<td>";
                        echo "<td width='15%'>"."ORDER TYPE"."<td>";
                            echo "<td width='12%' align='left'>"."VIEW"."<td>";
                        echo "</tr>";
                        foreach($approved_orders as $pr){
                           
                            echo "<tr>";
                            echo "<td width='10%'>".@$pr["ordNo"]."<td>";
                            echo "<td width='43%'>".@$pr["ordDesc"]."<td>";
                            echo "<td width='15%'>".@$pr["ordDate"]."<td>";
                            echo "<td width='15%'>".@$pr["ordType"]."<td>";
                                echo "<td width='12%'><a href='approvedOrders.php?ordNo=".@$pr["ordNo"]."''>view more details</a><td>";
                            echo "</tr>";
                            echo "<tr>";
                                echo "<td colspan='7' height='2'>".""."<td>";
                            echo "</tr>";
                        }
                       }
                    


					?>
                </table><br>
                 <!--------------------------3rd table for cancelled orders--------------------------------->
                 <table class="table-responsive" width="95%" align="center">
                	<?php
                       $cancelled_orders = getCancelledOrders($passCode);
                       
                      
                       if(empty($cancelled_orders)){
                        echo "<tr>";
                        echo "<td colspan='7'><label style='color: red'><i>No Cancelled Orders<i></label><td>";
                        echo "</tr>";

                       }
                      else{
                        echo "<tr style='color: #5FA5B3'>";
                            echo "<td colspan='7' style='color: red'><strong>"."Cancelled Orders"."</strong><td>";
                        echo "</tr>";
                        echo "<tr style='color: white' bgcolor='#002F74'>";
                        echo "<td width='10%'>"."ORDER No"."<td>";
                        echo "<td width='43%'>"."DESCRIPTION"."<td>";
                        echo "<td width='15%'>"."DATE CAPTURED"."<td>";
                        echo "<td width='15%'>"."ORDER TYPE"."<td>";
                            echo "<td width='12%' align='left'>"."VIEW"."<td>";
                        echo "</tr>";
                        foreach($cancelled_orders as $pr){
                            
                            echo "<tr>";
                            echo "<td width='10%'>".@$pr["ordNo"]."<td>";
                            echo "<td width='43%'>".@$pr["ordDesc"]."<td>";
                            echo "<td width='15%'>".@$pr["ordDate"]."<td>";
                            echo "<td width='15%'>".@$pr["ordType"]."<td>";
                                echo "<td width='12%'><a href='cancelledOrders.php?ordNo=".@$pr["ordNo"]."''>view more details</a><td>";
                            echo "</tr>";
                            echo "<tr>";
                                echo "<td colspan='7' height='2'>".""."<td>";
                            echo "</tr>";
                        }
                       }
                    


					?>
                </table><br>
                      <!--------------------------3rd table for invoiced orders--------------------------------->
                 <table class="table-responsive" width="95%" align="center">
                	<?php
                       $invoiced_orders = getInvoicedOrders($passCode);
                       
                      
                       if(empty($invoiced_orders)){
                        echo "<tr>";
                        echo "<td colspan='7'><label style='color: red'><i>No Invoiced Orders<i></label><td>";
                        echo "</tr>";

                       }
                      else{
                        echo "<tr style='color: #5FA5B3'>";
                            echo "<td colspan='7' style='color: red'><strong>"."Invoiced Orders"."</strong><td>";
                        echo "</tr>";
                        echo "<tr style='color: white' bgcolor='#002F74'>";
                        echo "<td width='10%'>"."ORDER No"."<td>";
                        echo "<td width='43%'>"."DESCRIPTION"."<td>";
                        echo "<td width='15%'>"."DATE CAPTURED"."<td>";
                        echo "<td width='15%'>"."ORDER TYPE"."<td>";
                            echo "<td width='12%' align='left'>"."VIEW"."<td>";
                        echo "</tr>";
                        foreach($invoiced_orders as $pr){
                            
                            echo "<tr>";
                            echo "<td width='10%'>".@$pr["ordNo"]."<td>";
                            echo "<td width='43%'>".@$pr["ordDesc"]."<td>";
                            echo "<td width='15%'>".@$pr["ordDate"]."<td>";
                            echo "<td width='15%'>".@$pr["ordType"]."<td>";
                                echo "<td width='12%'><a href='invoicedOrders.php?ordNo=".@$pr["ordNo"]."''>view more details</a><td>";
                            echo "</tr>";
                            echo "<tr>";
                                echo "<td colspan='7' height='2'>".""."<td>";
                            echo "</tr>";
                        }
                       }
                    


					?>
                </table><br>
               
                      <!--------------------------3rd table for invoiced orders--------------------------------->
                 <table class="table-responsive" width="95%" align="center">
                	<?php
                       $complete_orders = getCompletedOrders($passCode);
                       
                      
                       if(empty($complete_orders)){
                        echo "<tr>";
                        echo "<td colspan='7'><label style='color: red'><i>No Complete Orders<i></label><td>";
                        echo "</tr>";

                       }
                      else{
                        echo "<tr style='color: #5FA5B3'>";
                            echo "<td colspan='7' style='color: red'><strong>"."Complete Orders"."</strong><td>";
                        echo "</tr>";
                        echo "<tr style='color: white' bgcolor='#002F74'>";
                        echo "<td width='10%'>"."ORDER No"."<td>";
                        echo "<td width='43%'>"."DESCRIPTION"."<td>";
                        echo "<td width='15%'>"."DATE CAPTURED"."<td>";
                        echo "<td width='15%'>"."ORDER TYPE"."<td>";
                            echo "<td width='12%' align='left'>"."VIEW"."<td>";
                        echo "</tr>";
                        foreach($complete_orders as $pr){
                            
                            echo "<tr>";
                            echo "<td width='10%'>".@$pr["ordNo"]."<td>";
                            echo "<td width='43%'>".@$pr["ordDesc"]."<td>";
                            echo "<td width='15%'>".@$pr["ordDate"]."<td>";
                            echo "<td width='15%'>".@$pr["ordType"]."<td>";
                                echo "<td width='12%'><a href='completeOrders.php?ordNo=".@$pr["ordNo"]."''>view more details</a><td>";
                            echo "</tr>";
                            echo "<tr>";
                                echo "<td colspan='7' height='2'>".""."<td>";
                            echo "</tr>";
                        }
                       }
                    


					?>
                </table><br>
                     
               
             </td>
          </tr>
      </table>
    </td>
</tr>
<?php
include('footer.php');
?>
