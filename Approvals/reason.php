<?php 
	
    require "DBAPI.php";
    require "ODBCAPI.php";
    $passCode = $_SESSION['pCode'];
if(empty($passCode)){
    header ("location:login.php");
}else{
    $UserData = getUserDetails($passCode);
    $lockUser = @$UserData[0]['lockUser'];
    $maxAuth = @$UserData[0]['maxAuth'];
    $emailFrom = @$UserData[0]['email'];
    $maxAuth = @$UserData[0]['maxAuth'];
    $reqNo = $_GET['reqNo'];
    $auth = validateApprover2($reqNo);
    $authUser = @$auth[0]['authCode'];
    $authNumber = @$auth[0]['authNumber'];
    $allAuths = @$auth[0]['allAuths'];
    //take the auth numbers
    $separate_auths = explode(";", $allAuths);
    $auth1 = $separate_auths[0]; 
    $auth2 = $separate_auths[1];
    $auth3 = $separate_auths[2];
    $auth4 = $separate_auths[3];
    //take requisition amount
    $ReqAmt = getRequisitionLines($reqNo);
    $amt = @$ReqAmt[0]['totalAmt'];
    //take email details
    $ReqDate = checkReq($reqNo);
    $w_date = @$ReqDate[0]['reqDate'];
    $getEmailDate = getEmailDetails($w_date);
    $emailTo = @$getEmailDate[0]['from'];
    //Email constants
    $emailStatus = 0;
    $emailDate = date('Y-m-d');
    $emailCode = rand(50000,150000);
    
    if($passCode != $authUser){
        echo "<script>alert('Error!! This Requisition can only be approved by user($authUser)'); location='home.php'</script>";

    }

}
   

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>E-Promun</title>

<!----------------------------------------------------------- Bootstrap --------------------------------------------------------------->
<link href="css/bootstrap.css" rel="stylesheet">
<link rel="icon" href="img/axislogo.jpg">

<style>
body
{

	
	 	
}
#fill{
	margin-top:100px;
	
	
    }
#text{
    width:300px;
}
</style>
</head>
<body>

<form method="post" style="  box-shadow :1px 1px 20px blue; width: 20%; border-radius: 10px 10px 10px; margin:auto; margin-top: 15%; " >
<br>
	<table border="0" width="95%" align="center">
    	<tbody>
       
        <tr>
            <td height="10"></td>
        </tr>
        <tr>
            <td height="32">
                <div class="input-group">
                      
                      <input id="text" type="text" class="form-control input-sm" placeholder="Reason" name="reason" value="<?php echo isset($_POST['reason']) ? $_POST['reason'] : '' ?>" autofocus>
                 </div>
            </td>
        </tr>
   
 <tr> <td height="34" align="center"><button type="submit" name="log" class="btn btn-primary btn-block btn-ms">OK</button></td>
  
    	</tr>
        
        <tr>
            <td height="20" align="center">
			<?php
            if(isset($_POST['log']))
                {
                $date = date("Y/m/d");
                $actual_date = str_replace('/','',$date);
                $reason = $_POST['reason'];
                if(empty($reason)){
                    echo "<script>alert('Warning(Reason): cant be empty'); location='home.php'</script>";
                }
                if($maxAuth < $amt){
                    echo "<script>alert('You cant approve!! Reason: Maximum authorisation limit exceeded $maxAuth contact admin '); location='home.php'</script>";
                }
                else{
                if($authNumber == 0){
                    $authNumber = 1;
                }else{
                    $authNumber =$authNumber + 1;
                }
                if($auth2 == ""){
                    $reqStatus = "R";
                    approveRequisition($reason,$actual_date,$authNumber,$date,$reqNo);
                    
                    $emailSubject = "Authorisation Notice for Requisition: $reqNo";
                    $emailBody = "Greetings you are informed that your requisition for the highlighted requisition number has been approved by $passCode , it has been sent to the next process.";
                  
                    $Create = CreateEmails($emailCode, $emailTo, $emailFrom, $emailSubject, $emailBody,$emailStatus, $emailDate);
             
               //$save =  updatePromunRequisitions($reqStatus,$actual_date,$authNumber,$reqNo);
        
                echo "<script>alert('Requistion($reqNo): Has been approved by($passCode)'); location='home.php'</script>";
            }else{
                if($passCode==$auth1 && $auth2 != ""){
                $status = "*";
                updatePartiallySql($reason,$auth2,$reqNo);
               
                $emailSubject = "Authorisation Notice for Requisition: $reqNo";
                $emailBody = "Greetings you are informed that your requisition has been approved by $passCode , now sent to $auth2 for further approval.";
                
                $Create = CreateEmails($emailCode, $emailTo, $emailFrom, $emailSubject, $emailBody,$emailStatus, $emailDate);
                echo "<script>alert('Approved: Now sent to $auth2 for further Authorisations'); location='home.php'</script>";
                }
                if($passCode==$auth1 && $auth2 == ""){
                    $reqStatus = "R";
                    approveRequisition($reason,$actual_date,$authNumber,$date,$reqNo);
                    
                    $emailSubject = "Authorisation Notice for Requisition: $reqNo";
                    $emailBody = "Greetings you are informed that your requisition for the highlighted requisition number has been approved by $auth1 you can proceed with normal processing.";
                   
                    $Create = CreateEmails($emailCode, $emailTo, $emailFrom, $emailSubject, $emailBody,$emailStatus, $emailDate);
                    echo "<script>alert('Approved succesfully by $auth1'); location='home.php'</script>";
                }
                if($passCode==$auth2 && $auth3 != ""){
                    $status = "*";
                    updatePartiallySql($reason,$auth3,$reqNo);
                    
                    $emailSubject = "Authorisation Notice for Requisition: $reqNo";
                    $emailBody = "Greetings you are informed that your requisition has been approved by $passCode , now sent to $auth3 for further approval.";
                   
                    $Create = CreateEmails($emailCode, $emailTo, $emailFrom, $emailSubject, $emailBody,$emailStatus, $emailDate);
                    echo "<script>alert('Approved: Now sent to $auth3 for further Authorisations'); location='home.php'</script>";
                }
                if($passCode==$auth2 && $auth3 == ""){
                        $reqStatus = "R";
                    approveRequisition($reason,$actual_date,$authNumber,$date,$reqNo);
                   
                    $emailSubject = "Authorisation Notice for Requisition: $reqNo";
                    $emailBody = "Greetings you are informed that your requisition for the highlighted requisition number has been approved by $auth2 you can proceed with normal processing.";
                    
                    $Create = CreateEmails($emailCode, $emailTo, $emailFrom, $emailSubject, $emailBody,$emailStatus, $emailDate);
                    echo "<script>alert('Approved succesfully by $auth2'); location='home.php'</script>";
                }
                if($passCode==$auth3 && $auth4 != ""){
                        $status = "*";
                        updatePartiallySql($reason,$auth4,$reqNo);
                       
                        $emailSubject = "Authorisation Notice for Requisition: $reqNo";
                        $emailBody = "Greetings you are informed that your requisition has been approved by $passCode , now sent to $auth4 for further approval.";
                       
                        $Create = CreateEmails($emailCode, $emailTo, $emailFrom, $emailSubject, $emailBody,$emailStatus, $emailDate);
                        echo "<script>alert('Approved: Now sent to $auth4 for further Authorisations'); location='home.php'</script>";
                }
                if($passCode==$auth3 && $auth4 == ""){
                            $reqStatus = "R";
                            approveRequisition($reason,$actual_date,$authNumber,$date,$reqNo);
                           
                            $emailSubject = "Authorisation Notice for Requisition: $reqNo";
                            $emailBody = "Greetings you are informed that your requisition for the highlighted requisition number has been approved by $auth3 you can proceed with normal processing.";
                            
                            $Create = CreateEmails($emailCode, $emailTo, $emailFrom, $emailSubject, $emailBody,$emailStatus, $emailDate);
                            echo "<script>alert('Approved succesfully by $auth3'); location='home.php'</script>";
                }
                if($passCode==$auth4){
                            $reqStatus = "R";
                             approveRequisition($reason,$actual_date,$authNumber,$date,$reqNo);
    
                             $emailSubject = "Authorisation Notice for Requisition: $reqNo";
                             $emailBody = "Greetings you are informed that your requisition for the highlighted requisition number has been approved by $auth4 you can proceed with normal processing.";
                             $Create = CreateEmails($emailCode, $emailTo, $emailFrom, $emailSubject, $emailBody,$emailStatus, $emailDate);
                            echo "<script>alert('Approved: No further authorisation'); location='home.php'</script>";
                }

            }
                }
                
            }
            ?>
            </td>
        </tr>
    </tbody>
    </table>
</form>
<!--<form id="fill"><img src="img/fill.PNG" style="height:120px"></form>
--></div>

</body>
</html>
