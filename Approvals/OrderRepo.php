<?php

require "DBAPI.php";
$tot = 0;
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Reports</title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
</head>
<body><br>
<form method="post" style="  box-shadow :1px 1px 20px gray; width: 60%; border-radius: 10px 10px 10px; margin:auto">
<table width="100%" align="center" border="0">
	<tr>
    
    	<td align="center" height="25"></td>
      
    </tr>
 
    
    <tr>
    	<td align="center">
        	<?php
                $details = ReportOrderViewOrd();
                $ordNo = @$details[0]["ordNo"];
               
				
                    $take = ReportOrderViewDetailsOrd($ordNo);
                   //print_r($from);

                
                if(!empty($take)){
                    foreach($take as $detail){
                        $ordNo = @$detail["ordNo"];
                        $ordDate = @$detail["ordDate"];
                        $ordDesc = @$detail["ordDesc"];
                        $ordType = @$detail["ordType"];
                        $reqNo = @$detail["reqNo"];
                        $brCode = @$detail["brCode"];
                        $invStatus = @$detail["invStatus"];
                        $supplier = @$detail["supplier"];
                        $passCode = @$detail["passCode"];
                        $authCode =  @$detail["authCode"];
                        $ordStatus =  @$detail["ordStatus"];
                        //take supplier details
                        $supplier_name = getSupplierDetails($brCode);
                        $name = @$supplier_name[0]["name"];
                        $acc = @$supplier_name[0]["account"];
                        //take order name
                        $description = getOrderTypes($ordType);
                        $order_name = @$description[0]["ordDescrip"];
                        //get order details
						$OrderDetail =  getOrderDetails($ordNo);
						
                        foreach($OrderDetail as $OrderDetails){
                            $seq = @$OrderDetails["SeqNo"];
                            $dec = @$OrderDetails["descrip"];
                            $alloc = @$OrderDetails["allocation"];
                            $qty = @$OrderDetails["qty"];
                            $total = @$OrderDetails["amt"];
                           //calculate order totals
                            $tot = $tot + $total;
                        }
                        if($invStatus == "i"){
                            $invStatus = "Invoiced";
                        }
                        if($invStatus == "x"){
                            $invStatus = "Cancelled";
                        }
                        if($invStatus == "c"){
                            $invStatus = "Complete";
                        }
                        if($invStatus == "O" or $invStatus =="o"){
                            $invStatus = "Ordered";
                        }
                        if($ordStatus == ""){
                            $ordStatus = "Waiting Approval";
                        }
                        else{
                            $ordStatus = "Approved";
                        }
			
						echo "<table width='90%'>";
							echo "<tr>";
								echo "<td>";
									echo "<div class='panel panel-primary'>";
										echo "<table width='95%' align='center'>";
											echo "<tr>";
												echo "<td valign='middle'><img src='img/National court of arms.jpg' width='60' height='60'></td>";
												echo "<td align='center'>";
													echo "<table width='100%' align='center'>";
					echo "<tr><td align='center'><label style='color: blue; font-size: 14px'>E-PROMUN</label></td></tr>";
														echo "<tr><td align='center'><label style='color: brown'>ORDER REPORT</label></td></tr>";
													echo "</table>";
												echo "</td>";
												echo "<td valign='middle'><img src='img/National court of arms.jpg' width='60' height='60'></td>";
											echo "</tr>";
											echo "<tr>";
												echo "<td height='3' colspan='3' bgcolor='white'></td>";
											echo "</tr>";
											echo "<tr>";
												echo "<td height='3' colspan='3' bgcolor='brown'></td>";
											echo "</tr>";
											echo "<tr>";
												echo "<td height='3' colspan='3' bgcolor='white'></td>";
											echo "</tr>";
											echo "<tr>";
												echo "<td height='3' colspan='3' bgcolor='white'>";
													echo "<table width='100%' class='table-responsive'>";
															echo "<tr>";
																echo "<td width='15%'><strong>Order No</strong></td>";
																echo "<td width='35%'>".": ".$ordNo."</td>";
																echo "<td width='15%'><strong>Description</strong></td>";
																echo "<td width='35%'>".": ".$ordDesc."</td>";
															echo "</tr>";
															echo "<tr>";
																echo "<td width='15%'><strong>Date</strong></td>";
																echo "<td width='35%'>".": ".$ordDate."</td>";
																echo "<td width='15%'><strong>Req No</strong></td>";
																echo "<td width='35%'>".": ".$reqNo."</td>";
															echo "</tr>";;
															
													echo "</table>";
												echo "</td>";
											echo "</tr>";
											echo "<tr>";
												echo "<td colspan='3'>";
													echo "<table width='100%' class='table-bordered'>";
														echo "<tr>";
															echo "<td width='50%'><strong>ITEM</strong></td>";
															echo "<td width='50%'><strong>DESCRIPTION</strong></td>";
															echo "<td width='715%'></td>";
														echo "</tr>";
														echo "<tr>";
															echo "<td width='50%'>"."Supplier Name"."</td>";
                                                             
                                                            echo "<td width='50%'>".$name."</td>";     
															echo "<td width='715%'></td>";
														echo "</tr>";
														echo "<tr>";
															echo "<td width='50%'>"."Description"."</td>";
															echo "<td width='50%'>".$dec."</td>";
															echo "<td width='715%'></td>";
														echo "</tr>";
														echo "<tr>";
															echo "<td width='50%'>"."Allocation"."</td>";
															echo "<td width='50%'>".$alloc."</td>";
															echo "<td width='715%'></td>";
														echo "</tr>";
															echo "<tr>";
															echo "<td width='50%'>"."Quantity"."</td>";
                                                            echo "<td width='50%'>".$qty."</td>";
                                                            
															echo "<td width='715%'></td>";
														echo "</tr>";
														echo "<tr>";
														echo "<td width='50%' >"."Order Status"."</td>";
		
														echo "<td width='50%' >".$ordStatus."</td>";
														echo "<td width='715%'></td>";
													echo "</tr>";
													echo "<tr>";
													echo "<td width='50%' >"."Invoice Status"."</td>";
	
													echo "<td width='50%'>".$invStatus."</td>";
													echo "<td width='715%'></td>";
												echo "</tr>";
												echo "<tr>";
													echo "<td width='50%' >"."Approved By"."</td>";
	
													echo "<td width='50%'>".$authCode."</td>";
													echo "<td width='715%'></td>";
												echo "</tr>";
														echo "<tr>";
															echo "<td width='50%' style='color: red'>"."Total Cost"."</td>";
			
															echo "<td width='50%' style='color: red'>"."$".$tot."</td>";
															echo "<td width='715%'></td>";
														echo "</tr>";
													
														
														
													echo "</table>";
												echo "</td>";
											echo "</tr>";
										echo "</table>";
										echo "</br>";
									echo "</div";
								echo "</td>";
							echo "</tr>";
						echo "</table>";
					}
				}
			?>
        </td>
    </tr>
    <tr>
    	<td align="center" height="5">
       
            <button type="submit" class="btn btn-default btn-xs" name="btnconfirm" style="color: #008000; border-color:#008000; text-decoration: none;"><a href="OrderNoReport.php">Back</a></button>
             <button type="submit" class="btn btn-alert btn-xs" name="btnprint" style="color: #008000; border-color:#008000; text-decoration: none;" onClick="window.print()">Print</button>
            <br><br>
           
        </td>
    </tr>
    <tr>
</table>
</form>
</body>
</html>
<?php
include('footer1.php');
?>
