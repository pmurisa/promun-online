<?php
include('header.php');
require "../Approvals/DBAPI.php";
?>
<tr>
    <td colspan="2">
       <table border="1" class="table-bordered" width="100%">
          <tr>
             <td width="20%" style="background-color: white" valign="top">
				 <?php
                    include('admindashboard.php');
                 ?>
             </td>
             <td width="80%" valign="top"><br>
            <table class="table-stripped" border="0" width="98%" align="center">
            <tr>
            <td width="">
            <input id="text" type="text" class="form-control input-sm" placeholder="Enter Rate Payer Account(Type 'ALL' to send to all)" name="acc" value="<?php echo isset($_POST['acc']) ? $_POST['acc'] : '' ?>" autofocus>
             
             
            </td>
           
                <td width="1%"></td>
                <td width="5%"><button type="submit" name="btnsearch" class="btn btn-default btn-sm glyphicon glyphicon-search"></button></td>
            </tr>
             <tr>
             	<td colspan="7" align="center" height="10">
                </td>
             </tr>
             <tr>
        <td colspan="7" align="center"><?php
            if(isset($_POST['btnsearch']))
            {
                $acc = $_POST['acc'];
                
            
                

				if(empty($acc)  )
				{
					echo "<p style='color: red'>Please Choose rate payer</p>";
                }
               
				else
				{
					if($acc == "ALL" )
					{            
                        $sendEmail = getAllRatePayerArrears();
                      
                        if(empty($sendEmail)){
                            echo "<script>alert('Warning: No Emails with balances Yet');</script>";
                        }else{
                            $codata = getCompanyDetails();
                            
                            $coName =@$codata[0]["description"];
                            $from =@$codata[0]["email"];
                        
                            foreach($sendEmail as $em){
                                $acc = @$em['acc'];
                                $name = @$em['name'];
                                $to = @$em['email'];
                                $balance = @$em['balance'];
                               
                                $seq = rand(10000,1000000); 
                                $date = date("Y-m-d");
                                $subject = "Credit Notice";
                                $status = 0;
                                $body = "Greetings $name, we are kindly reminding you that you have an outstanding balance of $balance to $coName for account $acc ";
                                if(empty($to) or empty($from)){
                                    "No email addresses";  
                                  }else{
                                    $saveEmails = saveEmails($seq, $to, $from, $subject, $body,$status, $date);
                                    
                                  if($saveEmails['status'] =="ok"){
                                    echo "<script>alert('Click Ok to start Processing Emails'); location='../DB/index.php'</script>";   
                                  
                           
                                        
                                    }
                                  }
                                
                                


                            }

                        }
                     
                    
				
                    }
                    if($acc != "ALL"  )
					{            
                        $sendEmailS = getSingleRatePayerArrears($acc);
                     
                        if(empty($sendEmailS)){
                            echo "<script>alert('Warning: No Such account $acc');</script>";
                        }else{
                            $codata = getCompanyDetails();
                            
                            $coName =@$codata[0]["description"];
                            $from =@$codata[0]["email"];
                          //print_r($codata);
                            foreach($sendEmailS as $em){
                                $acc = @$em['acc'];
                                $name = @$em['name'];
                                $to = @$em['email'];
                                $balance = @$em['balance'];
                                $bCode = @$em['bCode'];
                                
                                $bank = @$em['bank'];
                                $seq = rand(10000,1000000); 
                                $date = date("Y-m-d");
                                $subject = "Credit Notice";
                                $status = 0;
                                $body = "Greetings $name, we are kindly reminding you that you have an outstanding balance of $balance to $coName for account $acc. You are advised to make payments via the following platforms: Ecocash Biller code: $bCode, steward bank account: $bank. Kind regards ";
                                //print_r($to);
                                 
                                    $saveEmails2 = saveEmails($seq, $to, $from, $subject, $body,$status, $date);
                                    if($saveEmails2['status'] =="ok"){
                                    echo "<script>alert('Click Ok to start Processing Emails'); location='../DB/index.php'</script>";   
                                  
                           
                                        
                                    }
                                   


                            }

                        }
                     
                    
				
					}
					
				}
                
            }
           
            ?></td>
            </tr>
         </table>
             </td>
          </tr>
      </table>
    </td>
</tr>
<?php
include('footer.php');
?>