<?php
try{
$dbodbc = new PDO('odbc:promun', 'Sysprogress', 'Sysprogress');
$dbodbc->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$dbexd = new PDO('odbc:exd', 'Sysprogress', 'Sysprogress');
$dbexd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (Exception $ex){
    echo "<script>alert('Warning:Promun Database is not Running(Any Process done will not be update to promun soon)');</script>";
    //die();
}
Function GetDbStatus(){
    
    try{
        $dbodbc = new PDO('odbc:promun', 'Sysprogress', 'Sysprogress');
        $dbodbc->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
        $dbexd = new PDO('odbc:exd', 'Sysprogress', 'Sysprogress');
        $dbexd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $result = "Yes";
        } catch (Exception $ex) {

       $result = "No";
   }
 
   return $result;
 }
 function updatePromunRequisitions($reqStatus,$actual_date,$authNumber,$reqNo){
    global $dbexd;
    try {
        
        $sql = $dbexd->prepare('update PUB."cmmreq" set "req-status" = ?, "req-auth-no"= ? , "auth-number"=? where "req-no" = ?');
        $sql->execute(array($reqStatus,$actual_date,$authNumber,$reqNo));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result["status"] = "ok";
        } else {
            $result["status"] = "fail";
        }
    } catch (Exception $ex) {
        $result["status"] = $ex->getMessage();
    }
    return $result;
}

function updatePromun($reqStatus,$reqNo){
    global $dbexd;
    try {
        
        $sql = $dbexd->prepare('update PUB."cmmreq" set "req-status" = ?  where "req-no" = ?');
        $sql->execute(array($reqStatus,$reqNo));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result["status"] = "ok";
        } else {
            $result["status"] = "fail";
        }
    } catch (Exception $ex) {
        $result["status"] = $ex->getMessage();
    }
    return $result;
}
function  updatePromunOrders($passCode,$reqNo){
    global $dbexd;
    try {
        
        $sql = $dbexd->prepare('update PUB."cmmamf" set "pr-type" = ?,printed = 1  where "order-no" = ?');
        $sql->execute(array($passCode,$reqNo));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result["status"] = "ok";
        } else {
            $result["status"] = "fail";
        }
    } catch (Exception $ex) {
        $result["status"] = $ex->getMessage();
    }
    return $result;
}

function partiallyApprovedRequisition($reason,$status,$authNumber,$reqNo){
    global $dbexd;
    try {
        
        $sql = $dbexd->prepare('update PUB."cmmreq" set "all-auth-reasons" = ?, "req-status"=?,"auth-number"=? where "req-no" = ?');
        $sql->execute(array($reason,$status,$authNumber,$reqNo));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result["status"] = "ok";
        } else {
            $result["status"] = "fail";
        }
    } catch (Exception $ex) {
        $result["status"] = $ex->getMessage();
    }
    return $result;
}







