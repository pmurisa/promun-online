<?php
include('header.php');
require "DBAPI.php";
$passCode = $_SESSION['pCode'];
if(empty($passCode)){
    header ("location:login.php");
}else{
    $UserData = getUserDetails($passCode);
    $lockUser = @$UserData[0]['lockUser'];
    $maxAuth = @$UserData[0]['maxAuth'];
   
	$reqNo = $_GET['ordNo'];
	$tot= 0; 

}


?>
<tr>
    <td colspan="2">
       <table border="1" class="table-bordered" width="100%">
          <tr>
             <td width="20%" style="background-color: white" valign="top">
				 <?php
                    include('approvaldashboard.php');
                 ?>
             </td>
             <td width="80%" valign="top">
             	<table width="100%" align="center" border="0">
	<tr>
    	<td align="center" height="25"></td>
    </tr>
    <tr>
    	<td align="center">
        	<?php
			
			$requisitions = getNewOrders($reqNo);
			foreach($requisitions as $pr){
				$ordNo = @$pr["ordNo"];
				$ordDate = @$pr["ordDate"];
				$ordDesc = @$pr["ordDesc"];
				$ordType = @$pr["ordType"];
				$reqNo = @$pr["reqNo"];
				$brCode = @$pr["brCode"];
				$invStatus = @$pr["invStatus"];
				$supplier = @$pr["supplier"];
				$passCode = @$pr["passCode"];
                $captCode =  @$pr["passCode"];
                $ordStatus =  @$pr["ordStatus"];
                //take supplier details
                $supplier_name = getSupplierDetails($brCode);
                $name = $supplier_name[0]["name"];
                $acc = $supplier_name[0]["account"];
                //Extra Order Details
				$OrderDetail =  getOrderDetails($ordNo);
				$whse = @$OrderDetail[0]["Whse"];
				$stkCode = @$OrderDetail[0]["stkCode"];
				
                //take order type descriptions
                $description = getOrderTypes($ordType);
                //print_r($description);
                $order_name = @$description[0]["ordDescrip"];

			    if($invStatus == "i"){
                    $invStatus = "Invoiced";
                }
                if($invStatus == "x"){
                    $invStatus = "Cancelled";
                }
                if($invStatus == "c"){
                    $invStatus = "Complete";
                }
                if($invStatus == "O" or $invStatus =="o"){
                    $invStatus = "Waiting Approval";
                }
                if($ordStatus == ""){
                    $ordStatus = "Waiting Approval";
                }
                else{
                    $ordStatus = "Approved";
                }
				
						echo "<table width='90%'>";
							echo "<tr>";
								echo "<td>";
									echo "<div class='panel panel-primary'>";
										echo "<table width='95%' align='center'>";
											echo "<tr>";
												echo "<td height='3' colspan='3' bgcolor='white'></td>";
											echo "</tr>";
											echo "<tr>";
												echo "<td height='3' colspan='3' bgcolor='white'></td>";
											echo "</tr>";
											echo "<tr>";
												echo "<td height='3' colspan='3' bgcolor='white'>";
													echo "<table width='100%' class='table-responsive'>";
															echo "<tr>";
																echo "<td width='40%'>Order No</td>";
																echo "<td width='60%'>".": ".$ordNo."</td>";
															echo "</tr>";
															echo "<tr>";
																echo "<td width='40%'>Date Captured</td>";
																echo "<td width='60%'>".": ".$ordDate."</td>";
															echo "</tr>";
															echo "<tr>";
																echo "<td width='40%'>Order Type</td>";
																echo "<td width='60%' style='color: blue'>".": ".$order_name."</td>";
                                                            echo "</tr>";
                                                            echo "<tr>";
																echo "<td width='40%'>Requisition No</td>";
																echo "<td width='60%'>".": ".$reqNo."</td>";
															echo "</tr>";
															if($stkCode !=0 or $whse !=0){
															echo "<tr>";
															
															echo "<td width='40%'>Warehouse </td>";
															echo "<td width='60%'>".": ".$whse."</td>";
														echo "</tr>";
														echo "<tr>";
														echo "<td width='40%'>Stock Code </td>";
														echo "<td width='60%'>".": ".$stkCode."</td>";
													echo "</tr>";
												}
													echo "</table>";
												echo "</td>";
											echo "</tr>";
											echo "<tr>";
												echo "<td colspan='3'>";
													echo "<table width='100%' class='table-bordered'>";
														echo "<tr>";
															echo "<td width='34%'><strong>DESCRIPTION</strong></td>";
															echo "<td width='15%'><strong>SUPPLIER</strong></td>";
															echo "<td width='15%'><strong>ORDER STATUS</strong></td>";
															echo "<td width='10%'><strong>INVOICE STATUS</strong></td>";
															echo "<td width='10%'><strong>CAPTURED BY</strong></td>";
															
															
														echo "</tr>";
													
														
														echo "<tr>";
															echo "<td width='34%'>".$ordDesc."</td>";
															echo "<td width='15%'>".$name."</td>";
															echo "<td width='15%'>".$ordStatus."</td>";
															echo "<td width='10%'>".$invStatus."</td>";
															echo "<td width='10%'>".$captCode."</td>";
													
                                                        echo "</tr>";
                                                        	
													echo "</table>";
												echo "</td>";
                                            echo "</tr>";
                                           
                                            echo "<tr>";
												echo "<td colspan='3'>";
													echo "<table width='100%' class='table-bordered'>";
														echo "<tr>";
															echo "<td width='34%'><strong>SEQ No</strong></td>";
															echo "<td width='15%'><strong>Description</strong></td>";
															echo "<td width='15%'><strong>Allocation</strong></td>";
															echo "<td width='10%'><strong>Quantity</strong></td>";
															echo "<td width='10%'><strong>Total</strong></td>";
															
															
                                                        echo "</tr>";
                                                        foreach($OrderDetail as $OrderDetails){
                                                            $seq = @$OrderDetails["SeqNo"];
                                                            $dec = @$OrderDetails["descrip"];
                                                            $alloc = @$OrderDetails["allocation"];
                                                            $qty = @$OrderDetails["qty"];
                                                            $total = @$OrderDetails["amt"];
                                                           
                                                            $tot = $tot + $total;
													
														
														echo "<tr>";
															echo "<td width='34%'>".$seq."</td>";
															echo "<td width='15%'>".$dec."</td>";
															echo "<td width='15%'>".$alloc."</td>";
															echo "<td width='10%'>".$qty."</td>";
															echo "<td width='10%'>"."$".$total."</td>";
													
                                                        echo "</tr>";
                                                    
                                                        }
															
													echo "</table>";
												echo "</td>";
                                            echo "</tr>";
                                            
                                                        //Total Row
                                                        echo "<tr>";
												echo "<td colspan='3'>";
													echo "<table width='100%' class='table-bordered'>";
													
                                                     
                                                            $info = "Total Cost";
													
														
														echo "<tr>";
															echo "<td width='70%' >".$info."</td>";
															echo "<td width='9.5%' style='color: red'>"."$".$tot.".00"."</td>";
															
													
                                                        echo "</tr>";
											
                                                    //include stotres     
															
													echo "</table>";
												echo "</td>";
											echo "</tr>";


										echo "</table>";
										echo "</br>";
									echo "</div";
								echo "</td>";
							echo "</tr>";
						echo "</table>";
            
                    }   
			?>
        </td>
    </tr>
     <tr>
    	<td align="center" height="5">
        	<table class="table-responsive" width="90%" align="center">
            <?php
			
			echo "<tr>";
			echo "<td align='right'>";
			echo "<button type='submit' class='btn btn-default btn-xs' name='btnapprove' style='color:#FF0000; border-color:#FF0000'>APPROVE</button>";
			//echo "<td><a href='approve.php?reqNo=".$reqNo."''>Approve</a><td>";
			echo "</td>";
			
			echo "<td width='2%'></td>";
			
			echo "<td align='left'>";
			echo "<button type='submit' class='btn btn-default btn-xs' name='btnreject' style='color: #008000; border-color:#008000'>DECLINE</button>";
			echo "</td>";
			echo "</tr>";
			
			echo "<tr><td colspan = '3' height = '10'></td></tr>";
			
			
			echo "<tr>";
			echo "<td colspan='3' align='center'>";
			if(isset($_POST['btnapprove']))
			{ 
				if($invStatus == "x"){
					echo "<script>alert('Notice:Order($ordNo) Has already been cancelled and cant be approved'); location='home.php'</script>";
				}if($invStatus == "i"){
					echo "<script>alert('Notice:Order($ordNo) has been Approved already!!'); location='home.php'</script>";
				}
				
				else{
					echo"<tr>";
					echo "<td colspan= '3' align='center' ><a href='orderReason.php?ordNo=".$ordNo."''>Provide Reason</a><td>";
					
					echo"</tr>";
				
			
				}
					
				}
			if(isset($_POST['btnreject']))
			{
				if($invStatus == "x"){
					echo "<script>alert('Notice: Requistion ($ordNo) Has already been Declined'); location='home.php'</script>";
				}else{
					echo "<td><a href='OrderRejecReason.php?ordNo=".$ordNo."''>Provide Reason</a><td>";
				
				
				}	
				}
				
				
			?>
          </table>
        </td>
    </tr>
    <tr>
    	<td align="center" height="15"></td>
    </tr>
</table>
             </td>
          </tr>
      </table>
    </td>
</tr>
<?php
include('footer.php');
?>
