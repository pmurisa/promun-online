<?Php
require('../Approvals/pdf/fpdf.php');
require "DBAPI.php";



class myPDF extends FPDF{
    function  header(){
     $this->image('img/axislogo.jpg',10,10,-200);
     $this->SetFont('Arial','B',14);
     $this->Cell(276,10,'SMS unsent messages Exception Report',0,0,'C');
     $this->Ln();
     $this->SetFont('Times','',12);
     $this->Cell(276,10,'Rate Payer Details',0,0,'C');
     $this->Ln(20);
    }
    function footer(){
        $this->SetY(-15);
        $this->SetFont('Arial','',8);
        $this->Cell(0,10,'Page'.$this->PageNo().'/{nb}',0,0,'C');


    }
    function headerTable(){
        $this->SetFont('Times','B',12);
        $this->Cell(60,10,'Account',1,0,'C');
        $this->Cell(60,10,'Credit',1,0,'C');
        $this->Cell(60,10,'Message',1,0,'C');
        $this->Cell(60,10,'Status',1,0,'C');
        $this->Ln();


    }
    function viewTable(){
        $this->SetFont('Times','',10);
        $status = "Unsent";
        $reason = "There is no phone Number";
       $data =  takeRejected();
       foreach($data as $da){
           $acc= @$da['acc'];
           $name= @$da['name'];
           $bal= @$da['balance'];
        
    $this->Cell(60,10,$acc,1,0,'C');
    $this->Cell(60,10,$bal,1,0,'C');
    $this->Cell(60,10,$reason,1,0,'C');
    $this->Cell(60,10,$status,1,0,'C');
    $this->Ln();
       }







    }




}

$pdf = new myPDF(); 
$pdf->AliasNbPages();
$pdf->AddPage('L','A4',0);
$pdf->headerTable();
$pdf->viewTable();
$pdf->SetFont('Arial','B',12);
$pdf->Output('unsentMessages.pdf','I');
?>