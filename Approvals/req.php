<?php
include('header.php');
require "DBAPI.php";
$passCode = $_SESSION['pCode'];
if(empty($passCode)){
    header ("location:login.php");
}else{
    $UserData = getUserDetails($passCode);
    $lockUser = @$UserData[0]['lockUser'];
    $maxAuth = @$UserData[0]['maxAuth'];
   
	$reqNo = $_GET['reqNo'];
	$tot= 0; 
	$alloc;
 
}


?>
<style>
/* Button used to open the contact form - fixed at the bottom of the page */
.open-button {
  background-color: #555;
  color: white;
  padding: 16px 20px;
  border: none;
  cursor: pointer;
  opacity: 0.8;
  position: fixed;
  bottom: 23px;
  right: 28px;
  width: 280px;
}
#entry{
width:100%;
height:15px;
font-size:10px;


}
/* The popup form - hidden by default */
.form-popup {
  display: none;
  position:fixed;
  bottom: 80px;
  right:100px;
  height:200px;
  border: 3px solid #f1f1f1;
  border-radius:15px;
  z-index: 9;
}

/* Add styles to the form container */
.form-container {
  max-width: 100px;
  padding: 10px;
  background-color:white;
  
}

/* Full-width input fields */
.form-container input[type=number], .form-container input[type=text] {
  width: 20px;
  padding: 15px;
  margin: 5px 0 22px 0;
  border: none;
  background: #f1f1f1;
}

/* When the inputs get focus, do something */
.form-container input[type=number]:focus, .form-container input[type=text]:focus {
  background-color: #ddd;
  outline: none;
}

/* Set a style for the submit/login button */
.form-container .btn {
  background-color: #4CAF50;
  color: white;
  padding: 16px 20px;
  border: none;
  cursor: pointer;
  width: 100%;
  margin-bottom:10px;
  opacity: 0.8;
}

/* Add a red background color to the cancel button */
.form-container .cancel {
  background-color: red;
}

/* Add some hover effects to buttons */
.form-container .btn:hover, .open-button:hover {
  opacity: 1;
}
 

  

</style>
<script>
function openForm() {
  document.getElementById("myForm").style.display = "block";
}

function closeForm() {
  document.getElementById("myForm").style.display = "none";
}
</script>
<tr>
    <td colspan="2">
       <table border="1" class="table-bordered" width="100%">
          <tr>
             <td width="20%" style="background-color: white" valign="top">
				 <?php
                    include('approvaldashboard.php');
                 ?>
             </td>
             <td width="80%" valign="top">
             	<table width="100%" align="center" border="0">
	<tr>
    	<td align="center" height="25"></td>
    </tr>
    <tr>
    	<td align="center">
        	<?php
			
			$requisitions = getNewRequisition($reqNo);
			//print_r($requisitions);
			foreach($requisitions as $pr){
				$reqNo = @$pr["reqNo"];
				$reqDate = @$pr["reqDate"];
				$reqDesc = @$pr["reqDesc"];
				$reqCode = @$pr["passCode"];
				$captCode = @$pr["captCode"];
				$authCode = @$pr["authCode"];
				$ordType = @$pr["ordType"];
				$reqType = @$pr["reqType"];
				$reqStatus = @$pr["reqStatus"];
				$authNumber =  @$pr["authNumber"];
				//get requisition lines
				$lines =  getRequisitionL($reqNo);
				//get Order types
				$description = getOrderTypes($ordType);
                $order_name = @$description[0]["ordDescrip"];
				if($reqStatus == "*"){
					$reqStatus = "UnAuthorised";
				}
				if($reqStatus == "R"){
					$reqStatus = "Approved";
				}
				if($reqStatus == "H"){
					$reqStatus = "On Hold";
				}
				if($reqStatus == "C"){
					$reqStatus = "Complete";
				}
				if($reqStatus == "x"){
					$reqStatus = "Rejected";

				}
				if($reqType == "N"){
					$reqType = "Non Stock";

				}
				if($reqType == "O"){
					$reqType = "Out Of stock";

				}
				if($reqType == "I"){
					$reqType = "Into Stock";

				}
				
						echo "<table width='90%'>";
							echo "<tr>";
								echo "<td>";
									echo "<div class='panel panel-primary'>";
										echo "<table width='95%' align='center'>";
											echo "<tr>";
												echo "<td height='3' colspan='3' bgcolor='white'></td>";
											echo "</tr>";
											echo "<tr>";
												echo "<td height='3' colspan='3' bgcolor='white'></td>";
											echo "</tr>";
											echo "<tr>";
												echo "<td height='3' colspan='3' bgcolor='white'>";
													echo "<table width='100%' class='table-responsive'>";
													echo "<tr>";
																echo "<td width='40%'>Req No</td>";
																echo "<td width='60%'>".": ".$reqNo."</td>";
															echo "</tr>";
															echo "<tr>";
																echo "<td width='40%'>Requisted Date</td>";
																echo "<td width='60%'>".": ".$reqDate."</td>";
															echo "</tr>";
															echo "<tr>";
																echo "<td width='40%'>Requisition Type</td>";
																echo "<td width='60%'>".": ".$reqType."</td>";
															echo "</tr>";
													echo "</table>";
												echo "</td>";
											echo "</tr>";
											echo "<tr>";
												echo "<td colspan='3'>";
													echo "<table width='100%' class='table-bordered'>";
														echo "<tr>";
														echo "<td width='34%'><strong>DESCRIPTION</strong></td>";
															echo "<td width='15%'><strong>ORDER TYPE</strong></td>";
															echo "<td width='15%'><strong>STATUS</strong></td>";
															echo "<td width='10%'><strong>REQUISTED BY</strong></td>";
															echo "<td width='10%'><strong>CAPTURED BY</strong></td>";
															
															
														echo "</tr>";
													
														
														echo "<tr>";
															echo "<td width='34%'>".$reqDesc."</td>";
															echo "<td width='15%'>".$order_name."</td>";
															echo "<td width='15%'>".$reqStatus."</td>";
															echo "<td width='10%'>".$reqCode."</td>";
															echo "<td width='10%'>".$captCode."</td>";
													
														echo "</tr>";
                                                        	
													echo "</table>";
												echo "</td>";
                                            echo "</tr>";
                                            
                                            echo "<tr>";
												echo "<td colspan='3'>";
													echo "<table width='100%' class='table-bordered'>";
                                                    echo "<tr>";
                                                    echo "<td width='34%'><strong>Seq No</strong></td>";
                                                    echo "<td width='15%'><strong>Description</strong></td>";
                                                    echo "<td width='15%'><strong>Allocation</strong></td>";
                                                    echo "<td width='10%'><strong>Quantity</strong></td>";
                                                    echo "<td width='10%'><strong>Cost</strong></td>";
                                                    
                                                    
                                                echo "</tr>";
                                                foreach($lines as $reqDetails){
                                                    $seq = @$reqDetails["seqNo"];
                                                    $dec = @$reqDetails["descrip"];
                                                    $alloc1 = @$reqDetails["alloc"];
                                                    $qty = @$reqDetails["qty"];
													$total = @$reqDetails["amt"];
                                                   
                                                    $tot = $tot + $total;
													
														
                                                    echo "<tr>";
                                                    echo "<td width='34%'>".$seq."</td>";
                                                    echo "<td width='15%'>".$dec."</td>";
                                                    echo "<td width='15%'>".$alloc1."</td>";
                                                    echo "<td width='10%'>".$qty."</td>";
                                                    echo "<td width='10%'>"."$".$total."</td>";
                                            
                                                echo "</tr>";
												
														}
														$alloc = $alloc1;
														
															
													echo "</table>";
												echo "</td>";
                                            echo "</tr>";
                                            
                                                        //Total Row
                                                        echo "<tr>";
												echo "<td colspan='3'>";
													echo "<table width='100%' class='table-bordered'>";
													
                                                     
                                                            $info = "Total Cost";
													
														
														echo "<tr>";
															echo "<td width='70%' >".$info."</td>";
															echo "<td width='9.5%' style='color: red'>"."$".$tot.".00"."</td>";
															
													
                                                        echo "</tr>";
                                                    
                                                        
															
													echo "</table>";
												echo "</td>";
											echo "</tr>";


										echo "</table>";
										echo "</br>";
									echo "</div";
								echo "</td>";
							echo "</tr>";
						echo "</table>";
            
                    }   
			?>
        </td>
    </tr>
     <tr>
    	<td align="center" height="5">
        	<table class="table-responsive" width="90%" align="center">
            <?php
			
			echo "<tr>";
			echo "<td align='right'>";
			echo "<button type='submit' class='btn btn-default btn-xs' name='btnapprove' style='color: #008000; border-color:#008000'>APPROVE</button>";
		
			echo "</td>";
			
			echo "<td width='2%'></td>";
			
			echo "<td align='left'>";
			echo "<button type='submit' class='btn btn-default btn-xs' name='btnreject' style='color:#FF0000; border-color:#FF0000' >DECLINE</button>";
			
			
			echo "</td>";
			
	
			
			echo "</tr>";
			
			echo "<tr><td colspan = '3' height = '10'></td></tr>";
			
			echo "<tr>";
			echo "<td colspan='3' align='center'>";
			if(isset($_POST['btnapprove']))
			{ 
				if($reqStatus == "Rejected"){
					echo "<script>alert('Notice: Requistion($reqNo) Has already been Declined'); location='home.php'</script>";
				}if($reqStatus == "Approved"){
					echo "<script>alert('Notice: Requistion($reqNo) has been Approved already!!'); location='home.php'</script>";
				}
				
				else{
					echo"<tr>";
					echo "<td colspan= '3' align='center' ><a href='reason.php?reqNo=".$reqNo."''>Provide Reason</a><td>";
					
					
					echo"</tr>";
				
				
				}
					
				}
			if(isset($_POST['btnreject']))
			{
				if($reqStatus == "Rejected"){
					echo "<script>alert('Notice: Requistion ($reqNo) Has already been Declined'); location='home.php'</script>";
				}else{
					echo"<tr colspan='1' align = 'center'>";
					echo "<td><a href='RejecReason.php?reqNo=".$reqNo."''>Provide Reason</a><td>";
					echo"</tr>";
				}	
				}
				if(isset($_POST['btnhold']))
			{
				if($reqStatus == "Rejected"){
					echo "<script>alert('Notice: Requistion ($reqNo) Has already been Declined'); location='home.php'</script>";
				}else{
					echo "<td><a href='HoldReason.php?reqNo=".$reqNo."''>Provide Reason</a><td>";
				
				
				}	
				}
				
				
			?>
          </table>
		  <div>
		  <button type="submit" class="btn cancel" onclick="openForm()">View Budget</button><br>
		  </div>
		  <div class="form-popup" id="myForm" style="box-shadow:1px 1px 20px blue; width:10%; padding:20px; margin-left:30%;background-color:white;>
  <form action="req.php" method= "POST" class="form-container">
  <?php
 
  //take vote details
  $year = date('Y');  
  $vote_data =  getVote($year,$alloc);
  $vote_acc = @$vote_data[0]['acc'];
  $balYear = @$vote_data[0]['balYear'];
  $amt= @$vote_data[0]['amt'];
  $budget= @$vote_data[0]['budget'];
  $separate_amt = explode(";", $amt); /*This is correct  */
  $amt1 = @$separate_amt[0]; 
  $amt2 = @$separate_amt[1];
  $amt3 = @$separate_amt[2];
  $amt4 = @$separate_amt[3];
  $amt5 = @$separate_amt[4];
  $amt6 = @$separate_amt[5];
  $amt7 = @$separate_amt[6];
  $amt8 = @$separate_amt[7];
  $amt9 = @$separate_amt[8];
  $amt10 = @$separate_amt[9];
  $amt11 = @$separate_amt[10];
  $amt12 = @$separate_amt[11];
  $amt13 = @$separate_amt[12];
  $separate_budget = explode(";", $budget);
  $budget1 = @$separate_budget[0]; 
  $budget2 = @$separate_budget[1]; 
  $budget3 = @$separate_budget[2];
  $budget4 = @$separate_budget[3]; 
  $budget5 = @$separate_budget[4]; 
  $budget6 = @$separate_budget[5];  
  $budget7 = @$separate_budget[6]; 
  $budget8 = @$separate_budget[7]; 
  $budget9 = @$separate_budget[8]; 
  $budget10 = @$separate_budget[9]; 
  $budget11 = @$separate_budget[10]; 
  $budget12 = @$separate_budget[11]; 
  $budget13 = @$separate_budget[12]; 
  //calculation of total budget
  $tt_budget= $budget1 + $budget2 + $budget3 + $budget4 + $budget5 + $budget6 + $budget7 + $budget8 + $budget9 + $budget10 + $budget11 + $budget12 + $budget13;
  $tt_amount = $amt1 + $amt2 + $amt3 + $amt4 + $amt5 + $amt6 + $amt7 + $amt8 + $amt9 + $amt10 + $amt11 + $amt12 + $amt13;
  //calculation of available budget
  $avail_budget = $tt_budget - $tt_amount;
   ?>
    <h5 style="text-shadow :1px 1px 20px white; color:red;">Budget Available</h5> 
	<label for="email" style="text-shadow :1px 1px 20px red; color:blue; font-size:9px;">Vote Number</label>
    <input type="number" id="entry"  name="votenum" value="<?php echo $alloc; ?>" disabled/>
	<label for="email" style="text-shadow :1px 1px 20px red; color:blue; font-size:9px;">Current Available Budget</label>
    <input type="number"  id="entry" name="budget" value="<?php echo $avail_budget; ?>" disabled/>
    <button type="submit" class="btn cancel" onclick="closeForm()">Close</button><br>
    


  </form>
</div>


        </td>
    </tr>
    <tr>
    	<td align="center" height="15"></td>
    </tr>
</table>
             </td>
          </tr>
      </table>
    </td>
</tr>




<?php
include('footer.php');
?>
