<?Php
require('/pdf/fpdf.php');
require "DBAPI.php";



class myPDF extends FPDF{
    function  header(){
     $this->image('img/axislogo.jpg',10,10,-200);   
     $this->SetFont('Arial','B',14);
     $this->Cell(276,10,'Emails Unsent Report',0,0,'C');
     $this->Ln();
     $this->SetFont('Times','',12);
     $this->Cell(276,10,'Email Delivery Details',0,0,'C');
     $this->Ln(20);
    }
    function footer(){
        $this->SetY(-15);
        $this->SetFont('Arial','',8);
        $this->Cell(0,10,'Page'.$this->PageNo().'/{nb}',0,0,'C');


    }
    function headerTable(){
        $this->SetFont('Times','B',12);
        $this->Cell(60,10,'From',1,0,'L');
        $this->Cell(60,10,'Send To',1,0,'L');
        $this->Cell(30,10,'Date',1,0,'C');
        $this->Cell(60,10,'Status',1,0,'C');
        $this->Cell(60,10,'Reason',1,0,'C');
        $this->Ln();


    }
    function viewTable(){
        $this->SetFont('Times','',10);
        $status = "Not Sent";
        $reason = "No Internet Access";
        
       $data =  sentEmail();
       foreach($data as $da){
           $from= @$da['from'];
           $email= @$da['toEmail'];
           $date= @$da['date'];
    $this->Cell(60,10,$from,1,0,'L');
    $this->Cell(60,10,$email,1,0,'L');
    $this->Cell(30,10,$date,1,0,'C');
    $this->Cell(60,10,$status,1,0,'C');
    $this->Cell(60,10,$reason,1,0,'C');
    $this->Ln();
       }

       




    }




}

$pdf = new myPDF(); 
$pdf->AliasNbPages();
$pdf->AddPage('L','A4',0);
$pdf->headerTable();
$pdf->viewTable();
$pdf->SetFont('Arial','B',12);
$pdf->Output('noNetEmails.pdf','I');

?>