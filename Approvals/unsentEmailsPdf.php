<?Php
require('../Approvals/pdf/fpdf.php');
require "DBAPI.php";



class myPDF extends FPDF{
    function  header(){
        $date = date('Y-m-d');
     $this->image('img/unsent.jpg',10,10,+20);   
     $this->SetFont('Arial','B',14);
     $this->Cell(276,10,'Unsent Emails Exception Report',0,0,'C');
     $this->Cell(276,10,$date,0,0,'C');
     $this->Ln();
     $this->SetFont('Times','',12);
     $this->Cell(276,10,'Email Delivery Details',0,0,'C');
     $this->Ln(20);
    }
    function footer(){
        $this->SetY(-15);
        $this->SetFont('Arial','',8);
        $this->Cell(0,10,'Page'.$this->PageNo().'/{nb}',0,0,'C');


    }
    function headerTable(){
        $this->SetFont('Times','B',12);
        $this->Cell(60,10,'Account',1,0,'C');
        $this->Cell(60,10,'Balance',1,0,'C');
        $this->Cell(60,10,'Status',1,0,'C');
        $this->Cell(60,10,'Reason',1,0,'C');
        $this->Ln();


    }
    function viewTable(){
        $this->SetFont('Times','',10);
        $status = "Submited";
        
       $data =  rejectedEm();
       foreach($data as $da){
           $acc= @$da['acc'];
           $balance= @$da['balance'];
           $status = "Unsent";
           $reason = "No Email Address";
    $this->Cell(60,10,$acc,1,0,'C');
    $this->Cell(60,10,$balance,1,0,'C');
    $this->Cell(60,10,$status,1,0,'C');
    $this->Cell(60,10,$reason,1,0,'C');
    $this->Ln();
       }







    }




}

$pdf = new myPDF(); 
$pdf->AliasNbPages();
$pdf->AddPage('L','A4',0);
$pdf->headerTable();
$pdf->viewTable();
$pdf->SetFont('Arial','B',12);
$pdf->Output('UnsentEmails.pdf','I');
?>