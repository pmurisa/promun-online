<?php
include('header.php');
require "DBAPI.php";
?>
<tr>
    <td colspan="2">
       <table border="1" class="table-bordered" width="100%">
          <tr>
             <td width="20%" style="background-color: white" valign="top">
				 <?php
                    include('approvaldashboard.php');
                 ?>
             </td>
             <td width="80%" valign="top"><br>
            <table class="table-stripped" border="0" width="98%" align="center">
            <tr>
                <td width="">
                	<input type="date" class="form-control input-sm" name="fromDate">
                        
                </td>
                <td width="1%"></td>
                <td width="">
                	<input type="date" class="form-control input-sm" name="toDate">
                        
                </td>
                <td width="1%"></td>
               
                    <td width="">
                	<select class="form-control input-sm" name="type">
                        <option>.....Select Orders.....</option>
                        <option>All</option>
                       <option>Approved</option>
                       <option>Rejected</option>
                    </select>
                </td>
                <td width="1%"></td>
                <td width="5%"><button type="submit" name="btnsearch" class="btn btn-default btn-sm glyphicon glyphicon-search"></button></td>
            </tr>
             <tr>
             	<td colspan="7" align="center" height="10">
                </td>
             </tr>
             <tr>
        <td colspan="7" align="center"><?php
            if(isset($_POST['btnsearch']))
            {
                $date = $_POST['fromDate'];
                $toDate=  $_POST['toDate'];
                $type = $_POST['type'];
                if($date > $toDate){
                    echo "<script color:red;>alert('Error: Start Date Can't be greater than End Date'); location='tcReport.php'</script>";
                }

				if(empty($date) or $type == ".....Select Orders....." or empty($toDate) )
				{
					echo "<p style='color: red'>Select all details</p>";
                }
               
				else
				{
					if($date <> " " or $type <> ".....Select Orders....." )
					{
                        //$orders = getOrdersReport($ord);
                        
               
							DeleteOrderView();
							if($type == "Approved")
							{
								insertInOrderView($date,$toDate,$type);
							}
							else
							{
								insertInOrderView($date,$toDate,$type);
							}
							
							//echo "<a href='ReportTamplate.php' target='_blank'><strong>View Orders</strong></a>";
						
					echo "<table class='table-bordered' width='100%' align='center'>";
						   {
							$type = getApprovedReport($type);
							
						echo "<tr style=' font-weight: bolder; color: white' bgcolor='#002F74'>";
                        echo "<td style='font-size: 12px'>"."ODER No."."</td>";
                        echo "<td style='font-size: 12px'>DESCRIPTION</td>";
                        echo "<td style='font-size: 12px'>SUPPLIER</td>";
                        echo "<td style='font-size: 12px'>SUPPLIER ACC</td>";
                        echo "<td style='font-size: 12px'>ORDER STATUS</td>";
                        echo "<td style='font-size: 12px'>INVOICE STATUS</td>";
                        echo "<td style='font-size: 12px'>APPROVED  BY</td>";
							echo "</tr>";
								
                            $ods = getOrdersReport($date,$toDate);
                            foreach($ods as $pr){
                                $ordNo = @$pr["ordNo"];
                                $ordDate = @$pr["ordDate"];
                                $ordDesc = @$pr["ordDesc"];
                                $ordType = @$pr["ordType"];
                                $reqNo = @$pr["reqNo"];
                                $brCode = @$pr["brCode"];
                                $invStatus = @$pr["invStatus"];
                                $supplier = @$pr["supplier"];
                                $passCode = @$pr["passCode"];
                                $authCode =  @$pr["authCode"];
                                $ordStatus =  @$pr["ordStatus"];
                                //take supplier details
                                $supplier_name = getSupplierDetails($brCode);
                                $name = @$supplier_name[0]["name"];
                                $acc = @$supplier_name[0]["account"];
                                //take order name
                                $description = getOrderTypes($ordType);
                                $order_name = $description[0]["ordDescrip"];
                                if($invStatus == "i"){
                                    $invStatus = "Invoiced";
                                }
                                if($invStatus == "x"){
                                    $invStatus = "Cancelled";
                                }
                                if($invStatus == "c"){
                                    $invStatus = "Complete";
                                }
                                if($authCode==""){
                                    $authCode="In Progress";
                                }
                                if($invStatus == "O" or $invStatus =="o"){
                                    $invStatus = "Ordered";
                                }
                                if($ordStatus == ""){
                                    $ordStatus = "Waiting Approval";
                                }
                                else{
                                    $ordStatus = "Approved";
                                }
									echo "<tr>";
										echo "<td style='font-size: 12px'>".$ordNo."</td>";
										echo "<td style='font-size: 12px'>".$ordDesc."</td>";
                                        echo "<td style='font-size: 12px'>".$name."</td>";
                                        echo "<td style='font-size: 12px'>".$acc."</td>";
										echo "<td style='font-size: 12px'>".$ordStatus."</td>";
										echo "<td style='font-size: 12px'>".$invStatus."</td>";
										echo "<td style='font-size: 12px'>".$authCode."</td>";
										
									echo "</tr>";
                                }
                             
							
						   }
                     echo "</table>";
                     if(!empty($ordNo)){
                         echo "<table class='table-bordered' width='100%' align='center'>";
                        echo "<tr>";
                        echo "<a href='ReportTamplate.php' target='_blank' style='color:red; '><strong>View Report</strong></a>";
                        echo "</tr>";
                    }
				
					}
					
				}
                
            }
           
            ?></td>
            </tr>
         </table>
             </td>
          </tr>
      </table>
    </td>
</tr>
<?php
include('footer.php');
?>