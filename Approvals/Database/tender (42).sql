-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 23, 2019 at 04:19 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tender`
--

-- --------------------------------------------------------

--
-- Table structure for table `award_view`
--

CREATE TABLE `award_view` (
  `id` int(255) NOT NULL,
  `tender_no` int(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `award_view`
--

INSERT INTO `award_view` (`id`, `tender_no`, `type`, `date`) VALUES
(1, 100077, 'Cooperative', '2019-04-08'),
(2, 100077, 'Cooperative', '2019-04-08'),
(3, 100077, 'Cooperative', '2019-04-08'),
(4, 100085, 'Cooperative', '2019-04-11');

-- --------------------------------------------------------

--
-- Table structure for table `bids`
--

CREATE TABLE `bids` (
  `id` int(11) NOT NULL,
  `tender_no` int(255) NOT NULL,
  `price` int(200) NOT NULL,
  `reg_number` int(255) NOT NULL,
  `quality` varchar(255) NOT NULL,
  `duration` int(255) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `type` varchar(255) NOT NULL,
  `price_minimum` double NOT NULL,
  `price_maximum` double NOT NULL,
  `expected_duration` int(255) NOT NULL,
  `expected_quality` varchar(255) NOT NULL,
  `weight` int(255) NOT NULL,
  `allocation_mode` varchar(255) NOT NULL,
  `technical_skills` varchar(255) NOT NULL,
  `methodology` varchar(255) NOT NULL,
  `quality_weight` double NOT NULL,
  `minimum_weight` double NOT NULL,
  `maximum_weight` double NOT NULL,
  `duration_weight` double NOT NULL,
  `method_weight` double NOT NULL,
  `skills_weight` double NOT NULL,
  `expected_method` varchar(255) NOT NULL DEFAULT 'none',
  `expected_skills` varchar(255) NOT NULL DEFAULT 'none'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bids`
--

INSERT INTO `bids` (`id`, `tender_no`, `price`, `reg_number`, `quality`, `duration`, `date`, `time`, `type`, `price_minimum`, `price_maximum`, `expected_duration`, `expected_quality`, `weight`, `allocation_mode`, `technical_skills`, `methodology`, `quality_weight`, `minimum_weight`, `maximum_weight`, `duration_weight`, `method_weight`, `skills_weight`, `expected_method`, `expected_skills`) VALUES
(1, 100077, 5000, 271411, 'hardened tar', 3, '2019-04-08', '22:34:28', 'public', 1000, 4000, 3, 'grinding', 10, 'Cooperative', 'grading', 'resurfacing', 1, 2, 2, 2, 1, 2, 'painting', 'grinding'),
(2, 100077, 2000, 609684, 'soft tar', 2, '2019-04-08', '22:35:12', 'public', 1000, 4000, 3, 'grinding', 10, 'Cooperative', 'digging', 'resurfacing', 1, 2, 2, 2, 1, 2, 'painting', 'grinding'),
(3, 100078, 3000, 271411, 'N/A', 2, '2019-04-09', '17:34:12', 'private', 0, 0, 0, 'none', 0, 'none', 'decorating', 'eating and cleaning', 0, 0, 0, 0, 0, 0, 'none', 'none'),
(4, 100078, 8000, 304446, 'N/A', 255, '2019-04-09', '17:38:08', 'private', 0, 0, 0, 'none', 0, 'none', 'building', 'eating and cleaning', 0, 0, 0, 0, 0, 0, 'none', 'none'),
(5, 100079, 4000, 271411, 'grinding', 4, '2019-04-09', '17:41:06', 'public', 1000, 3000, 2, 'welding', 14, 'Cooperative', 'surfacing', 'welding', 2, 2, 2, 2, 4, 2, 'grinding', 'painting'),
(6, 100079, 9000, 304446, 'welding', 2, '2019-04-09', '17:41:57', 'public', 1000, 3000, 2, 'welding', 14, 'Cooperative', 'painting', 'framing', 2, 2, 2, 2, 4, 2, 'grinding', 'painting'),
(7, 100082, 4000, 304446, 'gray', 3, '2019-04-09', '18:23:08', 'private', 0, 0, 0, 'none', 0, 'none', 'digging', 'building', 0, 0, 0, 0, 0, 0, 'none', 'none'),
(8, 100082, 3000, 271411, 'gray', 4, '2019-04-09', '18:25:06', 'private', 0, 0, 0, 'none', 0, 'none', 'surfacing', 'decorating', 0, 0, 0, 0, 0, 0, 'none', 'none'),
(9, 100083, 7000, 271411, 'greenbeans', 2, '2019-04-11', '21:27:08', 'public', 3400, 5000, 2, 'delta fanta', 10, 'Cooperative', 'general', 'usual', 1, 2, 2, 2, 1, 2, 'drinking', 'eating'),
(10, 100083, 3000, 609684, 'drybeans', 3, '2019-04-11', '21:27:56', 'public', 3400, 5000, 2, 'delta fanta', 10, 'Cooperative', 'fake', 'general', 1, 2, 2, 2, 1, 2, 'drinking', 'eating'),
(11, 100084, 9000, 271411, 'delta meals', 5, '2019-04-11', '21:30:56', 'public', 700, 7500, 3, 'drybeans', 14, 'Competitive', 'dairy', 'smart', 2, 2, 2, 2, 4, 2, 'drinking', 'eating'),
(12, 100084, 800, 609684, 'delta fanta', 1, '2019-04-11', '21:31:35', 'public', 700, 7500, 3, 'drybeans', 14, 'Competitive', 'eating', 'drinking', 2, 2, 2, 2, 4, 2, 'drinking', 'eating'),
(13, 100084, 565775, 206118, 'delta meals', 5, '2019-04-11', '23:14:11', 'public', 0, 0, 0, 'none', 0, 'none', 'fgfhg', 'dfgfh', 0, 0, 0, 0, 0, 0, 'none', 'none'),
(14, 100085, 4000, 206118, 'accer', 2, '2019-04-11', '23:30:59', 'public', 100, 1400, 3, 'accer', 10, 'Cooperative', 'computer skills', 'higher purchase', 2, 2, 2, 2, 1, 1, 'higher purchase', 'computer skills'),
(15, 100085, 1000, 271411, 'hp', 4, '2019-04-11', '23:31:53', 'public', 100, 1400, 3, 'accer', 10, 'Cooperative', 'eating', 'resurfacing', 2, 2, 2, 2, 1, 1, 'higher purchase', 'computer skills'),
(16, 100086, 6500, 271411, 'sand', 3, '2019-04-23', '14:04:33', 'public', 0, 0, 0, 'none', 0, 'none', 'eating', 'eating and cleaning', 0, 0, 0, 0, 0, 0, 'none', 'none'),
(17, 100086, 300, 609684, 'sandwhich', 2, '2019-04-23', '14:05:21', 'public', 0, 0, 0, 'none', 0, 'none', 'hewing', 'furnishing', 0, 0, 0, 0, 0, 0, 'none', 'none'),
(18, 100087, 4000, 271411, 'nashpaints', 3, '2019-04-23', '14:13:31', 'public', 2000, 3000, 2, 'grinding', 9, 'Cooperative', 'surfacing', 'building', 1, 1, 1, 1, 4, 1, 'higher purchase', 'grinding'),
(19, 100087, 3000, 609684, 'nashpaints', 4, '2019-04-23', '14:14:19', 'public', 2000, 3000, 2, 'grinding', 9, 'Cooperative', 'hewing', 'smart', 1, 1, 1, 1, 4, 1, 'higher purchase', 'grinding'),
(20, 100088, 3000, 271411, 'delta', 3, '2019-04-23', '14:21:19', 'public', 2000, 4000, 1, 'nashpaints', 8, 'Competitive', 'surfacing', 'eating and cleaning', 1, 1, 1, 1, 3, 1, 'higher purchase', 'decorating'),
(21, 100088, 4000, 609684, 'nashpaints', 7, '2019-04-23', '14:22:01', 'public', 2000, 4000, 1, 'nashpaints', 8, 'Competitive', 'surfacing', 'smart', 1, 1, 1, 1, 3, 1, 'higher purchase', 'decorating');

-- --------------------------------------------------------

--
-- Table structure for table `bids_details`
--

CREATE TABLE `bids_details` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `tender_no` int(255) NOT NULL,
  `reg_number` int(255) NOT NULL,
  `duration` text NOT NULL,
  `time` time NOT NULL,
  `technical_skills` varchar(255) NOT NULL,
  `methodology` varchar(255) NOT NULL,
  `quality` varchar(255) NOT NULL,
  `price` double NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bids_details`
--

INSERT INTO `bids_details` (`id`, `date`, `tender_no`, `reg_number`, `duration`, `time`, `technical_skills`, `methodology`, `quality`, `price`, `type`) VALUES
(5, '2018-10-27', 0, 0, '', '00:00:00', '', '', '', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `bids_results`
--

CREATE TABLE `bids_results` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `tender_no` int(255) NOT NULL,
  `reg_number` int(255) NOT NULL,
  `duration` text NOT NULL,
  `time` time NOT NULL,
  `technical_skills` varchar(255) NOT NULL,
  `methodology` varchar(255) NOT NULL,
  `quality` varchar(255) NOT NULL,
  `price` double NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bids_results`
--

INSERT INTO `bids_results` (`id`, `date`, `tender_no`, `reg_number`, `duration`, `time`, `technical_skills`, `methodology`, `quality`, `price`, `type`) VALUES
(5, '2018-10-27', 0, 0, '', '00:00:00', '', '', '', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `bid_report`
--

CREATE TABLE `bid_report` (
  `id` int(11) NOT NULL,
  `reg_number` int(255) NOT NULL,
  `tender_no` int(255) NOT NULL,
  `report` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bid_report`
--

INSERT INTO `bid_report` (`id`, `reg_number`, `tender_no`, `report`) VALUES
(9, 1000045, 100069, 'bidded'),
(10, 1000044, 100069, 'bidded'),
(11, 1000045, 100074, 'bidded'),
(12, 1000045, 100075, 'bidded'),
(13, 1000044, 100075, 'bidded'),
(14, 1000045, 100076, 'bidded'),
(15, 1000044, 100076, 'bidded'),
(16, 1000045, 100077, 'bidded'),
(17, 1000044, 100077, 'bidded'),
(18, 1000045, 100078, 'bidded'),
(19, 1000046, 100078, 'bidded'),
(20, 1000045, 100079, 'bidded'),
(21, 1000046, 100079, 'bidded'),
(22, 1000046, 100082, 'bidded'),
(23, 1000045, 100082, 'bidded'),
(24, 1000045, 100083, 'bidded'),
(25, 1000044, 100083, 'bidded'),
(26, 1000045, 100084, 'bidded'),
(27, 1000044, 100084, 'bidded'),
(28, 1000047, 100084, 'bidded'),
(29, 1000047, 100085, 'bidded'),
(30, 1000045, 100085, 'bidded'),
(31, 1000045, 100086, 'bidded'),
(32, 1000044, 100086, 'bidded'),
(33, 1000045, 100087, 'bidded'),
(34, 1000044, 100087, 'bidded'),
(35, 1000045, 100088, 'bidded'),
(36, 1000044, 100088, 'bidded');

-- --------------------------------------------------------

--
-- Table structure for table `codes`
--

CREATE TABLE `codes` (
  `id` int(11) NOT NULL,
  `code` varchar(20) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `codes`
--

INSERT INTO `codes` (`id`, `code`, `name`) VALUES
(3, 'RES100000', 'Pork'),
(4, 'RES100001', 'Bread'),
(6, 'RES100050', 'Mage'),
(5, 'RES111111', 'Gigies'),
(7, 'RES165746', 'Beans');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(11) NOT NULL,
  `department` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `department`) VALUES
(8, 'Reception'),
(9, 'Human resources'),
(10, 'Dinning'),
(11, 'It'),
(12, 'Drivers'),
(13, 'Accounts');

-- --------------------------------------------------------

--
-- Table structure for table `details`
--

CREATE TABLE `details` (
  `id` int(11) NOT NULL,
  `parcel_no` varchar(200) NOT NULL,
  `employee_number` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `details`
--

INSERT INTO `details` (`id`, `parcel_no`, `employee_number`) VALUES
(1, '100053', '10000025');

-- --------------------------------------------------------

--
-- Table structure for table `documentation`
--

CREATE TABLE `documentation` (
  `id` int(255) NOT NULL,
  `upn` varchar(255) NOT NULL,
  `img1` varchar(5000) NOT NULL,
  `img` varchar(2000) NOT NULL,
  `what_action` varchar(255) NOT NULL,
  `date_uploaded` date NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'waiting',
  `company_name` varchar(255) NOT NULL,
  `products` varchar(255) NOT NULL,
  `time_approved` time NOT NULL DEFAULT '00:00:00',
  `date_approved` date NOT NULL DEFAULT '0000-00-00',
  `reg_number` int(255) NOT NULL,
  `notices` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `years` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `documentation`
--

INSERT INTO `documentation` (`id`, `upn`, `img1`, `img`, `what_action`, `date_uploaded`, `status`, `company_name`, `products`, `time_approved`, `date_approved`, `reg_number`, `notices`, `location`, `years`, `username`) VALUES
(1, '1000044', 'document/Relationship Reference Priviledge Murisa .pdf', 'document/Relationship Reference Priviledge Murisa .pdf', 'New', '2003-03-19', 'approved', 'zesa holdings', 'electricity', '10:38:51', '2003-03-19', 609684, 'approved', 'harare', '6', 'delta'),
(2, '1000045', 'document/Relationship Reference Priviledge Murisa .pdf', 'document/Relationship Reference Priviledge Murisa .pdf', 'New', '2003-03-19', 'approved', 'coca cola', 'coke', '10:37:44', '2003-03-19', 271411, 'approved', 'harare', '45', 'coke'),
(3, '1000046', 'document/Relationship Reference Priviledge Murisa .pdf', 'document/Relationship Reference Priviledge Murisa .pdf', 'New', '2009-04-19', 'approved', 'football', 'coke', '17:37:11', '2009-04-19', 304446, 'approved', 'chinhoyi', '4', 'second'),
(4, '1000047', 'document/Relationship Reference Priviledge Murisa .pdf', 'document/Relationship Reference Priviledge Murisa .pdf', 'New', '2011-04-19', 'approved', 'zimstats', 'education metaria', '23:11:29', '2011-04-19', 206118, 'approved sir', 'harare', '4', 'tatenda2');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `username` varchar(30) NOT NULL,
  `name` varchar(50) NOT NULL,
  `department` varchar(100) NOT NULL,
  `dob` date NOT NULL,
  `doj` date NOT NULL,
  `address` varchar(100) NOT NULL,
  `sex` varchar(10) NOT NULL,
  `national_id` varchar(30) NOT NULL,
  `employee_number` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'free'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`username`, `name`, `department`, `dob`, `doj`, `address`, `sex`, `national_id`, `employee_number`, `status`) VALUES
('0774462926', 'patience', 'financial_officer', '2019-02-27', '2019-02-27', '450 highlands harare', 'Female', '67-565768h67', '10000025', 'free');

-- --------------------------------------------------------

--
-- Table structure for table `evaluations`
--

CREATE TABLE `evaluations` (
  `id` int(255) NOT NULL,
  `reg` int(255) NOT NULL,
  `evaluation` text NOT NULL,
  `weight` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `evaluations`
--

INSERT INTO `evaluations` (`id`, `reg`, `evaluation`, `weight`) VALUES
(2, 609684, '0', 10),
(3, 609684, '0', 10),
(4, 609684, '0', 10),
(5, 609684, '0', 10),
(6, 271411, 'evaluation for 271411', 16),
(7, 271411, 'evaluation for 271411', 16);

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

CREATE TABLE `expenses` (
  `id` int(11) NOT NULL,
  `operator` varchar(30) NOT NULL,
  `exp` varchar(50) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `amount` varchar(50) NOT NULL,
  `tender_no` varchar(20) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `expense_payment`
--

CREATE TABLE `expense_payment` (
  `id` int(11) NOT NULL,
  `exp` varchar(30) NOT NULL,
  `operator` varchar(30) NOT NULL,
  `tender_no` varchar(20) NOT NULL,
  `ref` varchar(30) NOT NULL,
  `description` varchar(100) NOT NULL,
  `amount` varchar(50) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `expense_payment`
--

INSERT INTO `expense_payment` (`id`, `exp`, `operator`, `tender_no`, `ref`, `description`, `amount`, `date`) VALUES
(43, 'Delivery', '0773985577', '1000097', '1000092', 'buying meals', '700', '2018-09-19'),
(45, 'Packages', '0773985577', '1000097', '1000094', 'buying meals', '500', '2018-09-19'),
(46, 'Stationery', '0773985577', '1000097', '1000095', 'buying meals', '1500', '2018-09-19'),
(44, 'Transport', '0773985577', '1000097', '1000093', 'buying meals', '300', '2018-09-19');

-- --------------------------------------------------------

--
-- Table structure for table `grading`
--

CREATE TABLE `grading` (
  `id` int(11) NOT NULL,
  `grade` varchar(10) NOT NULL,
  `normal` decimal(10,0) NOT NULL,
  `hourly` decimal(10,0) NOT NULL,
  `overtime` decimal(10,0) NOT NULL,
  `sick` decimal(10,0) NOT NULL,
  `holiday` decimal(10,0) NOT NULL,
  `accident` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grading`
--

INSERT INTO `grading` (`id`, `grade`, `normal`, `hourly`, `overtime`, `sick`, `holiday`, `accident`) VALUES
(1, 'A', '30', '5', '6', '30', '7', '30'),
(2, 'B', '28', '5', '6', '28', '7', '28'),
(3, 'C', '26', '5', '6', '26', '7', '26'),
(4, 'D', '24', '5', '6', '24', '7', '24'),
(5, 'E', '22', '5', '6', '22', '7', '22');

-- --------------------------------------------------------

--
-- Table structure for table `initial_budget`
--

CREATE TABLE `initial_budget` (
  `id` int(11) NOT NULL,
  `price_minimum` double NOT NULL DEFAULT '0',
  `price_maximum` double NOT NULL DEFAULT '0',
  `tender_no` varchar(50) NOT NULL,
  `duration` int(255) NOT NULL DEFAULT '0',
  `quality` varchar(255) NOT NULL DEFAULT 'Not set',
  `status` varchar(255) NOT NULL DEFAULT 'Not set',
  `date_expire` date NOT NULL,
  `weight` int(255) NOT NULL,
  `technical_skills` varchar(255) NOT NULL DEFAULT 'none',
  `methodology` varchar(255) NOT NULL DEFAULT 'none'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `initial_budget`
--

INSERT INTO `initial_budget` (`id`, `price_minimum`, `price_maximum`, `tender_no`, `duration`, `quality`, `status`, `date_expire`, `weight`, `technical_skills`, `methodology`) VALUES
(1, 1000, 4000, '100077', 3, 'grinding', 'complete', '2019-05-01', 10, 'grinding', 'painting'),
(2, 0, 0, '100078', 0, 'none', 'available', '2019-04-30', 0, 'none', 'none'),
(3, 1000, 3000, '100079', 2, 'welding', 'complete', '2019-05-01', 14, 'painting', 'grinding'),
(4, 0, 0, '100082', 0, 'none', 'available', '2019-05-01', 0, 'none', 'none'),
(5, 3400, 5000, '100083', 2, 'delta fanta', 'complete', '2019-05-01', 10, 'eating', 'drinking'),
(6, 700, 7500, '100084', 3, 'drybeans', 'complete', '2019-05-01', 14, 'eating', 'drinking'),
(7, 100, 1400, '100085', 3, 'accer', 'complete', '2019-05-03', 10, 'computer skills', 'higher purchase'),
(8, 0, 0, '100086', 0, 'none', 'available', '2019-04-24', 5, 'none', 'none'),
(9, 2000, 3000, '100087', 2, 'grinding', 'complete', '2019-04-25', 9, 'grinding', 'higher purchase'),
(10, 2000, 4000, '100088', 1, 'nashpaints', 'complete', '2019-04-27', 8, 'decorating', 'higher purchase');

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `login_time` time NOT NULL,
  `logout_time` time NOT NULL,
  `logout` varchar(20) NOT NULL DEFAULT 'none'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log`
--

INSERT INTO `log` (`id`, `username`, `date`, `login_time`, `logout_time`, `logout`) VALUES
(17, '0771234569', '2018-10-23', '14:40:40', '14:43:08', 'no'),
(18, '0773985577', '2018-10-23', '15:11:53', '00:00:00', 'none'),
(19, '0771000000', '2018-10-23', '23:39:26', '00:00:00', 'none'),
(20, '0772000000', '2018-10-23', '23:39:41', '00:00:00', 'none');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `sms` varchar(100) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `sms`, `date`) VALUES
(1, 'Salaries for September 2018 available', '2018-09-14'),
(2, 'Salaries for October 2018 available', '2018-09-15'),
(3, 'Salaries for September 2018 available', '2018-09-18'),
(4, 'Salaries for December 2018 available', '2018-12-01');

-- --------------------------------------------------------

--
-- Table structure for table `parcels`
--

CREATE TABLE `parcels` (
  `id` int(11) NOT NULL,
  `doc_types` varchar(255) NOT NULL,
  `route` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `land` varchar(255) NOT NULL,
  `quantity` varchar(255) NOT NULL,
  `number` int(255) NOT NULL,
  `mobile1` double NOT NULL,
  `item_value` double NOT NULL,
  `name` varchar(255) NOT NULL,
  `mobile2` double NOT NULL,
  `fragile` varchar(255) NOT NULL,
  `pin` int(255) NOT NULL,
  `parcel_no` double NOT NULL,
  `status` varchar(255) NOT NULL,
  `submited` varchar(255) NOT NULL DEFAULT 'no',
  `delivery` varchar(255) NOT NULL DEFAULT 'no',
  `allocated` varchar(255) NOT NULL DEFAULT 'no',
  `delivery_date` date NOT NULL,
  `delivery_time` time NOT NULL,
  `time` time NOT NULL,
  `driver_completed` varchar(255) NOT NULL DEFAULT 'no',
  `second_cell` int(255) NOT NULL,
  `measured_mass` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `parcels`
--

INSERT INTO `parcels` (`id`, `doc_types`, `route`, `address`, `date`, `land`, `quantity`, `number`, `mobile1`, `item_value`, `name`, `mobile2`, `fragile`, `pin`, `parcel_no`, `status`, `submited`, `delivery`, `allocated`, `delivery_date`, `delivery_time`, `time`, `driver_completed`, `second_cell`, `measured_mass`) VALUES
(3, 'Jewelery', 'Harare', 'Dzivarasekwa', '2018-10-27', '06574857', 'Above 2999kgs', 5, 775440413, 788, 'tazive', 774355746, 'Fragile', 10127, 100052, 'pending', 'no', 'no', 'no', '0000-00-00', '00:00:00', '05:11:36', 'no', 774354656, 5),
(2, 'Documents (EmS International)', 'chinhoyi', 'Fdgfdgdfg', '2018-10-27', '03453645', 'Above 2999kgs', 2, 772453647, 789, 'dfsdsf', 774355746, 'Fragile', 11738, 100051, 'pending', 'yes', 'no', 'no', '0000-00-00', '00:00:00', '00:18:38', 'no', 0, 0),
(1, 'Documents (EmS Domestic)', 'Chiredzi', 'Fwweaertr', '2018-10-27', '07869785', 'Above 2999kgs', 8, 776475635, 89, 'hfdhgh', 774355746, 'Fragile', 17694, 100050, 'pending', 'yes', 'no', 'no', '0000-00-00', '00:00:00', '00:12:44', 'no', 0, 0),
(4, 'Documents (EmS Domestic)', 'Masving', 'Gweru', '2018-10-27', '08768576', 'Above 2999kgs', 7, 775440413, 788, 'dggg', 774355746, 'Fragile', 19021, 100053, 'pending', 'no', 'no', 'no', '0000-00-00', '00:00:00', '06:32:49', 'no', 774354657, 78);

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` int(11) NOT NULL,
  `payment_method` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`id`, `payment_method`) VALUES
(5, 'Ecocash'),
(6, 'Telecash'),
(7, 'Onemoney'),
(8, 'Bank');

-- --------------------------------------------------------

--
-- Table structure for table `payment_details`
--

CREATE TABLE `payment_details` (
  `id` int(11) NOT NULL,
  `tender_no` varchar(50) NOT NULL,
  `approval_code` varchar(100) NOT NULL,
  `method` varchar(50) NOT NULL,
  `amount` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `operator` varchar(50) NOT NULL,
  `approved` varchar(255) NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_details`
--

INSERT INTO `payment_details` (`id`, `tender_no`, `approval_code`, `method`, `amount`, `date`, `operator`, `approved`) VALUES
(43, '1000093', 'D40', 'Ecocash', '300', '2018-09-12', 'manager', 'yes'),
(44, '1000094', 'D50', 'Ecocash', '200', '2018-09-12', 'manager', 'yes'),
(45, '1000096', 'e1000', 'Ecocash', '100000', '2018-09-17', 'manager', 'yes'),
(46, '1000097', '0000009', 'Ecocash', '8000', '2018-09-19', 'manager', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `private_tenders`
--

CREATE TABLE `private_tenders` (
  `id` int(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `supplier` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `employee_number` int(255) NOT NULL,
  `date_generated` date NOT NULL,
  `date_expire` date NOT NULL,
  `mobile` int(255) NOT NULL,
  `duration` varchar(255) NOT NULL,
  `date_commence` date NOT NULL,
  `initial_status` varchar(255) NOT NULL,
  `tender_no` int(255) NOT NULL,
  `img1` varchar(2000) NOT NULL,
  `img` varchar(2000) NOT NULL,
  `quality_one` varchar(255) NOT NULL DEFAULT 'none',
  `quality_two` varchar(255) NOT NULL DEFAULT 'none',
  `technical_skills` varchar(255) NOT NULL DEFAULT 'none',
  `methodology` varchar(255) NOT NULL DEFAULT 'none',
  `completed` varchar(255) NOT NULL DEFAULT 'no',
  `weight` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `private_tenders`
--

INSERT INTO `private_tenders` (`id`, `type`, `supplier`, `description`, `employee_number`, `date_generated`, `date_expire`, `mobile`, `duration`, `date_commence`, `initial_status`, `tender_no`, `img1`, `img`, `quality_one`, `quality_two`, `technical_skills`, `methodology`, `completed`, `weight`) VALUES
(4, 'Private', 'coke', 'testing four', 10000025, '2019-04-09', '2019-05-01', 775440413, '6weeks', '2019-05-04', 'submited', 100082, 'document/Relationship Reference Priviledge Murisa .pdf', 'document/1161162320tutorial questions (1) (1).doc', 'gray', 'white', 'building', 'painting', 'no', 20);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(255) NOT NULL,
  `products` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `products`) VALUES
(28, 'coke'),
(29, 'electricity'),
(30, 'coke'),
(31, 'education metaria');

-- --------------------------------------------------------

--
-- Table structure for table `reference`
--

CREATE TABLE `reference` (
  `id` int(11) NOT NULL,
  `tender_no` varchar(50) NOT NULL,
  `method` varchar(50) NOT NULL,
  `ref` varchar(50) NOT NULL,
  `amount` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `registered_vendors`
--

CREATE TABLE `registered_vendors` (
  `id` int(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `products` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `reg_number` int(255) NOT NULL,
  `notices` varchar(255) NOT NULL,
  `time_approved` time NOT NULL,
  `date_approved` date NOT NULL,
  `upn` int(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `confirmed` varchar(255) NOT NULL DEFAULT 'no',
  `access_status` varchar(255) NOT NULL DEFAULT 'activated'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registered_vendors`
--

INSERT INTO `registered_vendors` (`id`, `company_name`, `products`, `status`, `reg_number`, `notices`, `time_approved`, `date_approved`, `upn`, `username`, `confirmed`, `access_status`) VALUES
(24, 'jameson', 'catering services', 'approved', 448795, 'activated', '19:53:32', '2002-03-19', 1000043, 'tanaka', 'no', 'activated'),
(25, 'coca cola', 'coke', 'approved', 271411, 'approved', '10:37:44', '2003-03-19', 1000045, 'coke', 'no', 'activated'),
(26, 'zesa holdings', 'electricity', 'approved', 609684, 'approved', '10:38:51', '2003-03-19', 1000044, 'delta', 'no', 'activated'),
(27, 'football', 'coke', 'approved', 304446, 'approved', '17:37:11', '2009-04-19', 1000046, 'second', 'no', 'activated'),
(28, 'zimstats', 'education metaria', 'approved', 206118, 'approved sir', '23:11:29', '2011-04-19', 1000047, 'tatenda2', 'no', 'activated');

-- --------------------------------------------------------

--
-- Table structure for table `resources`
--

CREATE TABLE `resources` (
  `id` int(11) NOT NULL,
  `code` varchar(20) NOT NULL,
  `boruse` varchar(20) NOT NULL,
  `qty` varchar(20) NOT NULL,
  `department` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `pin` int(255) NOT NULL,
  `reference` double NOT NULL,
  `Remaining` double NOT NULL,
  `total_bought` double NOT NULL DEFAULT '0',
  `total_used` double NOT NULL DEFAULT '0',
  `status` varchar(255) NOT NULL DEFAULT 'pending',
  `cleared_date` date NOT NULL,
  `alert` varchar(255) NOT NULL DEFAULT 'No Alert'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `resources`
--

INSERT INTO `resources` (`id`, `code`, `boruse`, `qty`, `department`, `date`, `pin`, `reference`, `Remaining`, `total_bought`, `total_used`, `status`, `cleared_date`, `alert`) VALUES
(233, 'RES100000', 'bought', '40000', 'Accounts', '2018-09-14', 0, 0, 1703, 40000, 38497, 'pending', '0000-00-00', ''),
(234, 'RES100001', 'bought', '50000', 'Dinning', '2018-09-14', 0, 0, 49221, 50000, 2124, 'pending', '0000-00-00', ''),
(235, 'RES100050', 'bought', '30000', 'Drivers', '2018-09-14', 0, 0, 0, 0, 0, 'pending', '0000-00-00', ''),
(236, 'RES100000', 'used', '200', 'Dinning', '2018-09-14', 884, 200016, 1703, 50000, 2124, 'pending', '0000-00-00', 'Quantity Below 20%'),
(237, 'RES100001', 'used', '234', 'Dinning', '2018-09-14', 2142, 200017, 49221, 50000, 2124, 'pending', '0000-00-00', ''),
(238, 'RES100050', 'used', '345', 'Dinning', '2018-09-14', 5699, 200018, 0, 50000, 2124, 'cleared', '2018-09-14', ''),
(239, 'RES100001', 'used', '300', 'Dinning', '2018-09-14', 1881, 200019, 49221, 50000, 2124, 'pending', '0000-00-00', ''),
(240, 'RES100000', 'used', '100', 'Reception', '2018-09-14', 954, 200020, 1703, 0, 700, 'pending', '0000-00-00', 'Quantity Below 20%'),
(241, 'RES100001', 'used', '300', 'Reception', '2018-09-14', 5302, 200021, 49221, 0, 700, 'pending', '0000-00-00', ''),
(242, 'RES100001', 'used', '300', 'Reception', '2018-09-14', 11, 200022, 49221, 0, 700, 'pending', '0000-00-00', ''),
(243, 'RES100000', 'used', '299', 'Accounts', '2018-09-14', 842, 200023, 1703, 40000, 38497, 'pending', '0000-00-00', 'Quantity Below 20%'),
(244, 'RES100000', 'used', '299', 'Accounts', '2018-09-14', 4563, 200024, 1703, 40000, 38497, 'pending', '0000-00-00', 'Quantity Below 20%'),
(245, 'RES100000', 'used', '233', 'Accounts', '2018-09-14', 3132, 200025, 1703, 40000, 38497, 'pending', '0000-00-00', 'Quantity Below 20%'),
(246, 'RES100000', 'used', '7000', 'Accounts', '2018-09-14', 1744, 200026, 1703, 40000, 38497, 'pending', '0000-00-00', 'Quantity Below 20%'),
(247, 'RES100000', 'used', '5466', 'Accounts', '2018-09-14', 3806, 200027, 1703, 40000, 38497, 'pending', '0000-00-00', 'Quantity Below 20%'),
(248, 'RES100000', 'used', '20000', 'Accounts', '2018-09-14', 2635, 200028, 1703, 40000, 38497, 'pending', '0000-00-00', 'Quantity Below 20%'),
(249, 'RES100000', 'used', '5000', 'Accounts', '2018-09-14', 4576, 0, 1703, 40000, 38497, 'pending', '0000-00-00', 'Quantity Below 20%'),
(250, 'RES100000', 'used', '45', 'Dinning', '2018-09-14', 2768, 200030, 1703, 50000, 2124, 'pending', '0000-00-00', 'Quantity Below 20%'),
(251, 'RES100000', 'used', '1000', 'Dinning', '2018-09-14', 1726, 200031, 1703, 50000, 2124, 'pending', '0000-00-00', 'Quantity Below 20%'),
(252, 'RES100000', 'used', '200', 'Accounts', '2018-09-14', 1538, 200032, 1703, 40000, 38497, 'pending', '0000-00-00', 'Quantity Below 20%');

-- --------------------------------------------------------

--
-- Table structure for table `resource_details`
--

CREATE TABLE `resource_details` (
  `id` int(255) NOT NULL,
  `clearance_code` varchar(255) NOT NULL,
  `used` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `resource_details`
--

INSERT INTO `resource_details` (`id`, `clearance_code`, `used`) VALUES
(1, '200032', '');

-- --------------------------------------------------------

--
-- Table structure for table `response`
--

CREATE TABLE `response` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `number` varchar(255) NOT NULL,
  `feedback` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `clientfeedback` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `results`
--

CREATE TABLE `results` (
  `id` int(255) NOT NULL,
  `tender_no` int(255) NOT NULL,
  `reg` int(255) NOT NULL,
  `score_one` double NOT NULL,
  `score_two` double NOT NULL,
  `score_three` double NOT NULL,
  `score_four` double NOT NULL,
  `score_five` double NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `allocation_mode` varchar(255) NOT NULL,
  `total_score` double NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `results`
--

INSERT INTO `results` (`id`, `tender_no`, `reg`, `score_one`, `score_two`, `score_three`, `score_four`, `score_five`, `date`, `time`, `allocation_mode`, `total_score`, `status`) VALUES
(1, 100077, 271411, 8, 2, 2, 1, -1, '2019-04-08', '22:49:55', 'Cooperative', 12, 'complete'),
(2, 100077, 609684, 2, 2, -2, 1, 1, '2019-04-08', '22:49:55', 'Cooperative', 4, 'old'),
(3, 100079, 271411, 12, -2, -2, -4, -2, '2019-04-09', '17:54:27', 'Cooperative', 2, 'new'),
(4, 100079, 304446, 12, 2, 2, -4, 2, '2019-04-09', '17:54:27', 'Cooperative', 14, 'new'),
(5, 100083, 271411, 8, 2, -1, -1, -2, '2019-04-11', '21:34:15', 'Cooperative', 6, 'new'),
(6, 100083, 609684, 2, -2, -1, -1, -2, '2019-04-11', '21:34:15', 'Cooperative', -4, 'new'),
(7, 100084, 271411, 12, -2, -2, -4, -2, '2019-04-11', '21:35:00', 'Competitive', 2, 'new'),
(8, 100084, 609684, 2, 2, -2, 4, 2, '2019-04-11', '21:35:00', 'Competitive', 8, 'new'),
(9, 100085, 206118, 8, 2, 2, 1, 1, '2019-04-11', '23:38:42', 'Cooperative', 14, 'new'),
(10, 100085, 271411, 2, -2, -2, -1, -1, '2019-04-11', '23:38:42', 'Cooperative', -4, 'new'),
(11, 100087, 271411, 8, -1, -1, -4, -1, '2019-04-23', '14:17:24', 'Cooperative', 1, 'new'),
(12, 100087, 609684, 1, -1, -1, -4, -1, '2019-04-23', '14:17:24', 'Cooperative', -6, 'new'),
(13, 100088, 271411, 1, -1, -1, -3, -1, '2019-04-23', '14:24:38', 'Competitive', -5, 'new'),
(14, 100088, 609684, 1, -1, 1, -3, -1, '2019-04-23', '14:24:38', 'Competitive', -3, 'new');

-- --------------------------------------------------------

--
-- Table structure for table `task`
--

CREATE TABLE `task` (
  `id` int(11) NOT NULL,
  `vehicle_number` double NOT NULL,
  `employee_number` double NOT NULL,
  `task` varchar(255) NOT NULL,
  `destination` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `doc_type` varchar(255) NOT NULL,
  `land` double NOT NULL,
  `mobile` int(11) NOT NULL,
  `parcel_no` double NOT NULL,
  `date` date NOT NULL,
  `task_number` int(255) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'pending',
  `completed` varchar(255) NOT NULL DEFAULT 'no',
  `reallocated` varchar(255) NOT NULL DEFAULT 'no',
  `time_allocated` time NOT NULL,
  `time_confirmed` time NOT NULL,
  `reason` varchar(255) NOT NULL DEFAULT 'No reason'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `task`
--

INSERT INTO `task` (`id`, `vehicle_number`, `employee_number`, `task`, `destination`, `address`, `doc_type`, `land`, `mobile`, `parcel_no`, `date`, `task_number`, `status`, `completed`, `reallocated`, `time_allocated`, `time_confirmed`, `reason`) VALUES
(1, 1029, 10000013, 'go to bluez', 'Bulawayo', 'Fdgfdgdfg', 'Documents (EmS International)', 3453645, 774355746, 100051, '2018-10-27', 19224, 'confirmed', 'no', 'no', '00:19:50', '00:26:29', 'No reason'),
(2, 1029, 10000013, 'go to bluez', 'Bulawayo', 'Fdgfdgdfg', 'Documents (EmS International)', 3453645, 774355746, 100052, '2018-10-27', 6480, 'confirmed', 'no', 'no', '00:19:58', '00:26:29', 'No reason'),
(3, 1030, 10000013, 'go', 'Chiredzi', 'Fwweaertr', 'Documents (EmS Domestic)', 7869785, 774355746, 100050, '2018-10-27', 17678, 'pending', 'no', 'no', '05:34:33', '00:00:00', 'No reason');

-- --------------------------------------------------------

--
-- Table structure for table `tender_available`
--

CREATE TABLE `tender_available` (
  `id` int(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `weight` int(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `employee_id` int(255) NOT NULL,
  `date_generated` date NOT NULL,
  `date_expire` date NOT NULL,
  `mobile` int(255) NOT NULL,
  `material_type` varchar(255) NOT NULL,
  `quantity` varchar(255) NOT NULL,
  `date_commence` date NOT NULL,
  `duration` varchar(255) NOT NULL,
  `allocation_mode` varchar(255) NOT NULL,
  `expected_vendors` int(255) NOT NULL,
  `initial_status` varchar(255) NOT NULL DEFAULT 'pending',
  `tender_no` int(255) NOT NULL,
  `img1` varchar(2000) NOT NULL DEFAULT 'no',
  `img` varchar(2000) NOT NULL DEFAULT 'no',
  `availability` varchar(255) NOT NULL DEFAULT 'no',
  `quality_one` varchar(255) NOT NULL DEFAULT 'none',
  `quality_two` varchar(255) NOT NULL DEFAULT 'none',
  `technical_skills` varchar(255) NOT NULL DEFAULT 'none',
  `methodology` varchar(255) DEFAULT 'none',
  `completed` varchar(255) NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tender_available`
--

INSERT INTO `tender_available` (`id`, `type`, `weight`, `description`, `employee_id`, `date_generated`, `date_expire`, `mobile`, `material_type`, `quantity`, `date_commence`, `duration`, `allocation_mode`, `expected_vendors`, `initial_status`, `tender_no`, `img1`, `img`, `availability`, `quality_one`, `quality_two`, `technical_skills`, `methodology`, `completed`) VALUES
(1, 'Public', 10, 'Road rehabilitation', 10000025, '2019-04-08', '2019-05-01', 775440413, 'ALL', '', '2019-05-04', '5 weeks', 'Cooperative', 3, 'submited', 100077, 'document/Relationship Reference Priviledge Murisa .pdf', 'document/1161162320tutorial questions (1) (1).doc', 'yes', 'hardened tar', 'soft tar', 'surfacing', 'resurfacing', 'yes'),
(2, 'Public', 14, 'delta deliveries', 10000025, '2019-04-09', '2019-05-01', 775440413, 'ALL', '', '2019-05-04', '3 weeks', 'Cooperative', 3, 'submited', 100079, 'document/Relationship Reference Priviledge Murisa .pdf', 'document/1161162320tutorial questions (1) (1).doc', 'yes', 'grinding', 'welding', 'reframing', 'painting', 'yes'),
(3, 'Public', 10, 'verify functionality', 10000025, '2019-04-11', '2019-05-01', 775440413, 'ALL', '', '2019-05-04', '4 months', 'Cooperative', 2, 'submited', 100083, 'document/Relationship Reference Priviledge Murisa .pdf', 'document/1161162320tutorial questions (1) (1).doc', 'yes', 'greenbeans', 'drybeans', 'general', 'usual', 'yes'),
(4, 'Public', 14, 'competitive tender', 10000025, '2019-04-11', '2019-05-01', 775440413, 'ALL', '', '2019-05-04', '7weekdays', 'Competitive', 1, 'submited', 100084, 'document/Relationship Reference Priviledge Murisa .pdf', 'document/1161162320tutorial questions (1) (1).doc', 'yes', 'delta meals', 'delta fanta', 'eating', 'drinking', 'yes'),
(5, 'Public', 10, 'tender from tatenda', 10000025, '2019-04-11', '2019-05-03', 775440413, 'ALL', '', '2019-05-04', '7weekdays', 'Cooperative', 2, 'submited', 100085, 'document/Relationship Reference Priviledge Murisa .pdf', 'document/1161162320tutorial questions (1) (1).doc', 'yes', 'accer', 'hp', 'computer skills', 'higher purchase', 'yes'),
(6, 'Public', 5, 'generation gap', 10000025, '2019-04-23', '2019-04-24', 775440413, 'ALL', '', '2019-04-26', '7weekdays', 'Cooperative', 3, 'submited', 100086, 'document/Relationship Reference Priviledge Murisa .pdf', 'document/1161162320tutorial questions (1) (1) (1).doc', 'yes', 'sand', 'sandwhich', 'decorating', 'higher purchase', 'no'),
(7, 'Public', 9, 'evening', 10000025, '2019-04-23', '2019-04-25', 775440413, 'ALL', '', '2019-05-01', '2', 'Cooperative', 3, 'submited', 100087, 'document/Relationship Reference Priviledge Murisa .pdf', 'document/1161162320tutorial questions (1) (1) (1).doc', 'yes', 'lafarge', 'nashpaints', 'digging', 'drinking', 'yes'),
(8, 'Public', 8, 'competitive', 10000025, '2019-04-23', '2019-04-27', 775440413, 'ALL', '', '2019-05-02', '4', 'Competitive', 1, 'submited', 100088, 'document/Relationship Reference Priviledge Murisa .pdf', 'document/1161162320tutorial questions (1) (1) (1).doc', 'yes', 'delta', 'nashpaints', 'eating', 'drinking', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `tender_evaluation`
--

CREATE TABLE `tender_evaluation` (
  `id` int(255) NOT NULL,
  `tender_no` int(255) NOT NULL,
  `minimum` double NOT NULL,
  `maximum` double NOT NULL,
  `quality` varchar(255) NOT NULL,
  `duration` varchar(255) NOT NULL,
  `score` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tender_feedback`
--

CREATE TABLE `tender_feedback` (
  `id` int(11) NOT NULL,
  `tender_no` int(255) NOT NULL,
  `reg` int(255) NOT NULL,
  `message` text NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `status` varchar(255) NOT NULL,
  `contact_details` varchar(255) NOT NULL,
  `participation_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tender_feedback`
--

INSERT INTO `tender_feedback` (`id`, `tender_no`, `reg`, `message`, `date`, `time`, `status`, `contact_details`, `participation_id`) VALUES
(1, 100077, 271411, 'We are thankful for participating for tender 100077, we are glad to inform that you won the tender', '2019-04-08', '22:51:10', 'Supplier Confirmed', '(+2637777878)(+2637758978)', 7475),
(2, 100077, 271411, 'We are thankful for participating for tender 100077, we are glad to inform that you won the tender', '2019-04-08', '22:51:10', 'Supplier Confirmed', '(+2637777878)(+2637758978)', 4795),
(3, 100079, 304446, 'We are thankful for participating for tender 100079, we are glad to inform that you won the tender', '2019-04-09', '17:55:37', 'Supplier Confirmed', '(+2637777878)(+2637758978)', 6497),
(4, 100079, 304446, 'We are thankful for participating for tender 100079, we are glad to inform that you won the tender', '2019-04-09', '17:55:37', 'Supplier Confirmed', '(+2637777878)(+2637758978)', 6393),
(5, 100083, 609684, 'We are thankful for participating for tender 100083, we are glad to inform that you won the tender', '2019-04-11', '21:34:15', 'new', '(+2637777878)(+2637758978)', 5393),
(6, 100083, 271411, 'We are thankful for participating for tender 100083, we are glad to inform that you won the tender', '2019-04-11', '21:34:15', 'new', '(+2637777878)(+2637758978)', 6210),
(7, 100084, 609684, 'We are thankful for participating for tender , we are glad to inform that you won the tender', '2019-04-11', '21:49:59', 'Supplier Confirmed', '(+2637777878)(+2637758978)', 5296),
(8, 100084, 609684, 'We are thankful for participating for tender 100084, we are glad to inform that you won the tender', '2019-04-11', '21:49:59', 'Supplier Confirmed', '(+2637777878)(+2637758978)', 5366),
(9, 100084, 609684, 'We are thankful for participating for tender 100084, we are glad to inform that you won the tender', '2019-04-11', '21:49:59', 'Supplier Confirmed', '(+2637777878)(+2637758978)', 7700),
(10, 100084, 609684, 'We are thankful for participating for tender 100084, we are glad to inform that you won the tender', '2019-04-11', '21:49:59', 'Supplier Confirmed', '(+2637777878)(+2637758978)', 7800),
(11, 100084, 609684, 'We are thankful for participating for tender 100084, we are glad to inform that you won the tender', '2019-04-11', '21:49:59', 'Supplier Confirmed', '(+2637777878)(+2637758978)', 3107),
(12, 100084, 609684, 'We are thankful for participating for tender 100084, we are glad to inform that you won the tender', '2019-04-11', '21:49:59', 'Supplier Confirmed', '(+2637777878)(+2637758978)', 5637),
(13, 100084, 609684, 'We are thankful for participating for tender 100084, we are glad to inform that you won the tender', '2019-04-11', '21:49:59', 'Supplier Confirmed', '(+2637777878)(+2637758978)', 7966),
(14, 100084, 609684, 'We are thankful for participating for tender 100084, we are glad to inform that you won the tender', '2019-04-11', '21:49:59', 'Supplier Confirmed', '(+2637777878)(+2637758978)', 7163),
(15, 100084, 609684, 'We are thankful for participating for tender 100084, we are glad to inform that you won the tender', '2019-04-11', '21:49:59', 'Supplier Confirmed', '(+2637777878)(+2637758978)', 7293),
(16, 100084, 609684, 'We are thankful for participating for tender 100084, we are glad to inform that you won the tender', '2019-04-11', '21:49:59', 'Supplier Confirmed', '(+2637777878)(+2637758978)', 3377),
(17, 100084, 609684, 'We are thankful for participating for tender 100084, we are glad to inform that you won the tender', '2019-04-11', '21:49:59', 'Supplier Confirmed', '(+2637777878)(+2637758978)', 7191),
(18, 100084, 609684, 'We are thankful for participating for tender 100084, we are glad to inform that you won the tender', '2019-04-11', '21:49:59', 'Supplier Confirmed', '(+2637777878)(+2637758978)', 8358),
(19, 100084, 609684, 'We are thankful for participating for tender 100084, we are glad to inform that you won the tender', '2019-04-11', '21:49:59', 'Supplier Confirmed', '(+2637777878)(+2637758978)', 5692),
(20, 100084, 609684, 'We are thankful for participating for tender 100084, we are glad to inform that you won the tender', '2019-04-11', '21:49:59', 'Supplier Confirmed', '(+2637777878)(+2637758978)', 7603),
(21, 100084, 609684, 'We are thankful for participating for tender 100084, we are glad to inform that you won the tender', '2019-04-11', '21:49:59', 'Supplier Confirmed', '(+2637777878)(+2637758978)', 6293),
(22, 100084, 609684, 'We are thankful for participating for tender 100084, we are glad to inform that you won the tender', '2019-04-11', '21:49:59', 'Supplier Confirmed', '(+2637777878)(+2637758978)', 6944),
(23, 100085, 271411, 'We are thankful for participating for tender 100085, we are glad to inform that you won the tender', '2019-04-11', '23:38:42', 'new', '(+2637777878)(+2637758978)', 8623),
(24, 100085, 206118, 'We are thankful for participating for tender 100085, we are glad to inform that you won the tender', '2019-04-11', '23:38:42', 'new', '(+2637777878)(+2637758978)', 3379),
(25, 100087, 609684, 'We are thankful for participating for tender 100087, we are glad to inform that you won the tender', '2019-04-23', '14:17:24', 'new', '(+2637777878)(+2637758978)', 3161),
(26, 100087, 271411, 'We are thankful for participating for tender 100087, we are glad to inform that you won the tender', '2019-04-23', '14:17:24', 'new', '(+2637777878)(+2637758978)', 7869),
(27, 100088, 609684, 'We are thankful for participating for tender 100088, we are glad to inform that you won the tender', '2019-04-23', '14:24:38', 'new', '(+2637777878)(+2637758978)', 6239);

-- --------------------------------------------------------

--
-- Table structure for table `tender_number`
--

CREATE TABLE `tender_number` (
  `id` int(255) NOT NULL,
  `number` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tender_number`
--

INSERT INTO `tender_number` (`id`, `number`) VALUES
(1, 100088);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(20) NOT NULL,
  `access` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `status` varchar(30) NOT NULL DEFAULT 'Activated',
  `user` varchar(50) NOT NULL,
  `logged_in` varchar(20) NOT NULL DEFAULT 'no',
  `submited_documents` varchar(255) NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `access`, `email`, `status`, `user`, `logged_in`, `submited_documents`) VALUES
(110, '0774000000', 'password', 'financial_officer', 'hazvi@gmail.com', 'Activated', '0774000000', 'no', 'yes'),
(125, '0774462926', '1234password', 'financial_officer', '450 highlands harare', 'Activated', '0774462926', 'no', 'yes'),
(128, 'coke', '1234password', 'vendor', 'pmurisa28@gmail.com', 'Activated', 'coke', 'no', 'yes'),
(127, 'delta', '1234pass', 'vendor', 'pmurisa28@gmail.com', 'Activated', 'delta', 'no', 'yes'),
(129, 'hr manager', 'password', 'hr', 'hr@gmail.com', 'Activated', '', 'no', 'yes'),
(2, 'manager', '1234password', 'MANAGEMENT', '', 'Activated', '0777766483', 'no', 'yes'),
(123, 'priviledge', '1234pass', 'vendor', 'pmurisa28@gmail.com', 'Activated', 'priviledge', 'no', 'yes'),
(130, 'second', '1234password', 'vendor', 'pmurisa28@gmail.com', 'Activated', 'second', 'no', 'yes'),
(126, 'tanaka', '1234pass', 'vendor', 'tanaka@gmail.com', 'Activated', 'tanaka', 'no', 'yes'),
(131, 'tatenda2', '1234pass', 'vendor', 'pmurisa28@gmail.com', 'Activated', 'tatenda2', 'no', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `vehicles_details`
--

CREATE TABLE `vehicles_details` (
  `id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `plate` varchar(255) NOT NULL,
  `make` varchar(255) NOT NULL,
  `vehicle_no` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'available'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicles_details`
--

INSERT INTO `vehicles_details` (`id`, `type`, `plate`, `make`, `vehicle_no`, `date`, `status`) VALUES
(1, 'Private', 'vbg5768', 'Toyota', '1029', '2018-10-27', 'assigned'),
(2, 'Truck', 'yut6758', 'BMW', '1030', '2018-10-27', 'assigned');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_initial`
--

CREATE TABLE `vehicle_initial` (
  `id` int(11) NOT NULL,
  `number` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle_initial`
--

INSERT INTO `vehicle_initial` (`id`, `number`) VALUES
(1, '1030');

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

CREATE TABLE `vendors` (
  `id` int(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `access` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `user` varchar(255) NOT NULL,
  `vendor_number` int(255) NOT NULL,
  `approved` varchar(255) NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendors`
--

INSERT INTO `vendors` (`id`, `username`, `password`, `access`, `email`, `status`, `user`, `vendor_number`, `approved`) VALUES
(41, 'tanaka', '1234pass', 'vendor', 'tanaka@gmail.com', 'Activated', 'tanaka', 1000043, 'no'),
(42, 'delta', '1234pass', 'vendor', 'pmurisa28@gmail.com', 'Activated', 'delta', 1000044, 'no'),
(43, 'coke', '1234pass', 'vendor', 'pmurisa28@gmail.com', 'Activated', 'coke', 1000045, 'no'),
(44, 'second', '1234password', 'vendor', 'pmurisa28@gmail.com', 'Activated', 'second', 1000046, 'no'),
(45, 'tatenda2', '1234pass', 'vendor', 'pmurisa28@gmail.com', 'Activated', 'tatenda2', 1000047, 'no');

-- --------------------------------------------------------

--
-- Table structure for table `vendor_details`
--

CREATE TABLE `vendor_details` (
  `id` int(255) NOT NULL,
  `vendor_number` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendor_details`
--

INSERT INTO `vendor_details` (`id`, `vendor_number`) VALUES
(1, 1000047);

-- --------------------------------------------------------

--
-- Table structure for table `view`
--

CREATE TABLE `view` (
  `id` int(255) NOT NULL,
  `tender_no` int(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `award_view`
--
ALTER TABLE `award_view`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bids`
--
ALTER TABLE `bids`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bids_details`
--
ALTER TABLE `bids_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bids_results`
--
ALTER TABLE `bids_results`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bid_report`
--
ALTER TABLE `bid_report`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `codes`
--
ALTER TABLE `codes`
  ADD PRIMARY KEY (`code`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `details`
--
ALTER TABLE `details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `documentation`
--
ALTER TABLE `documentation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `evaluations`
--
ALTER TABLE `evaluations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `expense_payment`
--
ALTER TABLE `expense_payment`
  ADD PRIMARY KEY (`exp`),
  ADD UNIQUE KEY `unique` (`id`);

--
-- Indexes for table `grading`
--
ALTER TABLE `grading`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `initial_budget`
--
ALTER TABLE `initial_budget`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parcels`
--
ALTER TABLE `parcels`
  ADD PRIMARY KEY (`pin`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`payment_method`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `payment_details`
--
ALTER TABLE `payment_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `private_tenders`
--
ALTER TABLE `private_tenders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reference`
--
ALTER TABLE `reference`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `registered_vendors`
--
ALTER TABLE `registered_vendors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `resources`
--
ALTER TABLE `resources`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `response`
--
ALTER TABLE `response`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `results`
--
ALTER TABLE `results`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `task`
--
ALTER TABLE `task`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tender_available`
--
ALTER TABLE `tender_available`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tender_evaluation`
--
ALTER TABLE `tender_evaluation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tender_feedback`
--
ALTER TABLE `tender_feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tender_number`
--
ALTER TABLE `tender_number`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`username`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `vehicles_details`
--
ALTER TABLE `vehicles_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicle_initial`
--
ALTER TABLE `vehicle_initial`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendors`
--
ALTER TABLE `vendors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_details`
--
ALTER TABLE `vendor_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `view`
--
ALTER TABLE `view`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `award_view`
--
ALTER TABLE `award_view`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `bids`
--
ALTER TABLE `bids`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `bids_details`
--
ALTER TABLE `bids_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `bids_results`
--
ALTER TABLE `bids_results`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `bid_report`
--
ALTER TABLE `bid_report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `codes`
--
ALTER TABLE `codes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `details`
--
ALTER TABLE `details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `documentation`
--
ALTER TABLE `documentation`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `evaluations`
--
ALTER TABLE `evaluations`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `expenses`
--
ALTER TABLE `expenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `expense_payment`
--
ALTER TABLE `expense_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `grading`
--
ALTER TABLE `grading`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `initial_budget`
--
ALTER TABLE `initial_budget`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `parcels`
--
ALTER TABLE `parcels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `payment_details`
--
ALTER TABLE `payment_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `private_tenders`
--
ALTER TABLE `private_tenders`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `reference`
--
ALTER TABLE `reference`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `registered_vendors`
--
ALTER TABLE `registered_vendors`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `resources`
--
ALTER TABLE `resources`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=253;

--
-- AUTO_INCREMENT for table `response`
--
ALTER TABLE `response`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `results`
--
ALTER TABLE `results`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `task`
--
ALTER TABLE `task`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tender_available`
--
ALTER TABLE `tender_available`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tender_evaluation`
--
ALTER TABLE `tender_evaluation`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tender_feedback`
--
ALTER TABLE `tender_feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `tender_number`
--
ALTER TABLE `tender_number`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=132;

--
-- AUTO_INCREMENT for table `vehicles_details`
--
ALTER TABLE `vehicles_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `vehicle_initial`
--
ALTER TABLE `vehicle_initial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `vendors`
--
ALTER TABLE `vendors`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `vendor_details`
--
ALTER TABLE `vendor_details`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `view`
--
ALTER TABLE `view`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
