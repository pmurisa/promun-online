<?php

include('adminHeader.php');
require "DBAPI.php";
//require "AutoSendEmails.php";
$passCode = $_SESSION['passCode'];
$isAdmin = $_SESSION['isAdmin'];
//$vat = $_SESSION['vat'];

if(empty($passCode)){
    header ("location:adminLogin.php");
}
if($isAdmin != "1"){
    echo "<script>alert('Error: You are Not admin'); location='adminlogout.php'</script>";

}
else{
 
    // take comapny details
    $codata = getCompanyDetails();
   
    if(empty($codata)){

        header ("location:LoadContent.php");  
    }
    //sync all emails to gmail
    else{
     
        $UserData = getUserDetails($passCode);
        $lockUser = @$UserData[0]['lockUser'];
        $maxAuth = @$UserData[0]['maxAuth'];
        $user = @$UserData[0]['passCode'];
        $name =@$codata[0]["description"];
        
        
        
    }
    
     
     

    

}






?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>E-Promun</title>
<script>
function openForm() {
  document.getElementById("firstForm").style.display = "block";
}
function closeForm() {
  document.getElementById("firstForm").style.display = "none";
}
function openServer() {
  document.getElementById("serverForm").style.display = "block";
}
function openSMTP() {
  document.getElementById("smtpForm").style.display = "block";
}
    function closeForm() {
  document.getElementById("myForm").style.display = "none";
}

function closeServer() {
  document.getElementById("serverForm").style.display = "none";
}

function closeSMTP() {
  document.getElementById("smtpForm").style.display = "none";
}
    </script>
 
<style>

  .open-button {
  background-color: #555;
  color: white;
  padding: 16px 20px;
  border: none;
  cursor: pointer;
  opacity: 0.8;
  position: fixed;
  bottom: 23px;
  right: 28px;
  width: 280px;
}

/* The popup form - hidden by default */
.form-popup {
  
  position:fixed;
  top:200px;
  display:none;
  right: 150px;
  border: 3px solid #f1f1f1;
  border-radius:15px;
  z-index: 9;
  
}

/* Add styles to the form container */
.form-container {
  max-width: 200px;
  padding: 10px;
  background-color:white;
  
}

/* Full-width input fields */
.form-container input[type=number], .form-container input[type=text], .form-container input[type=email] {
  width: 100%;
  padding: 15px;
  margin: 5px 0 2px 0;
  border: none;
  background: #f1f1f1;
}

/* When the inputs get focus, do something */
.form-container input[type=number]:focus, .form-container input[type=text]:focus, .form-container input[type=email]:focus {
  background-color: #ddd;
  outline: none;
}

/* Set a style for the submit/login button */
.form-container .btn {
  background-color: green;
  color: white;
  padding: 16px 20px;
  border: none;
  cursor: pointer;
  width: 100%;
  margin-bottom:10px;
  opacity: 0.8;
}

/* Add a red background color to the cancel button */
.form-container .cancel {
  background-color: red;

}

/* Add some hover effects to buttons */
.form-container .btn:hover, .open-button:hover {
  opacity: 1;
}
.input{
  font-size:10px;
  border-radius:4px;
  
}
  
  
  </style>
     
  </head>
 


 <body>
<tr>
    <td colspan="2">
       <table border="1" class="table-bordered" width="100%" style="background-color: #002F74; border-radius:20px; border-color:white;">
          <tr>
             <td width="20%" style="background-color: white" valign="top">
				 <?php
                    include('admindashboard.php');
                 ?>
             </td>
             <td width="80%" valign="top"><br>
            <table class="table-stripped" border="0" width="98%" align="center"  >
            <td>
            <a href="#"><img src="img/company.jpg" style="margin-top:10px; margin-left:2%; " class="img-circle" width="60" height="60"></a><br>
            <button type="submit"  class="btn btn-primary  btn-ms" onclick="openForm()">Account Setup</button><br>
            <a href="#"><img src="img/server.jpg" style="float-left; margin-left:30%;margin-top:10px;  " class="img-circle" width="50" height="50"></a><br>
            <button style="margin-left:27%;" type="submit"  class="btn btn-danger  btn-ms" onclick="openServer()">Server Setup</button><br>
            <a href="#"><img src="img/gmail_icon.jpg" style="float-left; margin-left:60%;margin-top:10px;  " class="img-circle" width="50" height="50"></a><br>
            <button style="margin-left:57%;" type="submit"  class="btn btn-success  btn-ms" onclick="openSMTP()">SMTP Setup</button><br>
            </td>
            <td>
            
            
            </td>
            </tr>
      
            <tr>
            <td>
            
            
            </td>
          </tr>
      </table>
      <div class="form-popup" id="serverForm" style="padding:15px;">
  <form method="post" class="form-container">
    <h6 style="color:red;">Change server IP Address</h6>

    <input type="text" class="input" placeholder="Current IP (197.45.0.12:80)" name="old" required >
    <input type="text" class="input" placeholder="New IP (197.45.0.12:80)" name="new" required>
    <button type="submit" name = "save" class="btn btn-primary  btn-ms "  >Save</button>
    <button type="submit"  class="btn btn-warning  btn-ms" onclick="closeServer()">Remove</button>
    </form>
  </div>

<?php
            if(isset($_POST['save']))
                {
                $old = trim($_POST['old']);
                $new = trim($_POST['new']);
                $first_no = substr($new,0,3);
                $first_dot = substr($new,3,1);
                $second_no = substr($new,4,2);
                $second_dot = substr($new,6,1);
                $third_no = substr($new,7,1);
                $third_dot = substr($new,8,1);
                $fourth_no = substr($new,9,2);
                
                if(empty($old)or empty($new))
                {
                  echo "<script color:red;>alert('No details provided'); location='adminHome.php'</script>";
                }
                if($first_dot <> "." or $second_dot <> "." or $third_dot <> ".")
                {
                  echo "<script color:red;>alert('Invalid IP Format is 000.00.0.00'); location='adminHome.php'</script>";
                }
               
                else
                { 	
                    $check_ip = ipExist($old);
                   
                    if($check_ip['status'] == "passed"){
                    $save = updateServer($new,$old);
                    //update the audit table for future reference
                    $date = date('Y-m-d');
        
                    $action = "User $passCode changed server details from $old to $new ";
                    $log = logDetails($passCode,$action,$date);
                   
                    echo "<script color:red;>alert('Succesfully server details'); location='adminHome.php'</script>";
                  
                    }
                }
            }
            ?>
               
 
            <div class="form-popup" id="smtpForm" style="padding:15px;">
  <form method="post" class="form-container">
    <h6 style="color:red;">Enter SMTP Details</h6>

    <input type="text" class="input" placeholder="Host (Eg smtp.gmail.com)" name="host" required>
    <input type="text" class="input" placeholder="Port (eg 587)" name="port" required>
    <input type="email" class="input" placeholder="Username (email)" name="username" required>
    <input type="text" class="input" placeholder="Password" name="password" required>
    <button type="submit" name = "saveSMTP" class="btn btn-primary  btn-ms "  >Save</button>
    <button type="submit"  class="btn btn-warning  btn-ms" onclick="closeSMTP()">Remove</button>
    </form>
  </div>
 
<?php
            if(isset($_POST['saveSMTP']))
                {
                $host = $_POST['host'];
                $port = $_POST['port'];
                $username = $_POST['username'];
                $password = $_POST['password'];
                
                if(empty($host)or empty($port) or empty($username) or empty($password))
                {
                  echo "<script color:red;>alert('No details provided'); location='adminHome.php'</script>";
                }
               
                else
                { 	
                  
                  $checkCo =  getCompanyDetails();
                  $codeLA = @$checkCo[0]['code'];
                    
                    $save = saveSMTP($host,$port,$username,$password,$codeLA);
                    //update the audit table for future reference
                    $date = date('Y-m-d');
        
                    $action = "User $passCode entered new smtp details ";
                    $log = logDetails($passCode,$action,$date);
                   
                    echo "<script color:red;>alert('Succesfully server details'); location='adminHome.php'</script>";
                  
                    
                }
            }
            ?>
    
             <div class="form-popup" id="firstForm" style="padding:15px;">
  <form method="post" class="form-container">
  <h6 style="color:red;">Please Enter Your Local Authority Details</h6>
<input type="text" class="input" placeholder="Procurement Email" name="email" required><br>
<input type="number" class="input" placeholder="Biller Code" name="bCode" required><br>
<input type="text" class="input" placeholder="Bank Account" name="bank" required><br>
<input type="text" class="input" placeholder="SMS Account Name" name="sms" required><br>
<input type="text" class="input" placeholder="SMS Account password" name="smspwd" required><br>
<input type="number" class="input" placeholder="SMS Credits" name="smscredit" required><br>
<input type="text" class="input" placeholder="SMS API" name="api" required><br>
<button type="submit" name = "saveDetails" class="btn btn-primary  btn-ms "  >Save</button>
</form>
  </div>
 

           <?php
           if(isset($_POST['saveDetails']))
               {
                
               $email = $_POST['email'];
               $bCode = $_POST['bCode'];
               $bank = $_POST['bank'];
               $sms = $_POST['sms'];
               $smsCredit = $_POST['smscredit'];
               $smspwd = $_POST['smspwd'];
               $api = $_POST['api'];
               
               if(empty($email)or empty($bCode)or empty($bank)or empty($sms)or empty($smspwd)or empty($api))
               {
                 echo "<script color:red;>alert('No details provided'); location='adminHome.php'</script>";
               }
              
               else
               { 	
                 $check =  getCompanyDetails();
                 $code = @$check[0]['code'];
              
                  $save = updateCo($email,$bCode,$bank,$sms,$smspwd,$smsCredit,$api,$code );
                  
                   
                  if(isset($save)){
                    echo "<script color:red;>alert('LA $des Successfully updated'); location='adminHome.php'</script>";
                  }
                  
                 
                 
               }
           }
           ?>
      
  

      
    </td>
</tr>
<body>
</html>
<?php
include('footer1.php');
?>
