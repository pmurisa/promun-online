<?php
class InputValidator
{
	var $input;
	var $valid =false;
	

function __construct($in){
	$this->input=$in;
}
function isString()
{
	if (is_string($this->input))
	{
		return true;
	}
	else
	{
		return false;
	}
}

function isIdentifier()
{
	//makes sure the name is made up of just alphabetic characters...reject names like fgh56, john_56
	
	  if (ctype_alpha($this->input)) {
      return true;
	  }
	  else
	  {
		  
return false;   
 }
}

function isDate(){
	$str_dateformat ="Y-m-d";
	 $str_dt = $this->input;
$date = DateTime::createFromFormat($str_dateformat, $str_dt, new DateTimeZone("Africa/Harare"));
  return $date && $date->format($str_dateformat) == $str_dt;
}

function isDOB()
{ //users at least 18 years of age.

//check if input is valid date

if($this->isDate())
{
$from = strtotime($this->input);
$today = time();
$difference = $today - $from;
 $time_btwn = floor($difference / (60 * 60 * 24 * 365));
 
 if($time_btwn >=18)
 {
	 return true;
 }
 else
 {
	 return false;
 }
 
}
else
{
	return false;
}
}
function startsWith($start)
{
	return strpos($this->input,$start)===0;
	
}
function contains($cont)
{
// search for $cont within the string
    $pos = strpos($this->input, $cont);
    if ($pos === false) {
      return false;
    } else {
       return true;
    }

}
 function hasNumber($num)
{ //search the string for 0-9
 $pos = strpos($this->input, $num);
    if ($pos === false) {
      return false;
    } else {
       return true;
    }
}

function isNumber()
{
	if (is_numeric($this->input)){
		
		return true;
	}
	else {
	return false;
	}
}


}



?>