<?php

require_once ("messaging.php");
require_once ("../Approvals/DBAPI.php");
$smsDetails = takeReadySms();
//take company details
$LaInf = getCompanyDetails();
$laname = $LaInf[0]["description"];
$from = @$smsDetails[0]['senderId'];



?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<style>
/*Sidemenu*/
.menu 
{
	list-style:none;
}
.menu li ul 
{
	list-style:none;
}
.menu li ul li 
{
	display:block; 
	height:40px; 
	border-bottom:solid 1px #E4D9D9;
}
.menu li ul a, .menu li ul a:visited, .menu li ul a:active {
	display:block; 
	height:40px; 
	padding:5px 5px 0px 5px; 
	text-decoration:none; 
	color: blue;
	font-size: 12px;
	}
.menu li ul  a:hover {
	background: white;
	color: brown;
	font-weight: bold;
	border-bottom:solid 1px #E4D9D9;
}
.menu h2 {
	display:block;
	border-bottom:solid 2px brown;
	padding:5px;
	margin:0px 0px 0px 0px;
	font-family: Arial, Helvetica, sans-serif;
	font-size:18px;
	color:white;
	font-weight: bolder;
}
#circle div {
	background-color: #000;

    margin-left:340px;
	height: 60px;
	
	width: 60px;
	-webkit-animation-name: circle_move;
	-webkit-border-radius: 60px;
	-webkit-animation-duration: 4s;
	-webkit-animation-iteration-count: infinite;
	-webkit-animation-direction: linear;
	opacity: 0.5;
	}
	div#one{
		align:center;
	 	-webkit-animation-delay: 0.5s;

	 }
	div#two{
	 	-webkit-animation-delay: 1.2s;
	}

	div#three{
	 	-webkit-animation-delay: 2s;
	}
	@-webkit-keyframes circle_move{
		0% { background-color:white; }
		30%{ opacity:1;
		  background-color:red;
		    }
		60%	{ background-color:green; }	
		100% { 
			opacity:0.5;
			background-color:purple; }
	      }
</style>
<title>E-Promun Approval Platform</title>

<!------------------------------------------------------------------------- Bootstrap --------------------------------------------------------------->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
<link rel="icon" href="img/axislogo.jpg">
</head>
<body><br>
<form method="post" style="  box-shadow :1px 1px 20px gray; width: 95%; border-radius: 10px 10px 10px; margin:auto">
<table border="0" width="90%" align="center" class="table-responsive" style="border-color:gray">
<tbody>
          <tr>
        	<td colspan="2"  height="50" align="center">
            	<table border="0" width="90%" align="center" class="table-responsive" style="border-color:gray">
                	<tr>
                    	<td align="center">
                        	<img src="img/axislogo.jpg" width="100" height="100" class="img-circle">
                        </td>
                        <td width="80%" align="center">
                        	<h3 align="center" style="color: blue"><strong>E-Promun System</strong></h3>
							<p style="line-height: 20px">Zimbabwe <br> <label style="color:#FF0000">"Your Order, Our Commitment"</label></p>
                        </td>
                        <td align="center">
                        	<img src="img/download.jpg" width="100" height="100" class="img-responsive">
                        </td>
                    </tr>
                </table>
            </td>
          </tr>
		  <tr>
        	<td colspan="2" height="5" style="background-color: #E4D9D9">
            </td>
          </tr>
          <tr>
          	<td colspan="2">
          		<table width="100%" border="0">
                	<tr>
                        <td width="35%">
                        <h5 style=" color:brown;" class="badge">
                            <strong>Urban Councils Board <?php ?>
							
							</strong></h5>
                        </td>
                        <td align="right" style="color: #0000FF" width="35%">
                            <?php

  echo "<h5 style='color: blue'><a href='../Approvals/adminlogout.php'><strong>click here to logout</strong></a></h5>";
							?>
                         </td>
                     </tr>
                </table>
             </td>
          </tr>
  
 
<tr>
    <td colspan="2">
       <table border="1" class="table-bordered" width="100%" style="background-color: #002F74; border-radius:20px; border-color:white;">
          <tr>
             <td width="20%" style="background-color: white" valign="top">
				 <?php
				 
                    include('admindashboard.php');
                 ?>
             </td>
             <td width="80%" valign="top"><br>
            <table class="table-stripped" border="0" width="98%" align="center"  >
            <td>
			<div id="circle">
	<div id="one"><img src="img/gmail.jpg" width="100" height="100" class="img-circle"></div>
	

			<?php
			
 if(!empty($smsDetails))
{
	$checkNet =  checkInternet();
	if($checkNet['status']=="offline"){
		echo "<script>alert('Error: No network connection messages can not be submitted!!');location='../Approvals/noNetworkPdf.php '</script>";
	}else{
    foreach($smsDetails as $peruser)
	{
		$msg= @$peruser["message"];
		//process phone
		$to = @$peruser["phone"];
		//$pNo= @$peruser["phone"];
		//$code = 263;
		//$process= substr($pNo, 1,9);
		//$to = $code . $process;
		

		//$to= @$peruser["phone"];
		$id= @$peruser["txtid"];
		$to_send = new Message(time(),$to,$from,date("Y-m-d H:i:s"),null,$msg);
		$sendopt = $to_send->sendUserBulk($from);
		
		
		if($sendopt["status"]=="ok"){
		$response = '<div class="alert alert-success">Bulk SMS sent to selected user(s) </div>';
		//update sms
		$update = updateSms($id);
		

	    	//triger success report
			echo "<script>alert('View Sent Messages Report'); location='../Approvals/sentPdf.php'</script>";
		}
		elseif($sendopt["status"]=="off"){
			$response = ' <div class="alert alert-danger">credits not enough to send to selected contacts </div>';
		}
		else{
			echo "<script>alert('View Exception Report'); location='../Approvals/UnsentPdf.php'</script>";
			
		}
		
	}
	}




}




?>
</div>

            </td>
            <td>
            <p id="piechart_3d" style="width: 500px; height: 300px;"></p>
            
            </td>
            </tr>
      
            <tr>
            <td>
            
            
            </td>
          </tr>
      </table>
  

      
    </td>
</tr>
<?php
include('footer.php');
?>
