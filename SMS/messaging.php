<?php

//@ob_start();
//@session_start();
//require_once('mesgAPI.php');
require_once('../Approvals/DBAPI.php');
//$vat = $_SESSION['vat'];
/*$companyDetails = companySms($vat);
foreach($companyDetails as $c){
    $account = @$c['smsAccount'];
    $password = @$c['password'];
}*/

//include_once('libs/httpful.phar');

class Message implements JsonSerializable {

    private $to;
    private $from;
    private $sentTime;
    private $receivedTime;
    private $body;
    private $id;
    private $acc;
    private $user;
    private $status;
    private $delivered;
    private $validity;
    private $setFlow;
    private $varflowlevel;
   

    function __construct($id, $to, $from, $sentTime, $receivedTime, $body) {
        $this->to = $to;
        $this->from = $from;
        $this->sentTime = $sentTime;
        $this->receivedTime = $receivedTime;
        $this->body = $body;
        $this->id = $id;
        //$this->acc = $_SESSION['acc'];
        $this->user = $_SESSION['passCode'];
        $this->status = 'submitted';
    }

    function getTo() {
        return $this->to;
    }

    function setTo($to) {
        $this->to = $to;
    }

    function getFrom() {
        return $this->from;
    }

    function setFrom($from) {
        $this->from = $from;
    }

    function getSentTime() {
        return $this->sentTime;
    }

    function setSentTime($sentTime) {
        $this->sentTime = $sentTime;
    }

    function getReceivedTime() {
        return $this->receivedTime;
    }

    function setReceivedTime($receivedTime) {
        return $this->receivedTime = $receivedTime;
    }

    function getBody() {
        return $this->body;
    }

    function setBody($body) {
        return $this->body = $body;
    }

    function getId() {
        return $this->id;
    }

    function setId($id) {
        return $this->id = $id;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function getStatus() {
        return $this->status;
    }

    function setUser($user) {
        $this->user = $user;
    }

    function getUser() {
        return $this->user;
    }

    function setValidity($validity) {
        $this->validity = $validity;
    }

    function isValid() {
        return $this->validity;
    }

    function setFlow($flow) {
        $this->setFlow = $flow;
    }

    function setFlowLevel($level) {
        $this->varflowlevel = $level;
    }

  

    function save() {
        $credit = ceil(strlen($this->getBody()) / 160);
        saveToSendMsg($this, $credit);
        $file = fopen("msgoutlog.txt", "a");
        fclose($file);
    }

    function getKeyWord() {
        //trim whitespace from the ends of the string first

        $spaceFree = trim($this->body);

        //find the 1st word wc is the keyword
        return strtok($spaceFree, " "); //
    }

    function logMsgToFile() {
        return $this->id . "\t" . $this->from . "\t" . $this->to . "\t" . $this->body . "\t" . $this->sentTime . "\t" . $this->receivedTime . PHP_EOL;
    }

    public function jsonSerialize() {
        return get_object_vars($this);
    }

    function setAcc($acc) {
        $this->acc = $acc;
    }

    function getAcc() {
        return $this->acc;
    }

    function getDelivered() {
        return $this->delivered;
    }

    function setDelivered($delivrd) {
        $this->delivered = $delivrd;
    }

    function update() {
        updateMsg($this);
    }

    function sendUserBulk($from) {

        $balance = getBalance();
        $bal = $balance[0]['creds'];
        $credit = ceil(strlen($this->getBody()) / 160);

        if ($bal >= $credit) {
            $result["status"] = "ok";

            reduceToSendCredits($credit);
            $this->sendUserSms($from);
        } else {

            $result["status"] = "off";
        }
        return $result;
    }

    function sendUserSms($msg_from ) {
        if (strpos(trim($this->to), "0") === 0) {

            $recepient = "263" . substr($this->to, 1);
        } elseif (strpos(trim($this->to), "7") === 0) {

            $recepient = "263" . substr($this->to, 1);
        } else if (strpos(trim($this->to), "+") === 0) {
            $recepient = substr($this->to, 1);
        } elseif (strpos(trim($this->to), "2") === 0) {
            $recepient = $this->to;
        }
        //Take Account details
        $accountDetails = getSmsAccount();
        $account = $accountDetails[0]['smsAccount'];
        $password = $accountDetails[0]['password'];
        $api = $accountDetails[0]['api'];

       // $message = urlencode($this->getBody());
       $message = urlencode($this->body);
       $url = "https://rest.bluedotsms.com/api/SendSMS?api_id=$api&api_password=$password&sms_type=P&encoding=T&sender_id=$account&phonenumber=$recepient&textmessage=$message"; 
       // $url = "https://rest.bluedotsms.com/api/SendSMS?api_id=API6403997019&api_password=12AXissolutions&sms_type=P&encoding=T&sender_id=Axis&phonenumber=$recepient&textmessage=$message";
        
        
        
        $response = file_get_contents($url);

        $json = json_decode($response, TRUE);
        print_r($json);
        
      
        
    }

    //function to check balance
    function checkBalance() {
        $accountDetails = getSmsAccount();
        $account = $accountDetails[0]['smsAccount'];
        $password = $accountDetails[0]['password'];
        $api = $accountDetails[0]['api'];
        print_r($account);
        $url = "https://rest.bluedotsms.com/api/CheckBalance?api_id=$api&api_password=$password";  
        
        
        $response = file_get_contents($url);

        $json = json_decode($response, TRUE);
       // print_r($json);
        
      
        
    }

//interactions **********************************************************


    function ReplySMS($lstinstId) {
        if (strpos(trim($this->getFrom()), "0") === 0) {

            $recepient = "263" . substr($this->getFrom(), 1);
        } elseif (strpos(trim($this->getFrom()), "7") === 0) {

            $recepient = "263" . substr($this->getFrom(), 1);
        } else if (strpos(trim($this->getFrom()), "+") === 0) {
            $recepient = substr($this->getFrom(), 1);
        } elseif (strpos(trim($this->getFrom()), "2") === 0) {
            $recepient = $this->getFrom();
        }
      
        $message = urlencode($this->getBody());
        
        $url = "http://api.bluedotsms.com/api/mt/SendSMS?user=Axis&password=1234567890&senderid=".$this->getTo()."&channel=Normal&DCS=0&flashsms=0&number=".$recepient."&text=".$message;

    $response = file_get_contents($url);
    
  
        $json = json_decode($response, TRUE); //generate array object from the response from the web
        //if ($json['ErrorCode'] == "000" && $json['ErrorMessage'] == "Done") {
          //  $messageID = $json["JobId"]; 
          //  updateToSendMSG($messageID, $lstinstId);
       // } else {
           
            //print_r($json);
       // }
        //return json_encode($json);
    }

    function SendAndSaveMSG() {
        $balance = getBalance();
        $bal = $balance[0]['creds'];
        $credit = ceil(strlen($this->getBody()) / 160);

        if ($bal >= $credit) {
            $result["status"] = "ok";
       $lstid = saveRplyFormMsg($this, $credit);
                 reduceToSendCredits($credit);
            $this->ReplySMS($lstid);
        } else {

            $result["status"] = "off";
        }
        return $credit;
    }

}
